package org.ici.stats;

import org.ici.stats.utils.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DateUtilsTest {
    private static Logger logger = LoggerFactory.getLogger( DateUtilsTest.class );

    private static DateTimeFormatter formatter = DateTimeFormat.forPattern( "MM/yyyy" );

    private static Date from( String dateStr ) {
        DateTime date = formatter.parseDateTime( dateStr );
        return date.toDate();
    }

    private static String format( Date date ) {
        DateTime wrapper = new DateTime( date );
        return formatter.print( wrapper );
    }

    private void testDate( String in, String expectedOut ) {
        Date inDate = from( in );
        Date out = DateUtils.getMostRecentFullQuarter( inDate );
        assertEquals( "sent in: " + in + ", received: " + format(out), expectedOut, format( out ));
    }

    @Test
    public void testGetQuarterFromMonth() throws Exception {
        testDate( "03/2012", "03/2012" );
        testDate( "07/2012", "06/2012" );
        testDate( "10/2012", "09/2012" );
        testDate( "01/2012", "12/2011" );

    }
}