package org.ici.stats.utils;

import com.google.common.collect.Lists;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.Report;
import org.ici.stats.services.reports.mutualfund.MutualFundsProvider;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CategoryUtilsTest {

    Report mutualFunds;

    @Before
    public void setUp() throws Exception {
        mutualFunds = new MutualFundsProvider().get();
    }

    @Test
    public void getChildlessChildren() throws Exception {
    }

    @Test
    public void getFlatList() throws Exception {
    }

    @Test
    public void getFlatList1() throws Exception {
    }

    @Test
    public void categoriesAtLevel() throws Exception {
    }

    @Test
    public void categoriesWithoutLevel() throws Exception {
    }

    @Test
    public void getIobsForCategory() throws Exception {
        Category cat = mutualFunds.getCategoryByName( "Domestic Equity" );
        List<Integer> iobs = CategoryUtils.getIobsForCategory( cat );
        assertEquals(Lists.newArrayList( 1, 2, 3, 4, 5 ), iobs );
        Category cat2 = mutualFunds.getCategoryByName( "World Equity" );
        List<Integer> iobs2 = CategoryUtils.getIobsForCategory( cat2 );
        assertEquals(Lists.newArrayList( 6, 8, 9, 7, 10 ), iobs2 );
    }

}