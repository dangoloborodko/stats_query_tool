package org.ici.stats.dao;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.log4j.BasicConfigurator;
import org.ici.stats.domain.*;
import org.ici.stats.domain.builders.ReportRequestBuilder;
import org.ici.stats.guice.SerivceModule;
import org.ici.stats.services.reports.mutualfund.MutualFundsProvider;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class TrendsQueryDAOTest {

    TrendsQueryDAO dao;

    private static final org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern( "MM/dd/yyyy");

    @Before
    public void setUp() throws Exception {
        BasicConfigurator.configure();
        Injector injector = Guice.createInjector( new SerivceModule() );
        dao = injector.getInstance( TrendsQueryDAO.class );
    }

    @Test
    public void testFunds() throws Exception {
        Report mutualFund = new MutualFundsProvider().get();
        ReportRequestBuilder builder  = new ReportRequestBuilder(ReportType.MutualFunds );
        builder.setStart( "07/01/2017" );
        builder.setEnd( "07/31/2017" );
        builder.setPeriod( Period.Monthly );
        builder.addDataPoint( DataPoint.TotalNetAssets );
        Category cat = mutualFund.getCategoryByName( "Domestic Equity");
        builder.addCategory( cat );
        MethodOfSale mos = mutualFund.getMethodOfSaleByName( "Retail" );
        builder.addMethodsOfSale( mos );
        ReportResult results = dao.getData( builder.createReportRequest() );
        assertNotNull( results );
        assertFalse( results.getData().isEmpty() );
    }
}