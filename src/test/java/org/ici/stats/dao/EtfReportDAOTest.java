package org.ici.stats.dao;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.log4j.BasicConfigurator;
import org.ici.stats.domain.*;
import org.ici.stats.domain.builders.ReportRequestBuilder;
import org.ici.stats.guice.SerivceModule;
import org.ici.stats.services.reports.etf.ETFProvider;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class EtfReportDAOTest {

    EtfReportDAO dao;

    private static final org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern( "MM/dd/yyyy");

    @Before
    public void setUp() throws Exception {
        BasicConfigurator.configure();
        Injector injector = Guice.createInjector( new SerivceModule() );
        dao = injector.getInstance( EtfReportDAO .class );
    }

    @Test
    public void getData() throws Exception {
        ETFProvider etfProvider = new ETFProvider();
        Report etf = etfProvider.get();
        ReportRequestBuilder builder = new ReportRequestBuilder( ReportType.ETF );
        builder.addDataPoint(DataPoint.TotalNetAssets );
        builder.addCategory( etf.getCategoryByName( "Broad Based Equity" ));
        builder.setPeriod(Period.CurrentMonth);
        builder.setEnd( "06/30/2017" );
        ReportResult results = dao.getData( builder.createReportRequest() );
        assertNotNull( results );
        assertFalse( results.getData().isEmpty() );
    }

}