package org.ici.stats.domain;

import com.google.common.collect.Lists;
import org.ici.stats.services.reports.mutualfund.MutualFundsProvider;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class MosUtilsTest {

    Report mutualFunds;

    @Before
    public void setUp() throws Exception {
        mutualFunds = new MutualFundsProvider().get();
    }

    @Test
    public void testGetMosIds() throws Exception {
        MethodOfSale mos = mutualFunds.getMethodOfSaleByName( "Retail" );
        List<Integer> mosIds = MosUtils.getMosIds(Lists.newArrayList(mos));
        assertEquals( Lists.newArrayList(1, 2, 3, 4), mosIds );
    }
}