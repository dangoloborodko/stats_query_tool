/**
 * Created by jalexander on 10/27/2014.
 */

def parser = new XmlSlurper()
parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false);
def config = parser.parse( new File( "C:\\workspace\\stats_query_tool\\src\\main\\resources\\report-param-config.xml"));
println( "config: " + config."fund-categories" )
def groups = config."fund-categories"."fund-category-group".findAll{ it."supported-report".@id == 'R1' }

println( groups );
