package org.ici.stats.rest;

import org.slf4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * some helper methods to make sure we handle errors consistently
 */
public class ResponseHelper {
    public static Response error( Response.Status status, Logger logger, String message, Object ... pieces ) {
        logger.error( message, pieces );
        return Response.status( status ).entity( new ErrorMessage( message, pieces )).build();
    }

    public static Response badRequest( Logger logger, String message, Object ... pieces ) {
        return error( Response.Status.BAD_REQUEST, logger, message ,pieces );
    }

    public static Response notFound( Logger logger, String message, Object ... pieces ) {
        return error( Response.Status.NOT_FOUND, logger, message ,pieces );
    }

    public static <T> Response created( UriInfo _uriInfo, int itemId, T newItem ) {
        return Response.status( Response.Status.CREATED ).header( "Location", String.format(
                "%s/%s",_uriInfo.getAbsolutePath().toString(), itemId )).entity( newItem ).build();
    }

}
