package org.ici.stats.rest;

/**
 * holds an error message for bad requests
 */
public class ErrorMessage {
    private String message;

    public ErrorMessage() {}

    public ErrorMessage(String message) {
        this.message = message;
    }

    public ErrorMessage(String message, Object... parts) {
        String localMessage = message;
        int idx = 0;
        while ( localMessage.contains( "{}" ) && idx < parts.length) {
            localMessage = localMessage.replace( "{}", parts[idx++].toString() );
        }
        this.message = localMessage;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ErrorMessage{");
        sb.append("message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
