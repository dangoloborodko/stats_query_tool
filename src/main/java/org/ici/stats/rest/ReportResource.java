package org.ici.stats.rest;

import org.ici.stats.domain.ReportType;
import org.ici.stats.services.ReportDefinitionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * application resources
 */
@Path( "reports")
public class ReportResource {

    private static final Logger logger = LoggerFactory.getLogger(ReportResource.class);
    private final ReportDefinitionService reportService;

    @Context
    private UriInfo _uriInfo;

    @Inject
    public ReportResource(ReportDefinitionService reportService) {
        this.reportService = reportService;
    }

    @GET
    @Produces( MediaType.APPLICATION_JSON )
    public Response getAll() {
        return Response.ok( reportService.getReportTypes() ).build();
    }

    @GET
    @Path( "/{id}" )
    @Produces( MediaType.APPLICATION_JSON )
    public Response getReportDefinitionById( @PathParam( "id" ) String id ) {
        ReportType type = ReportType.valueOf( id );
        if ( type == null ) {
            return ResponseHelper.notFound( logger, "unable to find application with id: {}", id );
        }
        return Response.ok( reportService.getReportDefinition( type ) ).build();
    }
}
