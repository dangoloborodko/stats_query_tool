package org.ici.stats.services;

import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.Report;

import java.util.List;

public interface ReportDefinitionService {
    Report getReportDefinition(ReportType type);
    List<ReportType> getReportTypes();
}
