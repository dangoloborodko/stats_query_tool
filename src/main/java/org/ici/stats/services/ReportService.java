package org.ici.stats.services;

import org.ici.stats.domain.ReportRequest;
import org.ici.stats.domain.ReportResult;

public interface ReportService {
    ReportResult buildReport(ReportRequest request );
}
