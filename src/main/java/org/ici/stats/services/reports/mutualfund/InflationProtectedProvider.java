package org.ici.stats.services.reports.mutualfund;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Provider;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class InflationProtectedProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType( ReportType.TIPs );
        builder.setStartDate("01/2010" );
        return builder.addCategories(
                new Category("Inflation Protected/TIPS", 30,
                        new Category("Inflation Protected", 31),
                        new Category("TIPs", 32))
        ).addDataPoints(new DataPointGroup(Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetNewCashFlow)
        ).setDataPointFieldMap(
                new ImmutableMap.Builder<DataPoint,String>()
                        .put( DataPoint.TotalNetAssets, "tna_total" )
                        .put( DataPoint.NumberOfFunds, "tna_total" )
                        .put( DataPoint.NetNewCashFlow, "tna_total" ).build()

        ).createReport();
    }
}
