package org.ici.stats.services.reports.mutualfund;

import com.google.inject.Provider;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class SectorProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType( ReportType.Sector );
        builder.setStartDate("01/2000");
        return builder.addCategories(
                new Category("Sectors", 90,
                        new Category("Consumer", 91),
                        new Category("Financial", 92),
                        new Category("Health", 93),
                        new Category("Natural Resources", 94),
                        new Category("Precious Metals", 95),
                        new Category("Real Estate", 96),
                        new Category("Technology/Telecom", 97),
                        new Category("Utilities", 98),
                        new Category("Other Sectors", 99))
        ).addDataPoints(new DataPointGroup(Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetNewCashFlow)).createReport();
    }
}
