package org.ici.stats.services.reports.mutualfund;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Provider;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

/**
 * Created by jalexander on 10/27/2014.
 */

public class RetirementProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType( ReportType.Retirement );
        builder.setStartDate( "01/2003" );
        return builder.addCategories(
                new Category("All Retirement Funds", 50,
                        new Category("Equity", 51),
                        new Category("Hybrid", 52),
                        new Category("Bond", 53),
                        new Category("Money Market", 54))
        ).addDataPoints(new DataPointGroup(Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetNewCashFlow )
        ).setDataPointFieldMap(
                new ImmutableMap.Builder<DataPoint,String>()
                        .put( DataPoint.TotalNetAssets, "tna_total" )
                        .put( DataPoint.NumberOfFunds, "fundcount_total" )
                        .put( DataPoint.NetNewCashFlow, "net_new_cash" ).build()
        ).setFootNote("* Retirement mutual funds include share classes and funds that are predominantly available to retirement plans or IRAs. The table includes data for funds that invest primarily in other funds."
        ).createReport();
    }
}
