package org.ici.stats.services.reports.mutualfund;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Provider;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Level;
import org.ici.stats.domain.MethodOfSale;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.builders.ReportBuilder;

/**
 * structure for the mutual fund report
 */
public class MutualFundsProvider implements Provider<Report> {
    @Override
    public Report get() {
        int fakeId = -1;
        ReportBuilder builder = new ReportBuilder().setType(ReportType.MutualFunds);
        builder.setStartDate("01/1984").setMosStartDate("01/2000");
        builder.setAllowPartialQuarters(true);
        builder.addMethodsOfSale(
                new MethodOfSale("Retail", fakeId--,
                        new MethodOfSale("Retail excluding Variable Annuity", fakeId--,
                                new MethodOfSale("Sales Force", 1, MethodOfSale.AccessLevel.AdminOnly ),
                                new MethodOfSale("Direct Market", 2, MethodOfSale.AccessLevel.AdminOnly ),
                                new MethodOfSale("Retirement", 3, MethodOfSale.AccessLevel.AdminOnly)),
                        new MethodOfSale("Variable Annuity", 4)),
                new MethodOfSale("Institutional", 5));
        return builder.addCategories(
                new Category("All Mutual Funds", fakeId--, Level.All,
                        new Category("Long Term", fakeId--, Level.Super,
                                new Category("Equity", fakeId--, Level.Mega,
                                        new Category("Domestic Equity", 100, Level.High,
                                                new Category("Capital Appreciation", fakeId--, Level.Mid,
                                                        new Category("Growth", 1),
                                                        new Category("Sector", 2),
                                                        new Category("Alternative Strategies", 3)),
                                                new Category("Total Return", fakeId--, Level.Mid,
                                                        new Category("Value", 4),
                                                        new Category("Blend", 5))),
                                        new Category("World Equity", 101, Level.High, Level.Mid,
                                                new Category("Emerging Markets Equity", 6),
                                                new Category("International Equity", 8),
                                                new Category("Reqional Equity", 9),
                                                new Category("Global Equity", 7),
                                                new Category("World Equity - Alternative Strategies (from 2007)", 10))),
                                new Category("Hybrid", 102, Level.Mega, Level.High, Level.Mid,
                                        new Category("Asset Allocation", 11),
                                        new Category("Balanced", 12),
                                        new Category("Income-Mixed", 14),
                                        new Category("Flexible Portfolio", 13),
                                        new Category("Hybrid - Alternative Strategies (from 2006)", 15)),
                                new Category("Bond", 103, Level.Mega,
                                        new Category("Taxable Bond", fakeId--, Level.High,
                                                new Category("Investment Grade Bond", fakeId--, Level.Mid,
                                                        new Category("Investment Grade - Long Term", 17),
                                                        new Category("Investment Grade - Intermediate", 18),
                                                        new Category("Investment Grade - Short Term", 19),
                                                        new Category("Investment Grade - Ultra Short Term", 20),
                                                        new Category("Investment Grade - Multi Term", 16),
                                                        new Category("Inflation Protected (from 2003)", 21)),
                                                new Category("High Yield", 22, Level.Mid),
                                                new Category("Government Bond", fakeId--, Level.Mid,
                                                        new Category("Government - Multi Term", 23),
                                                        new Category("Government - Long Term", 24),
                                                        new Category("Government - Intermediate", 25),
                                                        new Category("Government - Short Term", 26),
                                                        new Category("Mortgage Backed", 27)),
                                                new Category("Multi Sector", fakeId--, Level.Mid,
                                                        new Category("Multi Sector - Long/Intermediate", 30),
                                                        new Category("Multi Sector - Short Term", 31),
                                                        new Category("Multi Sector - Multi Term", 28),
                                                        new Category("Multi Sector - Alternative Strategies (from 2007)", 32)),
                                                new Category("World Bond", fakeId--, Level.Mid,
                                                        new Category("Global Bond - Multi Term", 33),
                                                        new Category("Global Bond - Short Term", 34),
                                                        new Category("International Bond", 35))),
                                        new Category("Tax-exempt Bond", fakeId--, Level.High,
                                                new Category("State Municipal Bond", fakeId--, Level.Mid,
                                                        new Category("State Municipal", 36)),
                                                new Category("National Municipal Bond", fakeId--, Level.Mid,
                                                        new Category("National Municipal - Multi Term", 38),
                                                        new Category("National Municipal - Short Term", 39))))),
                        new Category("Money Market", 99, Level.Super, Level.Mega,
                                new Category("Taxable Money Market", fakeId--, Level.High, Level.Mid,
                                        new Category("Treasury & Repo Money Market", 40),
                                        new Category("Treasury & Agency Money Market", 41),
                                        new Category("Prime Money Market", 42)),
                                new Category("Tax-Exempt Money Market", fakeId--, Level.High, Level.Mid,
                                        new Category("National Tax-Exempt Money Market", 43),
                                        new Category("State Tax-Exempt Money Market", 44))))
        ).addDataPoints(
                new DataPointGroup(
                        "Assets, Flows, and Sales (Monthly)",
                        DataPoint.TotalNetAssets, DataPoint.LiquidAssets, DataPoint.TotalSales,
                        DataPoint.NewSales, DataPoint.ReinvestedDividends, DataPoint.Redemptions,
                        DataPoint.NetSales, DataPoint.NetNewSales, DataPoint.NetNewCashFlow,
                        DataPoint.SalesExchanges, DataPoint.RedemptionExchanges, DataPoint.NetExchanges
                ),
                new DataPointGroup("Portfolio Activity (Monthly)",
                        DataPoint.CommonStockPurchases, DataPoint.CommonStockSales, DataPoint.NetCommonStockPurchases,
                        DataPoint.OtherPortfolioPurchases, DataPoint.OtherPortfolioSales, DataPoint.NetOtherPortfolioPurchases,
                        DataPoint.AllPortfolioPurchases, DataPoint.AllPortfolioSales, DataPoint.NetPortfolioPurchases)
        ).setDataPointFieldMap(
                new ImmutableMap.Builder<DataPoint,String>()
                        .put( DataPoint.TotalNetAssets, "tna" )
                        .put( DataPoint.NumberOfFunds, "entities" )
                        .put( DataPoint.LiquidAssets, "ltot" )
                        .put( DataPoint.TotalSales, "total_sales" )
                        .put( DataPoint.NewSales, "sreg" )
                        .put( DataPoint.ReinvestedDividends, "srein" )
                        .put( DataPoint.Redemptions, "rreg" )
                        .put( DataPoint.NetSales, "net_sales")
                        .put( DataPoint.NetNewSales, "net_new_sales" )
                        .put( DataPoint.NetNewCashFlow, "net_new_cash" )
                        .put( DataPoint.SalesExchanges, "sex" )
                        .put( DataPoint.RedemptionExchanges, "rex" )
                        .put( DataPoint.NetExchanges, "net_exch" )
                        .put( DataPoint.CommonStockPurchases, "csppurchases" )
                        .put( DataPoint.CommonStockSales, "cspsales")
                        .put( DataPoint.NetCommonStockPurchases, "csp_net" )
                        .put( DataPoint.OtherPortfolioPurchases, "othppurchases" )
                        .put( DataPoint.OtherPortfolioSales, "othpsales" )
                        .put( DataPoint.NetOtherPortfolioPurchases, "othp_net" )
                        .put( DataPoint.AllPortfolioPurchases, "mctotp" )
                        .put( DataPoint.AllPortfolioSales, "mctots" )
                        .put( DataPoint.NetPortfolioPurchases, "mctot_net" ).build()
        ).setDataPointEquation(
                new ImmutableMap.Builder<DataPoint,String>()
                .put( DataPoint.NetSales, "sreg + srein - rreg")
                .put( DataPoint.TotalSales, "sreg + srein")
                .put( DataPoint.NetNewSales, "sreg - rreg")
                .put( DataPoint.NetNewCashFlow, "sreg - rreg + sex - rex")
                .put( DataPoint.NetExchanges, "sex - rex").build()
        ).createReport();
    }
}
