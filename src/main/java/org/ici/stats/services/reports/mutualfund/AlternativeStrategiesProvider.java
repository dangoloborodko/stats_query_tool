package org.ici.stats.services.reports.mutualfund;

import com.google.inject.Provider;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class AlternativeStrategiesProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType( ReportType.AlternativeStrategies );
        builder.setStartDate("01/2007");
        return builder.addCategories(
                new Category("Alternative Strategies", 100,
                        new Category("Domestic", 101),
                        new Category("World", 102),
                        new Category("Hybrid", 103),
                        new Category("Multisector Bond", 104),
                        new Category( "World Bond", 113, "01/2010" ))
        ).addDataPoints(new DataPointGroup( Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetNewCashFlow)).createReport();
    }
}
