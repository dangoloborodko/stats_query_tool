package org.ici.stats.services.reports.mutualfund;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Provider;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class ActiveIndexProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType(ReportType.ActiveIndex);
        builder.setStartDate("01/2000");
        return builder.addCategories(
                new Category("Active Funds: Total", 1,
                        new Category("Domestic Equity", 2),
                        new Category("World Equity", 3),
                        new Category("Hybrid And Bond", 4)),
                new Category("Index Funds: Total", 5,
                        new Category("Domestic Equity S&P 500", 6),
                        new Category("Domestic Equity Other", 7),
                        new Category("World Equity", 8),
                        new Category("Hybrid and Bond", 9))
        ).addDataPoints(new DataPointGroup( Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetNewCashFlow )
        ).setDataPointFieldMap(
                new ImmutableMap.Builder<DataPoint,String>()
                        .put( DataPoint.TotalNetAssets, "tna_total" )
                        .put( DataPoint.NumberOfFunds, "fundcount_total" )
                        .put( DataPoint.NetNewCashFlow, "net_new_cash" ).build()
        ).createReport();
    }
}
