package org.ici.stats.services.reports.mutualfund;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Provider;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class MarketCapitalizationProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType( ReportType.MarketCapitalization );
        builder.setStartDate("01/2000");
        return builder.addCategories(
                new Category("Growth", 70,
                        new Category("Small cap", 71),
                        new Category("Mid cap", 72),
                        new Category("Large cap", 73),
                        new Category("Multi cap", 74)),
                new Category( "Value", 75,
                        new Category("Small cap", 76),
                        new Category("Mid cap", 77),
                        new Category("Large cap", 78),
                        new Category("Multi cap", 79)),
                new Category( "Blend", 80,
                        new Category("Small cap", 81),
                        new Category("Mid cap", 82),
                        new Category("Large cap", 83),
                        new Category("Multi cap", 84))
        ).addDataPoints(new DataPointGroup(Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetNewCashFlow)
        ).setDataPointFieldMap(
                new ImmutableMap.Builder<DataPoint,String>()
                        .put( DataPoint.TotalNetAssets, "tna_total" )
                        .put( DataPoint.NumberOfFunds, "tna_total" )
                        .put( DataPoint.NetNewCashFlow, "tna_total" ).build()
        ).createReport();
    }
}
