package org.ici.stats.services.reports.etf;

import com.google.inject.Provider;
import org.ici.stats.domain.*;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class ETFActiveIndexProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType(ReportType.ETFActiveIndex);
        builder.setStartDate("01/2000");
        return builder.addCategories(
                new Category("Active Funds: Total", 1,
                        new Category("Domestic Equity", 2),
                        new Category("World Equity", 3),
                        new Category("Hybrid And Bond", 4)),
                new Category("Index Funds: Total", 5,
                        new Category("Domestic Equity S&P 500", 6),
                        new Category("Domestic Equity Other", 7),
                        new Category("World Equity", 8),
                        new Category("Hybrid and Bond", 9))
        ).addDataPoints(new DataPointGroup( Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetIssuance )
        ).createReport();
    }
}
