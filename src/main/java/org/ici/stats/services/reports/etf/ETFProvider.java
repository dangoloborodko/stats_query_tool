package org.ici.stats.services.reports.etf;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.ici.stats.domain.*;
import org.ici.stats.domain.builders.ReportBuilder;

/**
 * structure for the etf
 */
public class ETFProvider implements Provider<Report> {
    @Override
    public Report get() {
        int fakeId = -1;
        ReportBuilder builder = new ReportBuilder().setType(ReportType.ETF);
        builder.setStartDate("01/2000");
        builder.setAllowPartialQuarters(true);
        builder.setDataPoints( new DataPointGroup( "Data Points", DataPoint.TotalNetAssets, DataPoint.NetIssuance, DataPoint.GrossIssuance, DataPoint.GrossRedemptions ));
        return builder.addCategories(
                new Category("Industry Total", fakeId--, Level.All,
                        new Category("Equity", 800, IobLevel.CompIob98, Level.Mega,
                                new Category("Broad Based Equity", 800, IobLevel.SubCompIob98, Level.Super,
                                        new Category("Total Market", 800, IobLevel.Iob98, Level.High),
                                        new Category("Large Cap", 805, IobLevel.Iob98, Level.High),
                                        new Category("Mid Cap", 810, IobLevel.Iob98, Level.High),
                                        new Category("Small Cap", 815, IobLevel.Iob98, Level.High),
                                        new Category("Broad Based Other", 820, IobLevel.Iob98, Level.High)),
                                new Category("Sector", 801, IobLevel.SubCompIob98, Level.Super,
                                        new Category("Consumer", 825, IobLevel.SubIob98, Level.High),
                                        new Category("Financial", 826, IobLevel.SubIob98, Level.High),
                                        new Category("Health", 827, IobLevel.SubIob98, Level.High),
                                        new Category("Natural Resources", 828, IobLevel.SubIob98, Level.High),
                                        new Category("Real Estate", 830, IobLevel.SubIob98, Level.High),
                                        new Category("Technology", 831, IobLevel.SubIob98, Level.High),
                                        new Category("Utilities", 832, IobLevel.SubIob98, Level.High),
                                        new Category("Other Sectors", 833, IobLevel.SubIob98, Level.High)),
                                new Category("Global/International", 802, IobLevel.SubCompIob98, Level.Super,
                                        new Category("International", 835, IobLevel.SubIob98, Level.High),
                                        new Category("Global", 836, IobLevel.SubIob98, Level.High),
                                        new Category("Regional", 837, IobLevel.SubIob98, Level.High),
                                        new Category("Single Country", 838, IobLevel.SubIob98, Level.High),
                                        new Category("Emerging Markets", 839, IobLevel.SubIob98, Level.High))),
                        new Category("Hybrid", 801, IobLevel.CompIob98, Level.Mega,
                                new Category("Hybrid", 802, IobLevel.SubCompIob98, Level.Super,
                                        new Category("Hybrid", 855, IobLevel.Iob98, Level.High))),
                        new Category("Bond", 802, IobLevel.CompIob98, Level.Mega,
                                new Category("Bond", 803, IobLevel.SubCompIob98, Level.Super,
                                        new Category("Government Bond", 835, IobLevel.Iob98, Level.High),
                                        new Category("Municipal Bond", 851, IobLevel.Iob98, Level.High),
                                        new Category("Corporate Bond", 840, IobLevel.Iob98, Level.High),
                                        new Category("High Yield Bond", 845, IobLevel.Iob98, Level.High),
                                        new Category("International Bond", 850, IobLevel.Iob98, Level.High))),
                        new Category("Commodities", 829, IobLevel.SubIob98, Level.Mega))).createReport();

    }
}
