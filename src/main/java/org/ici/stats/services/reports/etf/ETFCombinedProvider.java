package org.ici.stats.services.reports.etf;

import com.google.inject.Provider;
import org.ici.stats.domain.*;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class ETFCombinedProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType(ReportType.MF_ETF);
        builder.setStartDate("01/2010");
        return builder.addCategories(
                new Category("Equity: Total", Constants.COMBINED_EQUITY_TOTAL,
                        new Category("Equity: Domestic", Constants.COMBINED_EQUITY_DOMESTIC),
                        new Category("Equity: World", Constants.COMBINED_EQUITY_WORLD)),
                new Category("Hybrid", Constants.COMBINED_HYBRID),
                new Category("Bond: Total", Constants.COMBINED_BOND_TOTAL,
                        new Category("Bond: Taxable", Constants.COMBINED_BOND_TAXABLE),
                        new Category("Bond: Municipal", Constants.COMBINED_BOND_MUNICIPAL)),
                new Category("Commodities", Constants.COMBINED_COMMODITIES)
        ).addDataPoints(new DataPointGroup( Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.Flows )
        ).createReport();
    }
}
