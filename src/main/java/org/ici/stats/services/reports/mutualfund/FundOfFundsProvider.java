package org.ici.stats.services.reports.mutualfund;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Provider;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.DataPointGroup;
import org.ici.stats.domain.Report;
import org.ici.stats.domain.builders.ReportBuilder;
import org.ici.stats.utils.Constants;

public class FundOfFundsProvider implements Provider<Report> {
    @Override
    public Report get() {
        ReportBuilder builder = new ReportBuilder().setType( ReportType.FOF );
        builder.setStartDate( "01/2000" );
        return builder.addCategories(
                new Category("Fund of Funds", 60,
                        new Category("Equity", 61),
                        new Category("Hybrid and Bond", 62))
        ).addDataPoints(new DataPointGroup(Constants.ASSETS_TEXT, DataPoint.TotalNetAssets, DataPoint.NetNewCashFlow )
        ).setDataPointFieldMap(
                new ImmutableMap.Builder<DataPoint,String>()
                        .put( DataPoint.TotalNetAssets, "tna_total" )
                        .put( DataPoint.NumberOfFunds, "tna_total" )
                        .put( DataPoint.NetNewCashFlow, "tna_total" ).build()
        ).setFootNote("* Funds of funds are mutual funds that invest primarily in other mutual funds.").createReport();
    }
}
