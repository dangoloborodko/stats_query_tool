package org.ici.stats.services;

import org.ici.stats.domain.ReportRequest;
import org.ici.stats.domain.ReportResult;

import java.util.List;

public interface MFSpecialReports {
    ReportResult getData(ReportRequest request);
}
