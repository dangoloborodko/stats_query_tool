package org.ici.stats.services.impl;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.*;
import org.ici.stats.services.MFSpecialReports;
import org.ici.stats.services.ReportDefinitionService;
import org.ici.stats.utils.DateUtils;
import org.ici.stats.dao.QuickQueryDao;
import org.ici.stats.db.domain.Groupiob12;
import org.ici.stats.db.domain.Groupiob12Microiob12Key;
import org.ici.stats.db.domain.Quickquery12Special;
import org.ici.stats.domain.*;
import org.ici.stats.utils.ReportRowUtils;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.*;

import static org.ici.stats.services.ReportUtils.getAdjustedTime;

public class MFSpecialReportsImpl implements MFSpecialReports {

    private final QuickQueryDao quickQueryDao;
    private final ReportDefinitionService reportDefinitionService;

    private static final DateTime Jan2010 = new DateTime( 2010, 1, 31, 0, 0 );

    @Inject
    public MFSpecialReportsImpl(QuickQueryDao quickQueryDao, ReportDefinitionService reportDefinitionService) {
        this.quickQueryDao = quickQueryDao;
        this.reportDefinitionService = reportDefinitionService;
    }

    @Override
    public ReportResult getData(ReportRequest request) {
        final Report report = reportDefinitionService.getReportDefinition( request.getReportType() );
        Set<Integer> iobs = Sets.newHashSet();
        Map<Integer,String> iob2CategoryName = Maps.newHashMap();
        for (Category category : request.getCategories()) {
            final int iob = category.getId();
            iobs.add( iob  );
            iob2CategoryName.put( iob, category.getName() );
        }

        List<Quickquery12Special> specials;
        Set<Integer> microIobs = getMicroIobs( iobs );
        if ( Period.CurrentMonth.equals( request.getPeriod() )) {
            specials = quickQueryDao.getSpecial( DateUtils.beginningOfMonth( request.getEnd() ), DateUtils.endOfMonth( request.getEnd() ), microIobs );
        } else {
            specials = quickQueryDao.getSpecial( request.getStart(), request.getEnd(), microIobs );
        }

        specials = filter3302( specials );
        Map<Integer,Groupiob12> groups = getGroupMap();
        Map<Integer,List<Quickquery12Special>> iob2Specials = Maps.newHashMap();
        for (int iob : iobs) {
            Groupiob12 curGroup = groups.get( iob );
            if ( curGroup == null ) {
                throw new IllegalArgumentException( "unable to find Groupiob12 for iob12: " + iob );
            } else {
                Predicate<Quickquery12Special> specialPredicate = getPredicate(iob, curGroup.getWhereClause() );
                List<Quickquery12Special> filteredSpecials = Lists.newArrayList( Collections2.filter( specials, specialPredicate ));
                iob2Specials.put( iob, filteredSpecials );
            }
        }

        final Map<AccumulatorKey,SpecialAccumulator> summingMap = Maps.newHashMap();
        for (Map.Entry<Integer, List<Quickquery12Special>> iob2SpecialEntry : iob2Specials.entrySet()) {
            final int iob = iob2SpecialEntry.getKey();
            final String name = iob2CategoryName.get( iob );
            List<Quickquery12Special> specialsForIob = iob2SpecialEntry.getValue();
            for (Quickquery12Special quickquery12Special : specialsForIob) {
                final DateTime adjusted = getAdjustedTime( quickquery12Special.getDate(), request.getPeriod() );
                final AccumulatorKey key = new AccumulatorKey( iob, name, adjusted );
                if ( summingMap.containsKey( key )) {
                    SpecialAccumulator accum = summingMap.get( key );
                    accum.add( quickquery12Special );
                } else {
                    SpecialAccumulator newAccum = new SpecialAccumulator();
                    newAccum.add( quickquery12Special );
                    summingMap.put( key, newAccum );
                }
            }
        }

        // convert our summed up map into results
        List<ReportRow> results = convertToTrends( summingMap, request.getPeriod(), request.getDataPoints() );
        ReportRowUtils.sortByCategory( results, report );
        return new ReportResult( request, results, null );
    }

    /**
     * creates a map of iobs to one or more microiobs
     * @return
     */
    private SetMultimap<Integer,Integer> getMicroIobs() {
        SetMultimap<Integer, Integer> iobMicroIobs = HashMultimap.create();
        List<Groupiob12Microiob12Key> groupMicros = quickQueryDao.getGroupMicros();
        for (Groupiob12Microiob12Key groupMicro : groupMicros) {
            iobMicroIobs.put( groupMicro.getGroupId(), groupMicro.getMicroiob12() );
        }
        return iobMicroIobs;
    }

    /**
     * creates a map of a micro iob to one or more iobs
     * @return
     */
    private SetMultimap<Integer,Integer> getMicroIobToIob() {
        SetMultimap<Integer, Integer> iobMicroIobs = HashMultimap.create();
        List<Groupiob12Microiob12Key> groupMicros = quickQueryDao.getGroupMicros();
        for (Groupiob12Microiob12Key groupMicro : groupMicros) {
            iobMicroIobs.put( groupMicro.getMicroiob12(), groupMicro.getGroupId() );
        }
        return iobMicroIobs;
    }

    private Set<Integer> getMicroIobsForIob( int iob ) {
        SetMultimap<Integer,Integer> microIobs = getMicroIobs();
        return microIobs.get( iob );
    }

    private Set<Integer> getMicroIobs(Set<Integer> iobs) {
        SetMultimap<Integer, Integer> iobMicroIobs = getMicroIobs();
        Set<Integer> microIobs = Sets.newHashSet();
        for (int iob : iobs) {
            microIobs.addAll( iobMicroIobs.get( iob ));
        }
        return microIobs;
    }

    private List<ReportRow> convertToTrends(Map<AccumulatorKey, SpecialAccumulator> summingMap, Period period, List<DataPoint> dataPoints ) {
        List<ReportRow> results = Lists.newArrayList();
        for (Map.Entry<AccumulatorKey, SpecialAccumulator> entry : summingMap.entrySet()) {
            int iob12 = entry.getKey().getIob12();
            String category = entry.getKey().getIob12Name();
            int fundCount = (int)entry.getValue().getFundcount_total();
            int month = entry.getKey().getDate().getMonthOfYear();
            if ( Period.Quarterly.equals( period )) {
                month = DateUtils.getQuarterFromMonth( month );
            }
            int year = entry.getKey().getDate().getYear();

            // now for the values
            Map<DataPoint,Long> pointMap = Maps.newHashMap();
            for (DataPoint dataPoint : dataPoints) {
                switch ( dataPoint ) {
                    case TotalNetAssets:
                        pointMap.put( DataPoint.TotalNetAssets, entry.getValue().getTna_total() );
                        break;
                    case NetNewCashFlow:
                        pointMap.put( DataPoint.NetNewCashFlow, createNetNewCashFlow( entry.getValue() ));
                        break;
                    case NetIssuance:
                        pointMap.put( DataPoint.NetIssuance, entry.getValue().getSreg_total() );
                        break;
                    case GrossIssuance:
                        pointMap.put ( DataPoint.GrossIssuance, entry.getValue().getSrein_total() );
                        break;
                    case GrossRedemptions:
                        pointMap.put( DataPoint.GrossRedemptions, entry.getValue().getRreg_total() );
                        break;
                }
            }
            results.add( new ReportRow( category, fundCount, month, year, pointMap ));
        }
        return results;
    }

    private Map<Integer,Groupiob12> getGroupMap() {
        List<Groupiob12> groups = quickQueryDao.getGroupIob12();
        Map<Integer,Groupiob12> result = Maps.newHashMap();
        for (Groupiob12 group : groups) {
            result.put( group.getGroupId(), group );
        }
        return result;
    }

    private static List<Quickquery12Special> filter3302( List<Quickquery12Special> filterMe ) {
        List<Quickquery12Special> results = Lists.newArrayList();
        for (Quickquery12Special f : filterMe) {
            if ( f.getMicroiob12() != 3302  && f.getDate().isAfter( Jan2010 )) {
                results.add( f );
            }
        }
        return results;
    }

    private Predicate<Quickquery12Special> getPredicate(int iob12, String value ) {
        Predicate<Quickquery12Special> wherePredicate = getPredicate( value );
        final Set<Integer> microIobs = getMicroIobsForIob( iob12 );
        Predicate<Quickquery12Special> iobPredicate = new Predicate<Quickquery12Special>() {
            @Override
            public boolean apply(Quickquery12Special special) {
                return microIobs.contains( special.getMicroiob12() );
            }
        };
        return Predicates.and( iobPredicate, wherePredicate );
    }

    private static Predicate<Quickquery12Special> getPredicate( String value ) {
        switch ( value ) {
            case "fof = 'n'":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "n".equalsIgnoreCase( special.getFof() );
                    }
                };
            case "fof != 'n'":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return !"n".equalsIgnoreCase(special.getFof());
                    }
                };
            case "index_fund = 'n'  and fof = 'n'":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "n".equalsIgnoreCase(special.getFof()) && "n".equalsIgnoreCase(special.getIndexFund());
                    }
                };
            case "index_fund = 'y' and fof = 'n'":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "n".equalsIgnoreCase(special.getFof()) && "y".equalsIgnoreCase(special.getIndexFund());
                    }
                };
            case "index_fund = 'y' and indexCode = 1 and fof = 'n'":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "n".equalsIgnoreCase(special.getFof()) && "y".equalsIgnoreCase(special.getIndexFund()) && special.getIndexcode() == 1;
                    }
                };
            case "index_fund = 'y' and indexCode != 1 and fof = 'n'":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "n".equalsIgnoreCase( special.getFof() ) && "y".equalsIgnoreCase( special.getIndexFund() ) && special.getIndexcode() != 1;
                    }
                };

            case "( lifecycle_fund = 'y' or lifestyle_fund = 'y' )":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "y".equalsIgnoreCase(special.getLifecycleFund()) || "y".equalsIgnoreCase(special.getLifestyleFund());
                    }
                };
            case "lifecycle_fund = 'y'":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "y".equalsIgnoreCase(special.getLifecycleFund());
                    }
                };
            case "(retirement_class = 'y' or mos12 =3)":
                return new Predicate<Quickquery12Special>() {
                    @Override
                    public boolean apply(Quickquery12Special special) {
                        return "y".equalsIgnoreCase(special.getRetirementClass()) || special.getMos12() == 3;
                    }
                };
            case "1=1":
                return Predicates.alwaysTrue();
        }
        throw new IllegalArgumentException( "unable to find predicate for expression: " + value );
    }

    private static long createNetNewCashFlow(SpecialAccumulator accum)  {
        return accum.getSreg_total() - accum.getRreg_total() + accum.getSex_total() - accum.getRex_total();
    }

}
