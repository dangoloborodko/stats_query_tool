package org.ici.stats.services.impl;

import com.google.common.collect.Sets;
import org.apache.poi.hssf.usermodel.*;
import org.joda.time.DateTime;

import java.util.Set;

public class ExportWork {
    private final HSSFWorkbook book;
    private final HSSFCellStyle integerStyle;
    private final HSSFCellStyle monthStyle;
    private final HSSFCellStyle yearStyle;
    private final HSSFCellStyle headerStyle;
    private final HSSFSheet sheet;

    private int rowIdx = 0;
    private HSSFRow currentRow;

    private final Set<Short> setColumns = Sets.newHashSet();

    ExportWork () {
        book = new HSSFWorkbook();
        sheet = book.createSheet();
        integerStyle = book.createCellStyle();
        monthStyle = book.createCellStyle();
        yearStyle = book.createCellStyle();
        headerStyle = book.createCellStyle();
        integerStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat("#,##0") );
        monthStyle.setDataFormat( book.createDataFormat().getFormat( "mmm-yyyy" ) );
        yearStyle.setDataFormat( book.createDataFormat().getFormat( "yyyy" ) );
        HSSFFont font = book.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        headerStyle.setFont( font );
        currentRow = sheet.createRow( 0 );
    }

    public void moveToRow( int row ) {
        rowIdx = row;
    }

    public void nextRow() {
        rowIdx++;
        currentRow = sheet.getRow( rowIdx );
        if ( currentRow == null ) {
            currentRow = sheet.createRow( rowIdx );
        }
    }

    public void addCell( int colIdx, String value ) {
        checkColWidth( colIdx );
        currentRow.createCell((short) colIdx).setCellValue( value );
    }

    public void addHeader( int colIdx, String value ) {
        checkColWidth( colIdx );
        HSSFCell c = currentRow.createCell((short) colIdx);
        c.setCellValue( value );
        c.setCellStyle( headerStyle );
    }

    public void addCell( int colIdx, long value ) {
        checkColWidth( colIdx );
        HSSFCell c = currentRow.createCell((short) colIdx);
        c.setCellValue( value );
        c.setCellStyle( integerStyle );
    }

    public void addMonthCell(int colIdx, DateTime date ) {
        checkColWidth( colIdx );
        HSSFCell c = currentRow.createCell((short) colIdx);
        c.setCellValue( date.toDate() );
        c.setCellStyle( monthStyle );
    }

    public void addYearCell(short colIdx, DateTime date ) {
        checkColWidth( colIdx );
        HSSFCell c = currentRow.createCell( colIdx );
        c.setCellValue( date.toDate() );
        c.setCellStyle( yearStyle );
    }

    private void checkColWidth( int colIdx ) {
        if ( !setColumns.contains( colIdx )) {
            short width = 15 * 256;
            sheet.setColumnWidth((short) colIdx, width );
            setColumns.add((short) colIdx);
        }
    }

    public HSSFWorkbook getBook() {
        return book;
    }
}
