package org.ici.stats.services.impl;

import org.ici.stats.dao.EtfReportDAO;
import org.ici.stats.dao.EtfSpecialReportDAO;
import org.ici.stats.dao.TrendsQueryDAO;
import org.ici.stats.dao.impl.EtfActiveIndexReportDAOImpl;
import org.ici.stats.dao.impl.EtfFundOfFundReportDAOImpl;
import org.ici.stats.dao.impl.EtfMfCombinedReportDAOImpl;
import org.ici.stats.domain.*;
import org.ici.stats.services.MFSpecialReports;
import org.ici.stats.services.ReportService;

import javax.inject.Inject;

public class ReportServiceImpl implements ReportService {

    private final TrendsQueryDAO trendsQueryDAO;
    private final EtfReportDAO etfReportDAO;
    private final MFSpecialReports mfSpecialReports;
    private final EtfSpecialReportDAO etfActiveIndexDao;
    private final EtfSpecialReportDAO etfCombinedDao;

    @Inject
    public ReportServiceImpl(TrendsQueryDAO trendsQueryDAO, EtfReportDAO etfReportDAO, MFSpecialReports mfSpecialReports, EtfActiveIndexReportDAOImpl etfActiveIndexDao, EtfMfCombinedReportDAOImpl etfCombinedDao) {
        this.trendsQueryDAO = trendsQueryDAO;
        this.etfReportDAO = etfReportDAO;
        this.mfSpecialReports = mfSpecialReports;
        this.etfActiveIndexDao = etfActiveIndexDao;
        this.etfCombinedDao = etfCombinedDao;
    }

    @Override
    public ReportResult buildReport(ReportRequest request) {
        ReportResult result = null;
        if ( ReportType.MutualFunds.equals( request.getReportType() )) {
            result = trendsQueryDAO.getData( request );
        } else if ( ReportType.SPECIAL_REPORTS.contains( request.getReportType() )) {
            result = mfSpecialReports.getData( request );
        } else if ( ReportType.ETF.equals( request.getReportType() )) {
            result = etfReportDAO.getData( request );
        } else if ( ReportType.ETFActiveIndex.equals( request.getReportType() )) {
            result = etfActiveIndexDao.getData( request );
        } else if ( ReportType.MF_ETF.equals( request.getReportType() )) {
            result = etfCombinedDao.getData( request );
        }
        return result;
    }

}
