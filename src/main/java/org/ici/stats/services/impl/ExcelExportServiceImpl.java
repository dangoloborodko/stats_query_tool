package org.ici.stats.services.impl;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.ReportRequest;
import org.ici.stats.domain.ReportResult;
import org.ici.stats.domain.ReportRow;
import org.ici.stats.services.ExcelExportService;
import org.joda.time.DateTime;

public class ExcelExportServiceImpl implements ExcelExportService {

    /**
     * @param work working set
     * @param result result
     */
    private void populateHeaders(ExportWork work, ReportResult result ) {
        int colIdx = 0;
        work.addHeader( colIdx++, "Date" );
        work.addHeader( colIdx++, "Category" );
        if ( result.isShowFundCounts() ) {
            work.addHeader( colIdx++, result.getFundCountLabel() );
        }
        for (DataPoint point : result.getRequest().getDataPoints()) {
            work.addHeader( colIdx++, point.getFullName() );
        }
    }

    private void setDate(ExportWork info, ReportResult result, ReportRow data, int colIdx) {
        int month;
        switch ( result.getRequest().getPeriod() ) {
            case Monthly:
            case CurrentMonth:
                month = data.getDatePeriod();
                break;
            case Quarterly:
                month = data.getDatePeriod() * 3;
                break;
            default:
                month = 12;
        }
        DateTime rowDate = new DateTime( data.getYear(), month, 1, 0, 0);
        info.addMonthCell( colIdx, rowDate );
    }


    private void populateData(ExportWork info, ReportResult result, ReportRow data) {
        int colIdx = 0;
        info.nextRow();
        setDate( info, result, data, colIdx++ );
        info.addCell( colIdx++, data.getCategory() );
        if ( result.isShowFundCounts() ) {
            info.addCell( colIdx++, data.getFundCount() );
        }
        for (DataPoint point : result.getRequest().getDataPoints() ) {
            Long value = data.getValueMap().get( point );
            info.addCell( colIdx++, value);
        }
    }

    @Override
    public HSSFWorkbook createReport(ReportResult result) {
        ExportWork info = new ExportWork();
        final ReportRequest request = result.getRequest();
        info.addCell( 0, request.getReportType().getName() );
        info.nextRow();
        info.addCell( 0, "All dollar figures are in millions." );
        info.nextRow();
        info.nextRow();
        populateHeaders( info, result );

        for ( ReportRow data : result.getData() ) {
            populateData( info, result, data );
        }

        return info.getBook();
    }
}
