package org.ici.stats.services.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.ici.stats.domain.ReportType;
import org.ici.stats.domain.Report;
import org.ici.stats.services.ReportDefinitionService;
import org.ici.stats.services.reports.etf.ETFActiveIndexProvider;
import org.ici.stats.services.reports.etf.ETFCombinedProvider;
import org.ici.stats.services.reports.etf.ETFProvider;
import org.ici.stats.services.reports.mutualfund.ActiveIndexProvider;
import org.ici.stats.services.reports.mutualfund.AlternativeStrategiesProvider;
import org.ici.stats.services.reports.mutualfund.EmergingMarketsProvider;
import org.ici.stats.services.reports.mutualfund.FloatingRateProvider;
import org.ici.stats.services.reports.mutualfund.FundOfFundsProvider;
import org.ici.stats.services.reports.mutualfund.InflationProtectedProvider;
import org.ici.stats.services.reports.mutualfund.MarketCapitalizationProvider;
import org.ici.stats.services.reports.mutualfund.MutualFundsProvider;
import org.ici.stats.services.reports.mutualfund.RetirementProvider;
import org.ici.stats.services.reports.mutualfund.SectorProvider;
import org.ici.stats.services.reports.mutualfund.TargetDateProvider;

import java.util.List;
import java.util.Map;

@Singleton
public class ReportDefinitionServiceImpl implements ReportDefinitionService {

    private final Map<ReportType,Report> reportMap = Maps.newHashMap();

    @Inject
    ReportDefinitionServiceImpl(
            ActiveIndexProvider activeIndexProvider,
            RetirementProvider retirementProvider,
            TargetDateProvider targetDateProvider,
            FloatingRateProvider floatingRateProvider,
            FundOfFundsProvider fundOfFundsProvider,
            EmergingMarketsProvider emergingMarketsProvider,
            MutualFundsProvider mutualFundsProvider,
            InflationProtectedProvider inflationProtectedProvider,
            MarketCapitalizationProvider marketCapitalizationProvider,
            SectorProvider sectorProvider,
            AlternativeStrategiesProvider alternativeStrategiesProvider,
            ETFProvider etfProvider,
            ETFActiveIndexProvider etfActiveIndexProvider,
            ETFCombinedProvider etfCombinedProvider ) {
        safePutAndUpdate(ReportType.ActiveIndex, activeIndexProvider.get());
        safePutAndUpdate(ReportType.Retirement, retirementProvider.get());
        safePutAndUpdate(ReportType.Target, targetDateProvider.get());
        safePutAndUpdate(ReportType.FloatingRate, floatingRateProvider.get());
        safePutAndUpdate(ReportType.FOF, fundOfFundsProvider.get());
        safePutAndUpdate(ReportType.EmergingMarkets, emergingMarketsProvider.get());
        safePutAndUpdate(ReportType.TIPs, inflationProtectedProvider.get());
        safePutAndUpdate(ReportType.MutualFunds, mutualFundsProvider.get());
        safePutAndUpdate(ReportType.MarketCapitalization, marketCapitalizationProvider.get());
        safePutAndUpdate(ReportType.Sector, sectorProvider.get());
        safePutAndUpdate(ReportType.AlternativeStrategies, alternativeStrategiesProvider.get());
        safePutAndUpdate(ReportType.ETF, etfProvider.get() );
        safePutAndUpdate(ReportType.ETFActiveIndex, etfActiveIndexProvider.get() );
        safePutAndUpdate(ReportType.MF_ETF, etfCombinedProvider.get() );
    }

    private void safePutAndUpdate(ReportType type, Report report) {
        Preconditions.checkArgument( type.equals( report.getReportType() ), "attempted to add report of type: %s using type: %s", report.getReportType(), type );
        reportMap.put( type, report );
    }

    @Override
    public Report getReportDefinition(ReportType type) {
        Report report = reportMap.get( type );
        if ( report == null ) {
            throw new IllegalStateException( "no report defined for: " + type );
        }
        return report;
    }

    @Override
    public List<ReportType> getReportTypes() {
        return ImmutableList.copyOf( ReportType.AVAILABLE_REPORTS );
    }
}
