package org.ici.stats.services;

import org.ici.stats.utils.DateUtils;
import org.ici.stats.domain.Period;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;

public class ReportUtils {

    public static final DecimalFormat NUMBER_FORMAT = new DecimalFormat("#,###");

    public static final DateTimeFormatter MONTH_FORMATTER = DateTimeFormat.forPattern("MMM-yyyy");

    public static DateTime getAdjustedTime(DateTime actual, Period period ) {
        switch( period ){
            case Monthly:
            case CurrentMonth:
                return actual;
            case Quarterly:
                return DateUtils.getMostRecentFullQuarter( actual );
            case Annual:
                return actual.withMonthOfYear( 12 ).withDayOfMonth( 31 );
        }
        throw new IllegalArgumentException( "unable to handle period: " + period );
    }

    public static String getDateLabel(Period period) {
        switch (period) {
            case Annual:
                return "Year";
            case Monthly:
            case CurrentMonth:
                return "Month";
            case Quarterly:
                return "Quarter";
        }
        return null;
    }

    public static String getDisplayDate(Period period, int datePeriod, int year) {
        switch (period) {
            case CurrentMonth:
            case Monthly:
                DateTime monthEnd = DateUtils.endOfMonth(datePeriod, year);
                return MONTH_FORMATTER.print(monthEnd);
            case Quarterly:
                return datePeriod + "Q-" + year;
            case Annual:
                return Integer.toString(year);
        }
        throw new IllegalArgumentException("unable to handle period: " + period);
    }


}
