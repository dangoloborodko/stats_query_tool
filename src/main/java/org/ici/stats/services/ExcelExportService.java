package org.ici.stats.services;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.ici.stats.domain.ReportResult;

public interface ExcelExportService {
    HSSFWorkbook createReport( ReportResult report );
}
