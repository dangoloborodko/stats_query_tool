package org.ici.stats.servlets;

import com.google.inject.Singleton;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.CurrentSelection;
import org.ici.stats.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.ici.stats.servlets.ServletUtils.forward;
import static org.ici.stats.servlets.ServletUtils.showJsp;
import static org.ici.stats.servlets.ServletUtils.getCurrentSelection;

@Singleton
public class EtfObjectivesServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(EtfObjectivesServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        logger.info( "handling etf objectives request");
        CurrentSelection currentSelection = getCurrentSelection(request);
        // if we haven't selected a report, need to go to the first page
        if (currentSelection.getReport() == null) {
            resp.sendRedirect(request.getContextPath() + "/");
            return;
        }

        handleCategory(request);

        String actionParam = request.getParameter("action");
        if ("next".equalsIgnoreCase(actionParam)) {
            // make sure something was selected
            if (currentSelection.getCategories() == null || currentSelection.getCategories().isEmpty()) {
                request.setAttribute("errorMessage", "At least one objective is required");
                showJsp(request, resp, "/WEB-INF/pages/etf_objectives.jsp");
            } else {
                forward(request, resp, "/dates");
            }
        } else {
            showJsp(request, resp, "/WEB-INF/pages/etf_objectives.jsp");
        }
    }

    private void handleCategory(HttpServletRequest request) {
        ServletUtils.updateCategoryFromRequest( request );
        CurrentSelection currentSelection = getCurrentSelection(request);
        List<Category> categories = currentSelection.getReport().getCategories();
        request.setAttribute(Constants.CATEGORIES, categories);
    }
}
