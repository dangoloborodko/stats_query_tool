package org.ici.stats.servlets;

import java.util.List;

public class ELFunctions {

    public static boolean contains(List list, Object o) {
        return (list != null) && (o != null) && list.contains(o);
    }

    public static String jsSafe( String makeMeSafe ) {
        return makeMeSafe.replace(" ", "" );
    }
}
