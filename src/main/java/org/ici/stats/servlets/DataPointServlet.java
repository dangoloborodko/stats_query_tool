package org.ici.stats.servlets;

import com.google.common.collect.Lists;
import com.google.inject.Singleton;
import org.ici.stats.domain.*;
import org.ici.stats.domain.CurrentSelection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Singleton
public class DataPointServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // ok, now to redirect to where we need to go
        String actionParam = req.getParameter( "action" );
        if ( "next".equalsIgnoreCase( actionParam )) {
            updateDataPoints( req );
            // need to switch this up depending on what we're working with
            CurrentSelection currentSelection = ServletUtils.getCurrentSelection(req);
            ReportType type = currentSelection.getReport().getReportType();
            if (ReportType.MutualFunds.equals( type )) {
                resp.sendRedirect( req.getContextPath() + "/mf/objectives");
            } else if ( ReportType.ETF.equals( type )) {
                resp.sendRedirect( req.getContextPath() + "/etf/objectives");
            } else {
                resp.sendRedirect( req.getContextPath() + "/specialty/objectives");
            }
        } else {
            req.setAttribute("monthly", ReportType.MutualFunds);
            req.setAttribute("monthlySpecialty", ReportType.SORTED_SPECIAL_REPORTS);
            req.getRequestDispatcher("/WEB-INF/pages/datapoints.jsp" ).forward( req, resp );
        }
    }

    private void updateDataPoints( HttpServletRequest req ) {
        // see if we have any data points that need saving
        CurrentSelection currentSelection = ServletUtils.getCurrentSelection(req);
        String[] dataPointsParam = req.getParameterValues("dataPoints");
        List<DataPoint> points;
        if (dataPointsParam == null || dataPointsParam.length == 0) {
            points = Lists.newArrayList();
        } else {
            points = DataPoint.getDataPoints(dataPointsParam);
        }
        currentSelection.setDataPoints( points );

        // see if we need to clear out and of our existing selection based on the current report
        // needed since when we change the report, some of the old data points are submitted at the same time
        updateDataPointsBasedOnReport( currentSelection.getDataPoints(), currentSelection.getReport() );
        updateCategoriesBasedOnReport( currentSelection.getCategories(), currentSelection.getReport() );
        updateMOSBasedOnReport( currentSelection.getMethodsOfSale(), currentSelection.getReport() );

        ServletUtils.updateCurrentSelection( req, currentSelection );
    }

    private static void updateCategoriesBasedOnReport(List<Category> categories, Report report) {
        Iterator<Category> catIter = categories.iterator();
        List<Category> reportCats = report.getCategories();
        while( catIter.hasNext() ) {
            Category curCat = catIter.next();
            if ( !reportCats.contains( curCat )) {
                catIter.remove();
            }
        }
    }

    private static void updateMOSBasedOnReport(List<MethodOfSale> methodsOfSale, Report report) {
        Iterator<MethodOfSale> mosIter = methodsOfSale.iterator();
        List<MethodOfSale> reportMos = report.getMethodsOfSale();
        while( mosIter.hasNext() ) {
            MethodOfSale curMos = mosIter.next();
            if ( !reportMos.contains( curMos )) {
                mosIter.remove();
            }
        }
    }

    private static void updateDataPointsBasedOnReport(List<DataPoint> points, Report currentReport) {
        Iterator<DataPoint> pointIter = points.iterator();
        List<DataPoint> reportPoints = currentReport.getAllDataPoints();
        while( pointIter.hasNext() ) {
            DataPoint curPoint = pointIter.next();
            if ( !reportPoints.contains( curPoint )) {
                pointIter.remove();
            }
        }
    }
}
