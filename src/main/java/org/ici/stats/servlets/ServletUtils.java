package org.ici.stats.servlets;

import com.google.common.collect.Lists;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.CurrentSelection;
import org.ici.stats.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ServletUtils {

    private static final Logger logger = LoggerFactory.getLogger(ServletUtils.class);

    private static final String CURRENT_SELECTION = "currentSelection";

    public static final void forward(HttpServletRequest req, HttpServletResponse resp, String path ) throws IOException {
        resp.sendRedirect( req.getContextPath() + path );
    }

    public static final void showJsp(HttpServletRequest request, HttpServletResponse resp, String jsp ) throws ServletException, IOException {
        request.getRequestDispatcher( jsp ).forward( request, resp );
    }

    public static void updateCategoryFromRequest(HttpServletRequest request) {
        CurrentSelection currentSelection = getCurrentSelection(request);
        // add any selected categories to our list
        String[] categoriesParam = request.getParameterValues(Constants.SELECTED_CATEGORIES);
        if (categoriesParam != null && categoriesParam.length != 0) {
            List<Category> categories = Lists.newArrayList();
            for (String catParam : categoriesParam) {
                Category foundCat = currentSelection.getReport().getCategoryByName(catParam);
                if (foundCat == null) {
                    logger.warn("unable to find category: {}", catParam);
                } else {
                    categories.add(foundCat);
                }
            }
            currentSelection.setCategories(categories);
        }
        updateCurrentSelection(request, currentSelection);
    }

    public static CurrentSelection getCurrentSelection(HttpServletRequest req ) {
        HttpSession session = req.getSession();
        CurrentSelection currentSelection = new CurrentSelection();
        if ( !session.isNew() && session.getAttribute( CURRENT_SELECTION ) != null ) {
            currentSelection  = (CurrentSelection) session.getAttribute(CURRENT_SELECTION);
        }
        return currentSelection;
    }

    public static void updateCurrentSelection(HttpServletRequest req, CurrentSelection selection ) {
        HttpSession s = req.getSession();
        s.setAttribute( CURRENT_SELECTION, selection );
    }
}
