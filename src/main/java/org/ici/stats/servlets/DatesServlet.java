package org.ici.stats.servlets;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.ici.stats.utils.CategoryUtils;
import org.ici.stats.utils.DateUtils;
import org.ici.stats.dao.ConfigDAO;
import org.ici.stats.domain.*;
import org.ici.stats.domain.CurrentSelection;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static org.ici.stats.servlets.ServletUtils.getCurrentSelection;

@Singleton
public class DatesServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger( DatesServlet.class );

    private final ConfigDAO configDAO;

    @Inject
    public DatesServlet(ConfigDAO configDAO) {
        this.configDAO = configDAO;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        CurrentSelection selection = getCurrentSelection(request);
        // if we haven't selected a report, need to go to the first page
        if (selection.getReport() == null) {
            resp.sendRedirect(request.getContextPath() +"/");
            return;
        }

        DateTime currentMonth = null;
        try {
            currentMonth = configDAO.getCurrentTrendsDate();
        } catch (SQLException e) {
            logger.error( "error getting trends date", e );
            resp.sendError( 500 );
        }

        String period = request.getParameter( "reportPeriod" );
        // if period isn't null, we're updating our selection
        if ( period != null ) {
            Period p = null;
            switch (period) {
                case "current":
                    p = Period.CurrentMonth;
                    break;
                case "annual":
                    p = Period.Annual;
                    break;
                case "quarterly":
                    p = Period.Quarterly;
                    break;
                case "range":
                    p = Period.Monthly;
                    break;
            }
            selection.setPeriod(p);

            if ( !Period.Monthly.equals( p )) {
                DateTime start = selection.getReport().getStartDate();
                selection.setStartDate( start );
                selection.setEndDate( currentMonth );
            } else {
                String fromMonthParam = request.getParameter( "fromMonth" );
                String fromYearParam = request.getParameter( "fromYear" );
                Integer fromMonth = Ints.tryParse( fromMonthParam );
                Integer fromYear = Ints.tryParse( fromYearParam );
                if ( fromMonth != null && fromYear != null ) {
                    DateTime start = DateUtils.endOfMonth( fromMonth, fromYear );
                    selection.setStartDate( start );
                }
                String toMonthParam = request.getParameter( "toMonth" );
                String toYearParam = request.getParameter( "toYear" );
                Integer toMonth = Ints.tryParse( toMonthParam );
                Integer toYear = Ints.tryParse( toYearParam );
                if ( toMonth != null && toYear != null ) {
                    DateTime end = DateUtils.endOfMonth( toMonth, toYear );
                    selection.setEndDate( end );
                }
            }
            ServletUtils.updateCurrentSelection( request, selection );
        }

        String actionParam = request.getParameter( "action" );
        if ( "run".equalsIgnoreCase( actionParam )) {
            resp.sendRedirect( request.getContextPath() +"/report");
        } else {
            DateTime earliestDate = CategoryUtils.getEarliestAvailableDate( selection.getCategories() );
            if ( earliestDate == null ) {
                earliestDate = selection.getReport().getStartDate();
            }
            // populate our available years
            List<Integer> years = Lists.newArrayList();
            for (int curYear = earliestDate.getYear(); curYear <= currentMonth.getYear() ; curYear++) {
                years.add( curYear );
            }
            request.setAttribute( "years", years );

            List<String> months = DateUtils.months();
            request.setAttribute( "months", months );

            request.setAttribute( "reportPeriod", period );
            request.setAttribute( "currentMonth", DateUtils.mm_dd_yyyy.print( currentMonth ));
            request.setAttribute( "firstQuarter", DateUtils.mmm_yyyy.print( earliestDate ));
            request.setAttribute( "currentQuarter", DateUtils.mmm_yyyy.print( currentMonth ));
            request.setAttribute( "firstAnnual", DateUtils.mmm_yyyy.print( earliestDate ));
            request.setAttribute( "currentAnnual", DateUtils.mmm_yyyy.print( currentMonth ));
            request.getRequestDispatcher("/WEB-INF/pages/dates.jsp" ).forward( request, resp );
        }
    }
}
