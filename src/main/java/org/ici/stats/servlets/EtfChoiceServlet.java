package org.ici.stats.servlets;

import com.google.common.base.Strings;
import com.google.inject.Singleton;
import org.ici.stats.domain.*;
import org.ici.stats.services.ReportDefinitionService;
import org.ici.stats.domain.CurrentSelection;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static org.ici.stats.servlets.ServletUtils.forward;
import static org.ici.stats.servlets.ServletUtils.getCurrentSelection;
import static org.ici.stats.servlets.ServletUtils.updateCurrentSelection;

@Singleton
public class EtfChoiceServlet extends HttpServlet {

    private final ReportDefinitionService reportDefinitionService;

    @Inject
    public EtfChoiceServlet(ReportDefinitionService reportDefinitionService) {
        this.reportDefinitionService = reportDefinitionService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // see if we have any data points that need saving
        CurrentSelection currentSelection = getCurrentSelection(req);
        String reportTypeParam = req.getParameter("reportType");
        boolean reportTypeChanged = !Strings.isNullOrEmpty( reportTypeParam );
        if ( reportTypeParam == null ) {
            if ( currentSelection.getReport() != null ) {
                reportTypeParam = currentSelection.getReport().getReportType().getXmlId();
            } else {
                reportTypeParam = ReportType.ETF.getXmlId();
            }
        }

        // we're changing our report type
        ReportType reportType = ReportType.getReportByXmlId(reportTypeParam);
        // get our options for the selected report
        Report report = reportDefinitionService.getReportDefinition(reportType);

        if (reportTypeChanged) {
            // see if we need to clear out and of our existing selection based on the current report
            // needed since when we change the report, some of the old data points are submitted at the same time
            updateDataPointsBasedOnReport( currentSelection.getDataPoints(), report );
            updateCategoriesBasedOnReport( currentSelection.getCategories(), report );
            updateMOSBasedOnReport( currentSelection.getMethodsOfSale(), report );
            currentSelection.setReport( report );
            updateCurrentSelection( req, currentSelection );
            forward( req, resp, "/datapoints" );
        } else {
            // ok, now to redirect to where we need to go
            req.setAttribute("etf", ReportType.ETF);
            req.setAttribute("etfSpecialty", ReportType.ETF_SPECIAL_REPORTS );
            req.getRequestDispatcher("/WEB-INF/pages/etf_choice.jsp" ).forward( req, resp );
        }
    }

    private static void updateCategoriesBasedOnReport(List<Category> categories, Report report) {
        Iterator<Category> catIter = categories.iterator();
        List<Category> reportCats = report.getCategories();
        while (catIter.hasNext()) {
            Category curCat = catIter.next();
            if (!reportCats.contains(curCat)) {
                catIter.remove();
            }
        }
    }

    private static void updateMOSBasedOnReport(List<MethodOfSale> methodsOfSale, Report report) {
        Iterator<MethodOfSale> mosIter = methodsOfSale.iterator();
        List<MethodOfSale> reportMos = report.getMethodsOfSale();
        while (mosIter.hasNext()) {
            MethodOfSale curMos = mosIter.next();
            if (!reportMos.contains(curMos)) {
                mosIter.remove();
            }
        }
    }

    private static void updateDataPointsBasedOnReport(List<DataPoint> points, Report currentReport) {
        Iterator<DataPoint> pointIter = points.iterator();
        List<DataPoint> reportPoints = currentReport.getAllDataPoints();
        while (pointIter.hasNext()) {
            DataPoint curPoint = pointIter.next();
            if (!reportPoints.contains(curPoint)) {
                pointIter.remove();
            }
        }
    }
}
