package org.ici.stats.servlets;

import com.google.common.collect.Lists;
import com.google.inject.Singleton;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.CurrentSelection;
import org.ici.stats.domain.Level;
import org.ici.stats.domain.MethodOfSale;
import org.ici.stats.utils.CategoryUtils;
import org.ici.stats.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.ici.stats.servlets.ServletUtils.forward;
import static org.ici.stats.servlets.ServletUtils.showJsp;

@Singleton
public class MutualFundObjectivesServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(MutualFundObjectivesServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        CurrentSelection currentSelection = ServletUtils.getCurrentSelection(request);
        // if we haven't selected a report, need to go to the first page
        if (currentSelection.getReport() == null) {
            resp.sendRedirect(request.getContextPath() + "/");
            return;
        }

        handleCategory(request, resp);
        handleMos(request, resp);

        String actionParam = request.getParameter("action");
        if ("next".equalsIgnoreCase(actionParam)) {
            // make sure something was selected
            if (currentSelection.getCategories() == null || currentSelection.getCategories().isEmpty()) {
                request.setAttribute("errorMessage", "At least one objective is required");
                showJsp(request, resp, "/WEB-INF/pages/mf_objectives.jsp");
            } else {
                forward(request, resp, "/dates");
            }
        } else {
            showJsp(request, resp, "/WEB-INF/pages/mf_objectives.jsp");
        }
    }

    private void handleMos(HttpServletRequest request, HttpServletResponse resp) {
        CurrentSelection currentSelection = ServletUtils.getCurrentSelection(request);
        String[] mosNames = request.getParameterValues(Constants.SELECTED_MOS);
        if (mosNames != null) {
            List<MethodOfSale> methodsOfSale = Lists.newArrayList();
            for (String mosName : mosNames) {
                MethodOfSale mos = currentSelection.getReport().getMethodOfSaleByName(mosName);
                if (mos == null) {
                    logger.warn("unable to find mos: {}", mosName);
                } else {
                    methodsOfSale.add(mos);
                }
            }
            currentSelection.setMethodsOfSale(methodsOfSale);
            ServletUtils.updateCurrentSelection(request, currentSelection);
        }
    }

    private void handleCategory(HttpServletRequest request, HttpServletResponse resp) {
        String categoryParam = request.getParameter(Constants.CATEGORY_LEVEL);
        if (categoryParam == null) {
            categoryParam = Constants.MACRO_CATEGORY;
        }

        ServletUtils.updateCategoryFromRequest(request);
        CurrentSelection currentSelection = ServletUtils.getCurrentSelection(request);

        List<Category> categories = Lists.newArrayList();
        if (Constants.MACRO_CATEGORY.equalsIgnoreCase(categoryParam)) {
            categories = CategoryUtils.categoriesAtLevel(currentSelection.getReport().getCategories(), Level.High);
        } else if (Constants.COMPOSITE_CATEGORY.equalsIgnoreCase(categoryParam)) {
            categories = CategoryUtils.categoriesAtLevel(currentSelection.getReport().getCategories(), Level.Mid);
        } else if (Constants.OBJECTIVES_CATEGORY.equalsIgnoreCase(categoryParam)) {
            categories = CategoryUtils.categoriesWithoutLevel(currentSelection.getReport().getCategories());
        } else if (Constants.HIERARCHY_CATEGORY.equalsIgnoreCase(categoryParam)) {
            categories = currentSelection.getReport().getCategories();
            request.setAttribute(Constants.HIERARCHY_CATEGORY, "yes");
        }
        request.setAttribute(Constants.CATEGORIES, categories);
        request.setAttribute(Constants.CATEGORY_LEVEL, categoryParam);
    }
}
