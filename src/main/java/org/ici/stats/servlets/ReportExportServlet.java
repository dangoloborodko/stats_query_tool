package org.ici.stats.servlets;

import com.google.inject.Singleton;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.ici.stats.dao.ConfigDAO;
import org.ici.stats.domain.Period;
import org.ici.stats.domain.ReportRequest;
import org.ici.stats.domain.builders.ReportRequestBuilder;
import org.ici.stats.services.ExcelExportService;
import org.ici.stats.domain.ReportResult;
import org.ici.stats.services.ReportService;
import org.ici.stats.domain.CurrentSelection;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static org.ici.stats.servlets.ServletUtils.getCurrentSelection;

@Singleton
public class ReportExportServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger( ReportExportServlet.class );

    private final ReportService reportService;
    private final ExcelExportService excelExportService;
    private final ConfigDAO configDAO;

    @Inject
    public ReportExportServlet(ReportService reportService, ExcelExportService excelExportService, ConfigDAO configDAO) {
        this.reportService = reportService;
        this.excelExportService = excelExportService;
        this.configDAO = configDAO;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

        CurrentSelection selection = getCurrentSelection(request);
        // if we haven't selected a report, need to go to the first page
        if (selection.getReport() == null) {
            resp.sendRedirect(request.getContextPath() +"/");
            return;
        }


        // create our report request from the session information
        ReportRequestBuilder builder = new ReportRequestBuilder( selection.getReport().getReportType() );
        builder.setPeriod( selection.getPeriod() );

        DateTime start = selection.getStartDate();
        DateTime end = selection.getEndDate();
        if ( Period.CurrentMonth.equals( selection.getPeriod())) {
            try {
                start = end = configDAO.getCurrentTrendsDate();
            } catch (SQLException e) {
                logger.error( "error getting current trends month", e );
                resp.sendError( 500, "error getting current trends month");
                return;
            }
        }

        builder.setStart( start );
        builder.setEnd( end );
        builder.setCategories( selection.getCategories() );
        builder.setDataPoints( selection.getDataPoints() );
        builder.setMethodsOfSale( selection.getMethodsOfSale() );
        ReportRequest reportRequest = builder.createReportRequest();

        ReportResult result = reportService.buildReport( reportRequest );

        HSSFWorkbook book = excelExportService.createReport( result );
        resp.setHeader("Content-disposition","attachment; filename=trends.xls");
        resp.setContentType( "application/vnd.ms-excel");
        book.write( resp.getOutputStream() );
    }
}
