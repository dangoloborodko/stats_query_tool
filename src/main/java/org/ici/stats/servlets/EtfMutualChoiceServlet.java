package org.ici.stats.servlets;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.ici.stats.domain.CurrentSelection;
import org.ici.stats.domain.ReportType;
import org.ici.stats.services.ReportDefinitionService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.ici.stats.servlets.ServletUtils.forward;

@Singleton
public class EtfMutualChoiceServlet  extends HttpServlet {

    private final ReportDefinitionService reportDefinitionService;

    @Inject
    public EtfMutualChoiceServlet(ReportDefinitionService reportDefinitionService) {
        this.reportDefinitionService = reportDefinitionService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest( req, resp );
    }

    private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String choice = req.getParameter( "choice" );
        if ( choice != null ) {
            CurrentSelection currentSelection = new CurrentSelection();
            if ( "etf".equalsIgnoreCase( choice )) {
                currentSelection.setReport( reportDefinitionService.getReportDefinition( ReportType.ETF ));
                ServletUtils.updateCurrentSelection( req, currentSelection );
                forward( req, resp, "/etf" );
                return;
            } else if ( "mutual".equalsIgnoreCase( choice )) {
                currentSelection.setReport( reportDefinitionService.getReportDefinition( ReportType.MutualFunds ));
                ServletUtils.updateCurrentSelection( req, currentSelection );
                forward(  req, resp, "/mutual" );
                return;
            }
        }
        req.getRequestDispatcher("/WEB-INF/pages/mf_etf_choice.jsp" ).forward( req, resp );
    }
}
