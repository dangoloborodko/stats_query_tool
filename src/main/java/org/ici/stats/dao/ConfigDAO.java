package org.ici.stats.dao;

import org.joda.time.DateTime;

import java.sql.SQLException;

/**
 * Created by jalexander on 10/28/2014.
 */
public interface ConfigDAO {
    DateTime getCurrentTrendsDate() throws SQLException;
}
