package org.ici.stats.dao;

import org.ici.stats.db.domain.Groupiob12;
import org.ici.stats.db.domain.Groupiob12Microiob12Key;
import org.ici.stats.db.domain.Quickquery12Special;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Set;

public interface QuickQueryDao {
    List<Groupiob12> getGroupIob12();
    List<Quickquery12Special> getSpecial( DateTime start, DateTime end, Set<Integer> microIobs );

    List<Groupiob12Microiob12Key> getGroupMicros();
}
