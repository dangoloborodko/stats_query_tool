package org.ici.stats.dao;

import org.ici.stats.domain.*;

import java.util.List;

public interface TrendsQueryDAO {
    ReportResult getData( ReportRequest request );
}
