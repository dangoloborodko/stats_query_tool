package org.ici.stats.dao.builders;

public class MRangeSqlBuilder extends AbstractSqlBuilder {
    public MRangeSqlBuilder() {
        super( "iob12" );
    }

    public String getOrderBy() {
        return " ORDER BY datepart(yy," + getDateField() + ") ";
    }
}
