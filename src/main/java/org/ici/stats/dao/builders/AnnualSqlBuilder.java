package org.ici.stats.dao.builders;

public class AnnualSqlBuilder extends AbstractSqlBuilder {
    public String getLastTimeSelect(String field) {
        return getLastTimeSelect(field, field);
    }

    public String getGroupBy() {
        return " GROUP BY " + getYear() + " ";
    }

    public String getTimePeriod() {
        return "'1'";
    }

    public AnnualSqlBuilder() {
        super();
    }

    public AnnualSqlBuilder(String categoryField) {
        super(categoryField);
    }

    /* (non-Javadoc)
     * @see org.ici.icinet.stats.dao.impl.SyBaseStatisticsQueryDAOImpl.SqlBuilder#getLastTimeSelect(java.lang.String, java.lang.String)
     */
    public String getLastTimeSelect(String field, String equation) {
        return "SUM(convert(float, (CASE datepart(mm, " + getDateField() + ") WHEN 12 THEN " + equation
                + " ELSE (CASE datepart(yy, " + getDateField() + ") WHEN datepart(yy,convert(datetime,value)) "
                + "THEN (CASE datepart(mm, " + getDateField() + ") WHEN datepart(mm,convert(datetime,value)) THEN "
                + equation + " ELSE 0 END) ELSE 0 END ) END))) AS " + field;
    }
}
