package org.ici.stats.dao.builders;

public interface SqlBuilder {

    public enum SelectorType { Fund, Class }

    public String getLastTimeSelect(String field);

    public String getSumSelect(String field);

    public String getLastTimeSelect(String field, String equation);

    public String getSumSelect(String field, String equation);

    public String getSumSelectorString(SelectorType type, boolean validForMOS);

    public String getDateSelect();

    public String getGroupBy();

    public String getWhere(String categories);

    public String getMOSWhere(String mos);

    public String getOrderBy();

    public String getTimePeriod();

    public String getYear();
}
