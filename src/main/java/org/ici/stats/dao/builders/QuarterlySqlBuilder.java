package org.ici.stats.dao.builders;

public class QuarterlySqlBuilder extends AbstractSqlBuilder {
    public QuarterlySqlBuilder() {
        super("iob12");
    }

    public String getLastTimeSelect(String field, String equation) {
        return "SUM(convert(float, case "
                + " when (datepart(mm," + getDateField() + ") in (3,6,9,12)) then " + equation
                + " when (datepart(mm," + getDateField() + ") not in (3,6,9,12)) then case"
                + " when (datepart(mm," + getDateField() + ") = datepart(mm, convert(datetime, value)) and datepart(yy," + getDateField() + ") = datepart(yy,convert(datetime, value)))"
                + " then " + equation
                + " else 0 end end)) AS " + field;
    }

    public String getTimePeriod() {
        return "datepart(qq," + getDateField() + ")";
    }
}
