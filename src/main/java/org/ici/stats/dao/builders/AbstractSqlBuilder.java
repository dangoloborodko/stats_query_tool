package org.ici.stats.dao.builders;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.utils.Constants;

import java.util.Map;

public abstract class AbstractSqlBuilder implements SqlBuilder {

    private final static ImmutableMap<DataPoint,String> dataPointFieldMap = ImmutableMap.<DataPoint,String>builder()
            .put( DataPoint.TotalNetAssets, "tna" )
            .put( DataPoint.NumberOfFunds, "entities" )
            .put( DataPoint.LiquidAssets, "ltot" )
            .put( DataPoint.TotalSales, "total_sales" )
            .put( DataPoint.NewSales, "sreg" )
            .put( DataPoint.ReinvestedDividends, "srein" )
            .put( DataPoint.Redemptions, "rreg" )
            .put( DataPoint.NetSales, "net_sales")
            .put( DataPoint.NetNewSales, "net_new_sales" )
            .put( DataPoint.NetNewCashFlow, "net_new_cash" )
            .put( DataPoint.SalesExchanges, "sex" )
            .put( DataPoint.RedemptionExchanges, "rex" )
            .put( DataPoint.NetExchanges, "net_exch" )
            .put( DataPoint.CommonStockPurchases, "csppurchases" )
            .put( DataPoint.CommonStockSales, "cspsales")
            .put( DataPoint.NetCommonStockPurchases, "csp_net" )
            .put( DataPoint.OtherPortfolioPurchases, "othppurchases" )
            .put( DataPoint.OtherPortfolioSales, "othpsales" )
            .put( DataPoint.NetOtherPortfolioPurchases, "othp_net" )
            .put( DataPoint.AllPortfolioPurchases, "mctotp" )
            .put( DataPoint.AllPortfolioSales, "mctots" )
            .put( DataPoint.NetPortfolioPurchases, "mctot_net" ).build();

    private final static ImmutableMap<DataPoint,String> dataPointEquationMap = ImmutableMap.<DataPoint,String>builder()
            .put( DataPoint.NetSales, "sreg + srein - rreg")
            .put( DataPoint.TotalSales, "sreg + srein")
            .put( DataPoint.NetNewSales, "sreg - rreg")
            .put( DataPoint.NetNewCashFlow, "sreg - rreg + sex - rex")
            .put( DataPoint.NetExchanges, "sex - rex").build();

    private final static ImmutableSet<DataPoint> transactionAggregates = ImmutableSet.of(
            DataPoint.TotalSales, DataPoint.NewSales,
            DataPoint.ReinvestedDividends, DataPoint.Redemptions,
            DataPoint.NetSales, DataPoint.NetNewSales, DataPoint.NetNewCashFlow,
            DataPoint.SalesExchanges, DataPoint.RedemptionExchanges,
            DataPoint.NetExchanges, DataPoint.CommonStockPurchases,
            DataPoint.CommonStockSales, DataPoint.NetCommonStockPurchases,
            DataPoint.OtherPortfolioPurchases, DataPoint.OtherPortfolioSales,
            DataPoint.NetOtherPortfolioPurchases, DataPoint.AllPortfolioPurchases,
            DataPoint.AllPortfolioSales, DataPoint.NetPortfolioPurchases
    );



    private String categoryField;
    private String dateField = "date";

    public AbstractSqlBuilder(String categoryField) {
        this.categoryField = categoryField;
    }

    public AbstractSqlBuilder() {
        this.categoryField = null;
    }

    public String getLastTimeSelect(String field) {
        return getLastTimeSelect(field, field);
    }

    public String getLastTimeSelect(String field, String equation) {
        return getSumSelect(field, equation);
    }

    public final String getDateSelect() {
        return getTimePeriod() + " time_period, " + getYear() + " year";
    }

    @Override
    public String getGroupBy() {
        return " GROUP BY " + getYear() + ", " + getTimePeriod() + " ";
    }

    @Override
    public String getWhere(String categories) {
        return " ,app_properties "
                + " WHERE " + categoryField + " IN ( " + categories + ") "
                + "AND name='" + Constants.APP_TREND_CUR_DATE + "' "
                + "AND " + dateField + " BETWEEN ? AND ? ";
    }

    @Override
    public String getSumSelect(String field) {
        return getSumSelect(field, field);
    }

    @Override
    public String getSumSelect(String field, String equation) {
        return "sum(convert(float, " + equation + ")) " + field;
    }

    @Override
    public String getYear() {
        return "datepart(yy, " + dateField + ")";
    }


    @Override
    public String getTimePeriod() {
        return "datepart(mm, " + dateField + ") ";
    }

    private static boolean isTransactionAggregate( DataPoint dataPoint ) {
        return transactionAggregates.contains( dataPoint );
    }

    @Override
    public String getSumSelectorString(SelectorType type, boolean validForMOS) {
        StringBuffer sumBuffer = new StringBuffer();
        for ( Map.Entry<DataPoint, String> dataFieldEntry : dataPointFieldMap.entrySet() ) {
            DataPoint dataPoint = dataFieldEntry.getKey();
            String field = dataFieldEntry.getValue();

            if ( isTransactionAggregate( dataPoint ) && (validForMOS == dataPoint.isMosCompatible() )) {
                String eq = field;
                if ( dataPointEquationMap.containsKey( dataPoint ) ) {
                    String equation = dataPointEquationMap.get( dataPoint );
                    if (SelectorType.Fund.equals(type)) {
                        eq = equation.replaceAll("\\b(\\w+)\\b", dataPoint.isMosCompatible() ? "classes.$1" : "funds.$1");
                    } else {
                        eq = equation;
                    }
                } else {
                    if (SelectorType.Fund.equals(type)) {
                        if ( dataPoint.isMosCompatible() ) {
                            eq = "classes." + eq;
                        } else {
                            eq = "funds." + eq;
                        }
                    }
                }
                sumBuffer.append(getSumSelect(field, eq));
                sumBuffer.append(", ");
            }

        }
        sumBuffer.delete(sumBuffer.length() - 2, sumBuffer.length() - 1);
        return sumBuffer.toString();
    }

    public String getCategoryField() {
        return categoryField;
    }

    public void setCategoryField(String categoryField) {
        this.categoryField = categoryField;
    }

    public String getDateField() {
        return dateField;
    }

    public void setDateField(String dateField) {
        this.dateField = dateField;
    }

    @Override
    public String getOrderBy() {
        return "";
    }

    @Override
    public String getMOSWhere(String mos) {
        return " AND mos12 in (" + mos + ") ";
    }
}
