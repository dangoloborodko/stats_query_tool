package org.ici.stats.dao.builders;

public class CurrentMonthSqlBuilder extends AbstractSqlBuilder {
    public CurrentMonthSqlBuilder() {
        super("iob12");
    }

    public String getWhere(String categories) {
        return " WHERE " + getCategoryField() + " IN (" + categories + ")" +
                "AND " + getDateField() + " = ? ";
    }
}
