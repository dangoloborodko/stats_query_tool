package org.ici.stats.dao.impl;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.ici.stats.dao.ConfigDAO;
import org.ici.stats.utils.Constants;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.SQLException;

public class ConfigDAOImpl implements ConfigDAO {
    private final DataSource dataSource;
    private final static Logger logger = LoggerFactory.getLogger(ConfigDAOImpl.class);

    private static final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern( "MM-dd-yyyy" );

    @Inject
    public ConfigDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public DateTime getCurrentTrendsDate() throws SQLException {
        return getDateTimeProperty( Constants.APP_TREND_CUR_DATE );
    }

    protected DateTime getDateTimeProperty( String key ) throws SQLException {
        String value = getAppProperty( key );
        if ( value != null ) {
            try {
                return dateFormatter.parseDateTime( value );
            } catch ( IllegalArgumentException iae ) {
                logger.error( "unable to parse date from: {}", value, iae );
                throw iae;            }
        } else {
            return null;
        }
    }

    protected String getAppProperty( String key ) throws SQLException {
        QueryRunner qr = new QueryRunner( dataSource );
        try {
            return qr.query( "select value from app_properties where name = ?", new ScalarHandler<String>(){}, key );
        } catch (SQLException e) {
            logger.error( "error getting property: {}", key, e );
            throw e;
        }
    }
}
