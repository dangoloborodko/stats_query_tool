package org.ici.stats.dao.impl;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.QueryRunner;
import org.ici.stats.dao.EtfSpecialReportDAO;
import org.ici.stats.dao.rsh.ETFSpecialReportRSH;
import org.ici.stats.domain.*;
import org.ici.stats.utils.Constants;
import org.ici.stats.utils.ReportRowUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

import static org.ici.stats.dao.DAOUtils.timestamp;

public class EtfMfCombinedReportDAOImpl implements EtfSpecialReportDAO {

    public static final String ETF_COMBINED_EQUITY_DOMESTIC = "Equity: Domestic";
    public static final String ETF_COMBINED_EQUITY_WORLD = "Equity: World";
    public static final String ETF_COMBINED_BOND_TAXABLE = "Bond: Taxable";
    public static final String ETF_COMBINED_BOND_MUNICIPAL = "Bond: Municipal";
    public static final String ETF_COMBINED_EQUITY_TOTAL = "Equity: Total";
    public static final String ETF_COMBINED_BOND_TOTAL = "Bond: Total";

    public static final List<String> NAME_ORDER = Lists.newArrayList(ETF_COMBINED_EQUITY_TOTAL, ETF_COMBINED_EQUITY_DOMESTIC, ETF_COMBINED_EQUITY_WORLD, FundType.Hybrid.name(), ETF_COMBINED_BOND_TOTAL, ETF_COMBINED_BOND_TAXABLE, ETF_COMBINED_BOND_MUNICIPAL, FundType.Commodities.name() );

    private final DataSource dataSource;

    private static final Logger logger = LoggerFactory.getLogger(EtfMfCombinedReportDAOImpl.class);

    enum FundType { Domestic, World, Hybrid, BondTaxable, BondMunicipal, Commodities }

    @Inject
    public EtfMfCombinedReportDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public ReportResult getData(ReportRequest request) {
        List<ReportRow> rows = Lists.newArrayList();

        // build our fund types from our categories
        List<FundType> types = Lists.newArrayList();
        boolean equityTotal = false;
        boolean bondTotal = false;
        for (Category category : request.getCategories()) {
            int value = category.getId();
            switch( value ) {
                case Constants.COMBINED_EQUITY_TOTAL:
                    equityTotal = true;
                    break;
                case Constants.COMBINED_EQUITY_DOMESTIC:
                    types.add( FundType.Domestic );
                    break;
                case Constants.COMBINED_EQUITY_WORLD:
                    types.add( FundType.Domestic );
                    break;
                case Constants.COMBINED_HYBRID:
                    types.add( FundType.Hybrid );
                    break;
                case Constants.COMBINED_BOND_TOTAL:
                    bondTotal = true;
                    break;
                case Constants.COMBINED_BOND_TAXABLE:
                    types.add( FundType.BondTaxable );
                    break;
                case Constants.COMBINED_BOND_MUNICIPAL:
                    types.add( FundType.BondMunicipal );
                    break;
                case Constants.COMBINED_COMMODITIES:
                    types.add( FundType.Commodities );
                    break;
            }
        }


        for (FundType fundType : types ) {
            List<ReportRow> mixed = Lists.newArrayList();
            String catName = getCategoryName( fundType );
            mixed.addAll( getEtfs( catName, request.getStart(), request.getEnd(), request.getPeriod(), fundType ));
            if (!FundType.Commodities.equals(fundType)) {
                mixed.addAll( getMutualFunds( catName, request.getStart(), request.getEnd(), request.getPeriod(), fundType ));
            }
            rows.addAll( ReportRowUtils.combineByCategoryAndDate( mixed, request.getPeriod() ));
        }

        if ( equityTotal ) {
            List<ReportRow> mixed = Lists.newArrayList();
            mixed.addAll( getEtfs(ETF_COMBINED_EQUITY_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.Domestic ));
            mixed.addAll( getMutualFunds(ETF_COMBINED_EQUITY_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.Domestic));
            mixed.addAll( getEtfs(ETF_COMBINED_EQUITY_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.World));
            mixed.addAll( getMutualFunds(ETF_COMBINED_EQUITY_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.World));
            rows.addAll( ReportRowUtils.combineByCategoryAndDate( mixed, request.getPeriod() ));
        } 
        if ( bondTotal ) {
            List<ReportRow> mixed = Lists.newArrayList();
            mixed.addAll( getEtfs(ETF_COMBINED_BOND_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.BondTaxable ));
            mixed.addAll( getMutualFunds(ETF_COMBINED_BOND_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.BondTaxable));
            mixed.addAll( getEtfs(ETF_COMBINED_BOND_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.BondMunicipal));
            mixed.addAll( getMutualFunds(ETF_COMBINED_BOND_TOTAL, request.getStart(), request.getEnd(), request.getPeriod(), FundType.BondMunicipal));
            rows.addAll( ReportRowUtils.combineByCategoryAndDate( mixed, request.getPeriod() ));
        }

        ReportRowUtils.sortByCatName( rows, NAME_ORDER );
        return new ReportResult( request, rows, null );
    }

    private List<ReportRow> getEtfs(String label, DateTime start, DateTime end, Period period, FundType type  ) {
        String iobList = "";
        switch ( type ) {
            case Domestic:
                iobList = " (iob98 in ( 800, 805, 810, 815, 820 ) or ( iob98 = 825 and subiob98 != 829 ))";
                break;
            case World:
                iobList = " iob98 = 830";
                break;
            case Hybrid:
                iobList = " iob98 = 855";
                break;
            case BondTaxable:
                iobList = " iob98 in ( 835, 840, 845, 850 )";
                break;
            case BondMunicipal:
                iobList = " iob98 = 851";
                break;
            case Commodities:
                iobList = " subiob98 = 829";
                break;
        }
        String sql = "select e.date, sum(convert(float,isnull(e.net_issuance,0))) as nncf, " +
                "           sum(convert(float,isnull(e.tna,0))) as tna, sum(e.fundcount) as funds, " +
                "           sum(e.classcount) as classcount, sum(e.duplicate_fundcount) as duplicate_fundcount " +
                "from quickquery98_etf e  " +
                "where e.date between ? and ?" +
                "    and fof = 'n' and " + iobList +
                " group by e.date " +
                " order by e.date";

        QueryRunner runner = new QueryRunner(dataSource);
        try {
            return  runner.query(sql, new ETFSpecialReportRSH( label, period, DataPoint.TotalNetAssets, DataPoint.Flows), timestamp(start), timestamp(end));
        } catch (SQLException e) {
            logger.error("error getting combined funds data for sql: {}", sql, e);
            return Lists.newArrayList();
        }
    }

    private List<ReportRow> getMutualFunds(String label, DateTime start, DateTime end, Period period, FundType type  ) {
        String iobList = "";
        switch ( type ) {
            case Domestic:
                iobList = " iob12 < 6";
                break;
            case World:
                iobList = " iob12 >= 6 and iob12 < 11";
                break;
            case Hybrid:
                iobList = " iob12 >= 11 and iob12 < 16";
                break;
            case BondTaxable:
                iobList = " iob12 >= 16 and iob12 < 36";
                break;
            case BondMunicipal:
                iobList = " iob12 >= 36";
                break;
        }
        String sql =
                "select q.date, " +
                "    sum((((isnull(q.sreg,0))+(isnull(q.sex,0)))-((isnull(q.rreg,0))+(isnull(q.rex,0))))) as nncf, " +
                "    sum(convert(float,isnull(q.tna,0))) as tna, " +
                "    sum(q.fundcount) as funds, sum(q.classcount) as classcount, 0 as duplicate_fundcount " +
                "from quickquery12 q  " +
                "where q.iob12 < 40 and q.date between ? and ? and " + iobList +
                "    group by q.date " +
                "    order by q.date";

        QueryRunner runner = new QueryRunner(dataSource);
        try {
            return  runner.query(sql, new ETFSpecialReportRSH( label, period, DataPoint.TotalNetAssets, DataPoint.Flows), timestamp(start), timestamp(end));
        } catch (SQLException e) {
            logger.error("error getting combined funds data for sql: {}", sql, e);
            return Lists.newArrayList();
        }
    }

    private static String getCategoryName( FundType type ) {
        switch ( type ) {
            case Domestic:
                return ETF_COMBINED_EQUITY_DOMESTIC;
            case World:
                return ETF_COMBINED_EQUITY_WORLD;
            case BondTaxable:
                return ETF_COMBINED_BOND_TAXABLE;
            case BondMunicipal:
                return ETF_COMBINED_BOND_MUNICIPAL;
            default:
                return type.name();
        }
    }
}
