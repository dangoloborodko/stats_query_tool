package org.ici.stats.dao.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.ici.stats.dao.QuickQueryDao;
import org.ici.stats.db.domain.Groupiob12;
import org.ici.stats.db.domain.Groupiob12Microiob12Key;
import org.ici.stats.db.domain.Quickquery12Special;
import org.ici.stats.db.domain.Quickquery12SpecialExample;
import org.ici.stats.db.mappers.Groupiob12Mapper;
import org.ici.stats.db.mappers.Groupiob12Microiob12Mapper;
import org.ici.stats.db.mappers.Quickquery12SpecialMapper;
import org.joda.time.DateTime;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

public class QuickQueryDaoImpl implements QuickQueryDao {

    private final Groupiob12Mapper groupiob12Mapper;
    private final Quickquery12SpecialMapper quickquery12SpecialMapper;
    private final Groupiob12Microiob12Mapper groupiob12Microiob12Mapper;

    @Inject
    public QuickQueryDaoImpl(Groupiob12Mapper groupiob12Mapper, Quickquery12SpecialMapper quickquery12SpecialMapper, Groupiob12Microiob12Mapper groupiob12Microiob12Mapper) {
        this.groupiob12Mapper = groupiob12Mapper;
        this.quickquery12SpecialMapper = quickquery12SpecialMapper;
        this.groupiob12Microiob12Mapper = groupiob12Microiob12Mapper;
    }


    @Override
    public List<Groupiob12> getGroupIob12() {
        return groupiob12Mapper.selectByExample( null );
        }

    @Override
    public List<Quickquery12Special> getSpecial(DateTime start, DateTime end, Set<Integer> microIobs) {
        Preconditions.checkArgument( microIobs != null && !microIobs.isEmpty(), "micro iobs are required" );
        Quickquery12SpecialExample example = new Quickquery12SpecialExample();
        example.or().andDateGreaterThanOrEqualTo( start ).andDateLessThanOrEqualTo( end ).andMicroiob12In(Lists.newArrayList( microIobs ));
        example.setDistinct( true );
        return quickquery12SpecialMapper.selectByExample( example );
    }

    @Override
    public List<Groupiob12Microiob12Key> getGroupMicros() {
        return groupiob12Microiob12Mapper.selectByExample(null);
    }
}
