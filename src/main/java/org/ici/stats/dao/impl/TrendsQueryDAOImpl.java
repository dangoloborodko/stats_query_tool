package org.ici.stats.dao.impl;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Ints;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.ici.stats.dao.DAOUtils;
import org.ici.stats.dao.TrendsQueryDAO;
import org.ici.stats.dao.builders.*;
import org.ici.stats.domain.*;
import org.ici.stats.services.ReportDefinitionService;
import org.ici.stats.utils.CategoryUtils;
import org.ici.stats.utils.ReportRowUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static org.ici.stats.dao.DAOUtils.timestamp;

public class TrendsQueryDAOImpl implements TrendsQueryDAO {

    private final DataSource dataSource;
    private final ReportDefinitionService reportService;

    private static final Logger logger = LoggerFactory.getLogger( TrendsQueryDAOImpl.class );

    @Inject
    public TrendsQueryDAOImpl(DataSource dataSource, ReportDefinitionService reportService) {
        this.dataSource = dataSource;
        this.reportService = reportService;
    }

    @Override
    public ReportResult getData( ReportRequest request ) {
        Report report = reportService.getReportDefinition( request.getReportType() );
        List<ReportRow> results = Lists.newArrayList();
        String fundCountLabel = "# of Funds";
        if ( request.getMethodsOfSale() == null || request.getMethodsOfSale().isEmpty() ) {
            for (Category cat : request.getCategories() ) {
                results.addAll( getFundLevelTrendsData( report, request.getStart(), request.getEnd(), request.getPeriod(), request.getDataPoints(), cat ));
            }
        } else {
            fundCountLabel = "# of Classes";
            for ( MethodOfSale mos : request.getMethodsOfSale() ) {
                for (Category cat : request.getCategories() ) {
                    results.addAll( getClassLevelTrendsData( report, request.getStart(), request.getEnd(), request.getPeriod(), request.getDataPoints(), cat, mos ));
                }
            }
        }

        ReportRowUtils.sortByCategory( results, report );
        return new ReportResult( request, results, fundCountLabel);
    }

    private static final Joiner commaSepJoiner = Joiner.on( "," );

    private List<ReportRow> getClassLevelTrendsData(Report report, DateTime startDate, DateTime endDate, Period period, List<DataPoint> points, Category cat, MethodOfSale mos) {
        SqlBuilder selector = getSelector(period);

        List<Integer> iobs = CategoryUtils.getIobsForCategory( cat );
        String iobForWhere = commaSepJoiner.join( iobs );

        List<Integer> mosIds = MosUtils.getMosIds( Lists.newArrayList( mos ));
        String mosForWhere = commaSepJoiner.join( mosIds );

        String queryBase =
                "SELECT " + selector.getDateSelect() + ", "
                        + selector.getLastTimeSelect("entities", "classcount") + ", "
                        + selector.getLastTimeSelect("tna") + ", "
                        + selector.getSumSelectorString(SqlBuilder.SelectorType.Class, false)
                        + " FROM quickquery12_web "
                        + selector.getWhere(iobForWhere) + " "
                        + selector.getMOSWhere(mosForWhere)
                        + selector.getGroupBy() + " "
                        + selector.getOrderBy();

        List<Object> params = buildParams( period, startDate, endDate );
        QueryRunner runner = new QueryRunner( dataSource );

        List<Map<String,Object>> queryResults;
        try {
            queryResults = runner.query( queryBase, new MapListHandler(), params.toArray() );
        } catch (SQLException e) {
            logger.error( "error getting class level trends data", e );
            return Lists.newArrayList();
        }

        return postProcessList( cat.getName() + " - " + mos.getName(), queryResults, report, period, points );
    }

    private String generateFundFields( Report report ) {
        List<DataPoint> points = report.getAllDataPoints();
        List<String> fields = Lists.newArrayList();
        for (DataPoint point : points) {
            if ( point.isTransactionAggregate() && !point.isMosCompatible() ) {
                fields.add( "funds." + report.getFieldName( point ));
            }
        }
        return commaSepJoiner.join( fields );
    }

    private static AbstractSqlBuilder getFundsBuilder(Period period ) {
        AbstractSqlBuilder selector = (AbstractSqlBuilder) getSelector(period);
        selector.setDateField( "funds.date" );
        selector.setCategoryField( "funds.iob12" );
        return selector;
    }

    private static AbstractSqlBuilder getClassesBuilder(Period period ) {
        AbstractSqlBuilder selector = (AbstractSqlBuilder) getSelector(period);
        selector.setDateField( "classes.date" );
        selector.setCategoryField( "classes.iob12" );
        return selector;
    }

    private void createTempTable( Connection c, Period period, DateTime startDate, DateTime endDate, Category cat ) {
        AbstractSqlBuilder selector = getFundsBuilder( period );
        String tempFundsTable = "SELECT " + selector.getDateSelect() + ", " +
                selector.getLastTimeSelect("entities", "funds.fundcount") + ", " +
                selector.getLastTimeSelect("ltot", "funds.ltot") + ", " +
                selector.getSumSelectorString(SqlBuilder.SelectorType.Fund, Boolean.FALSE) +
                "into #funds " +
                "from quickquery12_web funds " +
                selector.getWhere( iobStr( cat ))+
                "and funds.mos12 = 0 " +
                selector.getGroupBy();
        QueryRunner runner = new QueryRunner();
        // create our temporary table
        try {
            List<Object> params = Lists.newArrayList();
            if ( Period.CurrentMonth.equals( period )) {
                params.add( timestamp( endDate ));
            } else {
                params.add( timestamp( startDate ));
                params.add( timestamp( endDate ));
            }
            runner.update( c, tempFundsTable, params.toArray() );
        } catch (SQLException e) {
            logger.error( "error creating temp table", e );
            logger.error( "sql: {}", tempFundsTable );
        }
    }

    private static String iobStr( Category cat ) {
        List<Integer> iobs = CategoryUtils.getIobsForCategory( cat );
        return commaSepJoiner.join( iobs );
    }

    private List<ReportRow> getFundLevelTrendsData(Report report, DateTime startDate, DateTime endDate, Period period, List<DataPoint> points, Category cat)  {
        String fundFields = generateFundFields( report ) + ", funds.entities, funds.ltot";
        Connection c = null;
        List<Map<String, Object>> results = Lists.newArrayList();
        try {
            c = dataSource.getConnection();
            createTempTable( c, period, startDate, endDate, cat );

            AbstractSqlBuilder selector = getClassesBuilder( period );
            List<Object> params = buildParams( period, startDate, endDate );
            String queryBase =
                    "SELECT " + selector.getDateSelect() + ", "
                            + selector.getLastTimeSelect("tna", "classes.tna") + ", "
                            + selector.getSumSelectorString( SqlBuilder.SelectorType.Fund, Boolean.TRUE) + ", "
                            + fundFields
                            + " FROM quickquery12_web classes, #funds funds "
                            + selector.getWhere(iobStr( cat ))
                            + " AND " + selector.getTimePeriod() + " = funds.time_period AND " + selector.getYear() + " = funds.year "
                            + " AND classes.mos12 != 0 "
                            + selector.getGroupBy() + ", " + fundFields
                            + selector.getOrderBy();
            QueryRunner runner = new QueryRunner();
            results = runner.query( c, queryBase, new MapListHandler(), params.toArray() );

            // drop our temp table
            runner.update( c, "drop table #funds");
        } catch (SQLException e) {
            logger.error( "error getting funds level trends data", e );
        } finally {
            DbUtils.closeQuietly( c );
        }
        return postProcessList( cat.getName(), results, report, period, points );
    }

    private static List<Object> buildParams(Period period, DateTime startDate, DateTime endDate) {
        List<Object> params = Lists.newArrayList();
        if ( Period.CurrentMonth.equals( period )) {
            params.add( timestamp( endDate ));
        } else {
            params.add( timestamp( startDate ));
            params.add( timestamp( endDate ));
        }
        return params;
    }

    /**
     * convert our results from the query into TrendsDataVO
     * @param reportData data pulled from report
     * @param report report definition
     * @param period requested period
     * @param points data points
     * @return list of trends data
     */
    private List<ReportRow> postProcessList(String category, List<Map<String, Object>> reportData, Report report, Period period, List<DataPoint> points) {
        List<ReportRow> results = Lists.newArrayList();
        for (Map<String, Object> reportDatum : reportData) {
            int count = getIntValue( reportDatum, "entities" );
            int year = getIntValue( reportDatum, "year" );
            int month = 0;
            if ( Period.Annual.equals( period )) {
                String value = (String) reportDatum.get( "time_period" );
                month = Ints.tryParse( value );
            } else {
                month = getIntValue( reportDatum, "time_period");
            }
            Map<DataPoint,Long> dataMap = Maps.newHashMap();
            for (DataPoint dataPoint : points ) {
                String fieldName = report.getFieldName( dataPoint );
                dataMap.put( dataPoint, DAOUtils.getLongValue( reportDatum, fieldName ));
            }
            results.add(new ReportRow( category, count, month, year, dataMap ));

        }
        return results;
    }

    private static int getIntValue( Map<String,Object> map, String key ) {
        Preconditions.checkState( map.get( key ) != null, "map missing value for key %s", key );
        Preconditions.checkState( map.get( key ) instanceof Number, "map value not number for key: %s -- value: %s, type: %s", key, map.get( key ), map.get( key ).getClass() );
        return ((Number) map.get(key)).intValue();
    }

    private static SqlBuilder getSelector(Period period) {
        switch( period ) {
            case Quarterly:
                return new QuarterlySqlBuilder();
            case Annual:
                return new AnnualSqlBuilder( "iob12");
            case Monthly:
                return new MRangeSqlBuilder();
            case CurrentMonth:
                return new CurrentMonthSqlBuilder();

        }
        return null;
    }
}
