package org.ici.stats.dao.impl;

import com.google.common.base.Preconditions;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.ici.stats.utils.DateUtils;
import org.ici.stats.dao.EtfReportDAO;
import org.ici.stats.dao.rsh.SpecialReportRSH;
import org.ici.stats.domain.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class EtfReportDAOImpl implements EtfReportDAO {
    private final Logger logger = LoggerFactory.getLogger(EtfReportDAOImpl.class);

    private final DataSource dataSource;

    @Inject
    public EtfReportDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public ReportResult getData(ReportRequest request) {
        String username = getUsername();
        logger.info( "running report {} with username {}", request.getReportType(), username );
        // username should be unique, but just in case
        clearParams(username);
        storeParams( username, request );
        return executeQuery( username, request );
    }

    private static String getUsername() {
        return UUID.randomUUID().toString();
    }

    private ReportResult executeQuery(String username, ReportRequest request) {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            CallableStatement cs = conn.prepareCall("exec quickquery_iob98_etf ?, ?");
            cs.setString( "@login_name", username );
            cs.setString( "@super_user", "n" );
            ResultSetHandler<List<ReportRow>> handler = new SpecialReportRSH( request.getDataPoints(), request.getPeriod() );
            List<ReportRow> results = handler.handle(cs.executeQuery());
            return new ReportResult( request, results, null);
        } catch (SQLException e) {
            logger.error("error executing query with report id: {}, username: {}", request.getReportType().getDbId(), username );
            logger.error( "{}", e );
            throw new IllegalStateException( "error running report", e );
        } finally {
            DbUtils.closeQuietly( conn );
        }
    }

    private void storeParam(String username, String paramName, String paramValue) {
        logger.debug( "etf - storing param: {}, value: {} for username: {}", paramName, paramValue, username );
        QueryRunner qr = new QueryRunner( dataSource );
        try {
            qr.update( "INSERT INTO research..quickquery_etf_parameter ( login_name, parameter_type, parameter_value )" +
                " VALUES ( ?, ?, ? )", username, paramName, paramValue );
        } catch (SQLException e) {
            logger.error("error storing param: {}, username: {}, value: {}", new Object[] { paramName, username, paramValue }, e);
        }
    }

    private void storeParams(String username, ReportRequest request) {
        for (Category category : request.getCategories()) {
            Preconditions.checkNotNull( category.getIobLevel(), "missing iob level for category: %s", category );
            String iobName = iobName( category.getIobLevel() );
            storeParam( username, iobName, category.getId() );
        }

        // determine the level of category we are working with
        Category firstCategory = request.getCategories().iterator().next();

        String groupCode = groupCode( firstCategory.getLevels().iterator().next() );
        storeParam( username, "group1", groupCode );

        storeParam( username, "fof", request.isIncludeEtfFundOfFunds() ? "y" : "n" );
        // stored but actually not used -- included for completeness
        storeParam( username, "1940", "a");

        storeParam( username, "type", etfType( request.getEtfType() ));

        storeParam( username, "date_group", getDateGroup( request.getPeriod() ));
        if ( Period.CurrentMonth.equals(request.getPeriod())) {
            // we'll use the end month / year as the start
            storeParam( username, "begin_month", request.getEnd().getMonthOfYear() );
            storeParam( username, "begin_year", request.getEnd().getYear() );
        } else {
            storeParam( username, "begin_month", request.getStart().getMonthOfYear());
            storeParam( username, "begin_year", request.getStart().getYear() );
        }

        if ( Period.Quarterly.equals( request.getPeriod() )) {
            // adjust our report end month so that it falls on the last valid quarter
            DateTime endOfMonth = DateUtils.endOfMonth( request.getEnd().getMonthOfYear(), request.getEnd().getYear() );
            DateTime validQuarterEnd = DateUtils.getMostRecentFullQuarter( endOfMonth );
            storeParam( username, "end_month", validQuarterEnd.getMonthOfYear() );
            storeParam( username, "end_year", validQuarterEnd.getYear() );
            logger.info( "using {} as quarter end", DateUtils.mmYYYY.print( validQuarterEnd ));
        } else {
            storeParam( username, "end_month", request.getEnd().getMonthOfYear() );
            storeParam( username, "end_year", request.getEnd().getYear() );
        }
        storeParam( username, "mm_total", "N" );
        storeParam( username, "ytd_grand_total", "N" );
        storeParam( username, "ytd_group_total", "N" );
        storeParam( username, "ytd_quarterly_group_total", "N" );
    }

    private String groupCode(Level level) {
        switch ( level ) {
            case Mega:
                return "GT"; // Group Totals
            case Super:
                return "SG"; // not sure
            case High:
                return "BO"; // iob level?
        }
        throw new IllegalArgumentException( "unable to parse level: " + level );
    }

    private static String etfType( EtfType type ) {
        switch ( type ) {
            case Total:
                return "t";
            case Active:
                return "n";
            case Index:
                return "y";
        }
        throw new IllegalArgumentException( "unknown type: " + type );
    }

    private static String getDateGroup( Period period ) {
        switch( period ) {
            case Annual:
                return "YY";
            case Monthly:
            case CurrentMonth:
                return "MM";
            case Quarterly:
                return "QQ";
        }
        throw new IllegalArgumentException( "unable to handle period: " + period );
    }

    private static String iobName( IobLevel iobLevel ) {
        switch ( iobLevel ) {
            case CompIob98:
                return "compiob98";
            case Iob98:
                return "iob98";
            case SubCompIob98:
                return "subcompiob98";
            case SubIob98:
                return "subiob98";
        }
        throw new IllegalArgumentException( "unable to handle iob: " + iobLevel );
    }


    private void storeParam(String username, String paramName, int param) {
        storeParam( username, paramName, Integer.toString( param ));
    }

    private void clearParams(String username) {
        try {
            QueryRunner qr = new QueryRunner( dataSource );
            qr.update( "delete from dbo.quickquery_parameter_special where login_name = ?", username );
        } catch (SQLException e) {
            logger.error("error clearing params for user: {}", username, e);
        }
    }
}
