package org.ici.stats.dao.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.ici.stats.dao.SpecialReportDAO;
import org.ici.stats.dao.rsh.SpecialReportRSH;
import org.ici.stats.domain.*;
import org.ici.stats.utils.DateUtils;
import org.ici.stats.utils.CategoryUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class SpecialReportDAOImpl implements SpecialReportDAO {
    private final Logger log = LoggerFactory.getLogger(SpecialReportDAOImpl.class);

    private final DataSource dataSource;

    @Inject
    public SpecialReportDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public ReportResult getData(ReportRequest request) {
        String username = getUsername();
        log.info( "running report {} with username {}", request.getReportType(), username );
        // username should be unique, but just in case
        clearParams(username);
        int reportType = request.getReportType().getDbId();
        storeParams( username, reportType, request );
        List<ReportRow> result = executeQuery( reportType, username, request.getPeriod(), request.getDataPoints() );
        return new ReportResult( request, result, null );
    }

    private static String getUsername() {
        return UUID.randomUUID().toString();
    }

    private List<ReportRow> executeQuery(int reportId, String username, Period period, List<DataPoint> dataPoints) {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            CallableStatement cs = conn.prepareCall("exec quickquery_iob12_special_rpt ?, ?");
            cs.setInt( "@report_id", reportId );
            cs.setString( "@login_name", username );
            ResultSetHandler<List<ReportRow>> handler = new SpecialReportRSH( dataPoints, period );
            return handler.handle(cs.executeQuery());
        } catch (SQLException e) {
            log.error("error executing query with report id: {}, username: {}", reportId, username );
            log.error( "{}", e );
        } finally {
            DbUtils.closeQuietly( conn );
        }
        return Lists.newArrayList();
    }

    private void storeParam(String username, String paramName, String paramValue) {
        QueryRunner qr = new QueryRunner( dataSource );
        try {
            qr.update( "INSERT INTO dbo.quickquery_parameter_special ( login_name, parameter_type, parameter_value )" +
                " VALUES ( ?, ?, ? )", username, paramName, paramValue );
        } catch (SQLException e) {
            log.error("error storing param: {}, username: {}, value: {}", new Object[] { paramName, username, paramValue }, e);
        }
    }

    private void storeParams(String username, int reportId, ReportRequest reportParam) {
        storeParam( username, "group1", "I" );
        storeParam( username, "reportId", reportId );

        Set<Integer> iobs = Sets.newHashSet();
        for (Category category : reportParam.getCategories()) {
            iobs.addAll( CategoryUtils.getIobsForCategory( category ));
        }

        for ( int field : iobs ) {
            storeParam(username, "iob12", field);
        }
        storeParam( username, "date_group", getDateGroup( reportParam.getPeriod() ));
        if ( Period.CurrentMonth.equals(reportParam.getPeriod())) {
            // we'll use the end month / year as the start
            storeParam( username, "begin_month", reportParam.getEnd().getMonthOfYear() );
            storeParam( username, "begin_year", reportParam.getEnd().getYear() );
        } else {
            storeParam( username, "begin_month", reportParam.getStart().getMonthOfYear());
            storeParam( username, "begin_year", reportParam.getStart().getYear() );
        }

        if ( Period.Quarterly.equals( reportParam.getPeriod() )) {
            // adjust our report end month so that it falls on the last valid quarter
            DateTime endOfMonth = DateUtils.endOfMonth( reportParam.getEnd().getMonthOfYear(), reportParam.getEnd().getYear() );
            DateTime validQuarterEnd = DateUtils.getMostRecentFullQuarter( endOfMonth );
            storeParam( username, "end_month", validQuarterEnd.getMonthOfYear() );
            storeParam( username, "end_year", validQuarterEnd.getYear() );
            log.info( "using {} as quarter end", DateUtils.mmYYYY.print( validQuarterEnd ));
        } else {
            storeParam( username, "end_month", reportParam.getEnd().getMonthOfYear() );
            storeParam( username, "end_year", reportParam.getEnd().getYear() );
        }
        storeParam( username, "mm_total", "N" );
        storeParam( username, "ytd_grand_total", "N" );
        storeParam( username, "ytd_group_total", "N" );
        storeParam( username, "ytd_quarterly_group_total", "N" );
    }

    private static String getDateGroup( Period period ) {
        switch( period ) {
            case Annual:
                return "YY";
            case Monthly:
            case CurrentMonth:
                return "MM";
            case Quarterly:
                return "QQ";
        }
        throw new IllegalArgumentException( "unable to handle period: " + period );
    }

    private void storeParam(String username, String paramName, int param) {
        storeParam( username, paramName, Integer.toString( param ));
    }

    private void clearParams(String username) {
        try {
            QueryRunner qr = new QueryRunner( dataSource );
            qr.update( "delete from dbo.quickquery_parameter_special where login_name = ?", username );
        } catch (SQLException e) {
            log.error("error clearing params for user: {}", username, e);
        }
    }
}
