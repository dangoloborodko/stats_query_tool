package org.ici.stats.dao.impl;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.QueryRunner;
import org.ici.stats.dao.EtfSpecialReportDAO;
import org.ici.stats.dao.rsh.ETFSpecialReportRSH;
import org.ici.stats.domain.*;
import org.ici.stats.services.reports.etf.ETFActiveIndexProvider;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

import static org.ici.stats.dao.DAOUtils.timestamp;

public class EtfActiveIndexReportDAOImpl implements EtfSpecialReportDAO {

    private final DataSource dataSource;

    private static final Logger logger = LoggerFactory.getLogger(EtfActiveIndexReportDAOImpl.class);

    private final Report activeIndexReport = new ETFActiveIndexProvider().get();

    @Inject
    public EtfActiveIndexReportDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public ReportResult getData(ReportRequest request) {
        List<ReportRow> activeEquity = getEtfActiveIndexIndexData( "Active: Equity", request.getStart(), request.getEnd(), request.getPeriod(), ActiveIndexType.Equity, true );
        List<ReportRow> activeHybrid = getEtfActiveIndexIndexData( "Active: Hybrid", request.getStart(), request.getEnd(), request.getPeriod(), ActiveIndexType.Hybrid, true );
        List<ReportRow> indexDomestic = getEtfActiveIndexIndexData( "Index: Domestic" , request.getStart(), request.getEnd(), request.getPeriod(), ActiveIndexType.Domestic, false );
        List<ReportRow> indexHybrid = getEtfActiveIndexIndexData( "Index: Hybrid", request.getStart(), request.getEnd(), request.getPeriod(), ActiveIndexType.Hybrid, false );
        List<ReportRow> indexWorld = getEtfActiveIndexIndexData( "Index: World", request.getStart(), request.getEnd(), request.getPeriod(), ActiveIndexType.World, false );
        List<ReportRow> commodities = getEtfActiveIndexCommodityData( "Commodities", request.getStart(), request.getEnd(), request.getPeriod() );

        List<ReportRow> results = Lists.newArrayList();
        results.addAll( activeEquity );
        results.addAll( activeHybrid );
        results.addAll( indexDomestic );
        results.addAll( indexHybrid );
        results.addAll( indexWorld );
        results.addAll( commodities );
        return new ReportResult( request, results, null );
    }

    enum ActiveIndexType {Domestic, World, Hybrid, Equity }

    public List<ReportRow> getEtfActiveIndexIndexData(String category, DateTime startDate, DateTime endDate, Period period, ActiveIndexType type, boolean active ) {
        String iobList = "";
        if ( active ) {
            if (ActiveIndexType.Equity.equals(type)) {
                iobList = "800,805,810,815,820,825,830";
            } else { // hybrid
                iobList = "835,840,845,850,851,855";
            }
        } else {
            if (ActiveIndexType.Domestic.equals(type)) {
                iobList = "800,805,810,815,820,825";
            } else if (ActiveIndexType.World.equals(type)) {
                iobList = "830";
            } else {// Hybrid
                iobList = "835,840,845,850,851,855";
            }
        }

        String sql = "select sum(q.tna*1.0) as tna, sum(q.net_issuance*1.0) as nncf, sum(q.fundcount) as funds, q.date  " +
                "from quickquery98_etf q " +
                "where q.iob98 in (" + iobList + ") " +
                "    and q.subiob98 != 829 " +
                "    and q.fof = 'n' " +
                "    and q.index_fund = ? " +
                "    and q.date >= ? and q.date <= ? " +
                "group by q.date " +
                "order by q.date";

        QueryRunner runner = new QueryRunner(dataSource);
        try {
            String indexFund = active ? "n" : "y";
            return  runner.query(sql, new ETFSpecialReportRSH( category, period, DataPoint.TotalNetAssets, DataPoint.NetIssuance), indexFund, timestamp(startDate), timestamp(endDate));
        } catch (SQLException e) {
            logger.error("error getting active index data for sql: {}", sql, e);
            return Lists.newArrayList();
        }
    }

    public List<ReportRow> getEtfActiveIndexCommodityData(String category, DateTime startDate, DateTime endDate, Period period ) {
        String sql = "select sum(q.tna*1.0) as tna, sum(q.net_issuance*1.0) as nncf, sum(q.fundcount) as funds, q.date  " +
                "from quickquery98_etf q " +
                "where q.subiob98 = 829 " +
                "    and q.fof = 'n' " +
                "    and q.date >= ? and q.date <= ? " +
                "group by q.date " +
                "order by q.date";

        QueryRunner runner = new QueryRunner(dataSource);
        try {
            return  runner.query(sql, new ETFSpecialReportRSH( category, period, DataPoint.TotalNetAssets, DataPoint.NetIssuance), timestamp(startDate), timestamp(endDate));
        } catch (SQLException e) {
            logger.error("error getting active index data for sql: {}", sql, e);
            return Lists.newArrayList();
        }
    }
}
