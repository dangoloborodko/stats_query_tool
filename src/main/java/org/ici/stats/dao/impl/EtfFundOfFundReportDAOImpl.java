package org.ici.stats.dao.impl;

import com.google.common.collect.Lists;
import org.apache.commons.dbutils.QueryRunner;
import org.ici.stats.dao.EtfSpecialReportDAO;
import org.ici.stats.dao.rsh.ETFSpecialReportRSH;
import org.ici.stats.domain.*;
import org.ici.stats.services.reports.etf.ETFActiveIndexProvider;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

import static org.ici.stats.dao.DAOUtils.timestamp;

public class EtfFundOfFundReportDAOImpl implements EtfSpecialReportDAO {

    private final DataSource dataSource;

    private static final Logger logger = LoggerFactory.getLogger(EtfFundOfFundReportDAOImpl.class);

    private final Report activeIndexReport = new ETFActiveIndexProvider().get();

    @Inject
    public EtfFundOfFundReportDAOImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    enum FundType {Domestic, World, Hybrid, BondTaxable, Municipal, Commodities }

    @Override
    public ReportResult getData(ReportRequest request) {
        List<ReportRow> results = getEtfFundOfFundsData( "Equity", request.getStart(), request.getEnd(), request.getPeriod(), true );
        results.addAll( getEtfFundOfFundsData( "Hybrid", request.getStart(), request.getEnd(), request.getPeriod(), false ));

        return new ReportResult( request, results, null );
    }

    private List<ReportRow> getEtfFundOfFundsData(String label, DateTime start, DateTime end, Period period, boolean equity ) {
        String iobList = equity ? "800,805,810,815,820,825,830" : "835,840,845,850,851,855";
        String sql = "select convert(char(10), q.date, 101) as moddate, sum(q.tna*1.0) as tna, sum(q.net_issuance*1.0) as nncf, sum(q.gross_issuance*1.0) as issuance, sum(q.gross_redemption*1.0) as redemption, sum(q.fundcount) as funds, q.date " +
                "from quickquery98_etf q " +
                "where q.iob98 in (" + iobList + ") " +
                "    and q.fof = 'y' " +
                "    and q.date >= ? and q.date <= ? " +
                "group by q.date " +
                "order by q.date";

        QueryRunner runner = new QueryRunner(dataSource);
        try {
            return  runner.query(sql, new ETFSpecialReportRSH( label, period, DataPoint.TotalNetAssets, DataPoint.NetIssuance, DataPoint.GrossIssuance, DataPoint.GrossRedemptions), timestamp(start), timestamp(end));
        } catch (SQLException e) {
            logger.error("error getting fund of funds data for sql: {}", sql, e);
            return Lists.newArrayList();
        }
    }
}
