package org.ici.stats.dao;

import org.ici.stats.domain.ReportRequest;
import org.ici.stats.domain.ReportResult;

import java.util.List;

public interface SpecialReportDAO {
    ReportResult getData(ReportRequest request );
}
