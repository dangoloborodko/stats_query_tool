package org.ici.stats.dao;

import org.ici.stats.domain.ReportRequest;
import org.ici.stats.domain.ReportResult;
import org.ici.stats.domain.ReportRow;

import java.util.List;

public interface EtfReportDAO {
    ReportResult getData(ReportRequest request);
}
