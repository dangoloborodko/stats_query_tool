package org.ici.stats.dao;

import org.ici.stats.domain.ReportRequest;
import org.ici.stats.domain.ReportResult;

public interface EtfSpecialReportDAO {
    ReportResult getData(ReportRequest request );
}
