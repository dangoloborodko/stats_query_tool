package org.ici.stats.dao;

import com.google.common.base.Preconditions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;

public class DAOUtils {
    private static final Logger logger = LoggerFactory.getLogger(DAOUtils.class);

    public static long valueFromResultSet(ResultSet resultSet, String field, int scale ) throws SQLException {
        return resultSet.getBigDecimal( field ).setScale( scale, BigDecimal.ROUND_HALF_UP ).longValue();
    }

    public static long getLongValue(Map<String, Object> map, String key) {
        Preconditions.checkNotNull( map.get( key ), "unable to find key: %s, available values: %s", key, map.keySet() );
        return ((Number) map.get(key)).longValue();
    }

    public static int getIntValue(Map<String, Object> map, String key) {
        if (map.get(key) == null) {
            logger.error("unable to find key: {}", key);
        }
        return ((Number) map.get(key)).intValue();
    }

    public static Timestamp timestamp(DateTime convertMe ) {
        return ( convertMe == null ) ? null : new Timestamp( convertMe.getMillis() );
    }

}
