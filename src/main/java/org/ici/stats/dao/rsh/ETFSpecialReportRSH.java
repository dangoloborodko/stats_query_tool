package org.ici.stats.dao.rsh;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.dbutils.ResultSetHandler;
import org.ici.stats.utils.DateUtils;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.Period;
import org.ici.stats.domain.ReportRow;
import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.ici.stats.dao.DAOUtils.valueFromResultSet;

public class ETFSpecialReportRSH implements ResultSetHandler<List<ReportRow>> {

    private final List<DataPoint> dataPoints;
    private final Period period;
    private final String category;

    public ETFSpecialReportRSH(String category, Period period, DataPoint ... dataPoints ) {
        this.category = category;
        this.dataPoints = Lists.newArrayList(dataPoints);
        this.period = period;
    }

    @Override
    public List<ReportRow> handle(ResultSet resultSet) throws SQLException {
        List<ReportRow> trendsData = Lists.newArrayList();
        while ( resultSet.next() ) {
            Date date = resultSet.getDate( "date" );
            DateTime niceDate = new DateTime( date );
            int year = niceDate.getYear();
            int month = niceDate.getMonthOfYear();
            if ( period.equals( Period.Quarterly )) {
                month = DateUtils.getQuarterFromMonth( month );
            }
            int fundCount = resultSet.getInt( "funds" );
            // for each data point try and find
            Map<DataPoint, Long> dataMap = Maps.newHashMap();
            for (DataPoint dataPoint : dataPoints ) {
                switch ( dataPoint ) {
                    case TotalNetAssets:
                        dataMap.put( DataPoint.TotalNetAssets, valueFromResultSet( resultSet, "tna", 0 ));
                        break;
                    case Flows:
                        dataMap.put( DataPoint.Flows, valueFromResultSet( resultSet, "nncf", 0 ));
                        break;
                    case NetIssuance:
                        dataMap.put( DataPoint.NetIssuance, valueFromResultSet( resultSet, "nncf", 0 ));
                        break;
                    case GrossIssuance:
                        dataMap.put( DataPoint.GrossIssuance, valueFromResultSet( resultSet, "issuance", 0 ));
                        break;
                    case GrossRedemptions:
                        dataMap.put( DataPoint.GrossRedemptions, valueFromResultSet( resultSet, "redemption", 0 ));
                        break;
                }
            }
            trendsData.add( new ReportRow( category, fundCount, month, year, dataMap ));
        }
        return trendsData;
    }
}
