package org.ici.stats.dao.rsh;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.dbutils.ResultSetHandler;
import org.ici.stats.utils.DateUtils;
import org.ici.stats.domain.Period;
import org.ici.stats.domain.DataPoint;
import org.ici.stats.domain.ReportRow;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class SpecialReportRSH implements ResultSetHandler<List<ReportRow>> {
    private final List<DataPoint> dataPoints;
    private final Period datePeriodType;

    public SpecialReportRSH(List<DataPoint> dataPoints, Period datePeriodType) {
        this.dataPoints = dataPoints;
        this.datePeriodType = datePeriodType;
    }

    @Override
    public List<ReportRow> handle(ResultSet resultSet) throws SQLException {
        List<ReportRow> trendsData = Lists.newArrayList();
        while ( resultSet.next() ) {
            int topGroup = resultSet.getInt( "top_group" );
            if ( topGroup != 1 ) {
                continue;
            }
            String category = resultSet.getString("iob_name");
            int year = resultSet.getInt("year");
            Date dateGroup = resultSet.getDate( "date_group" );
            DateTime niceDate = new DateTime( dateGroup );
            int datePeriod = niceDate.getMonthOfYear();
            if ( datePeriodType.equals( Period.Quarterly )) {
                datePeriod = DateUtils.getQuarterFromMonth( datePeriod );
            }
            int fundCount = resultSet.getInt( "fundcount_total" );
            // for each data point try and find
            Map<DataPoint, Long> valueMap = Maps.newHashMap();
            for (DataPoint dataPoint : dataPoints ) {
                switch ( dataPoint ) {
                    case TotalNetAssets:
                        valueMap.put( DataPoint.TotalNetAssets, valueFromResultSet( resultSet, "tna_total", 0 ));
                        break;
                    case NetNewCashFlow:
                        valueMap.put( DataPoint.NetNewCashFlow, createNetNewCashFlow( resultSet ));
                        break;
                    case NetIssuance:
                        valueMap.put( DataPoint.NetIssuance, valueFromResultSet( resultSet, "sreg_total", 1 ));
                        break;
                    case GrossIssuance:
                        valueMap.put( DataPoint.GrossIssuance, valueFromResultSet( resultSet, "srein_total",  1 ));
                        break;
                    case GrossRedemptions:
                        valueMap.put( DataPoint.GrossRedemptions, valueFromResultSet( resultSet, "rreg_total", 1 ));
                        break;
                }
            }
            ReportRow data = new ReportRow( category, fundCount, datePeriod, year, valueMap );
            trendsData.add( data );
        }
        return trendsData;
    }

    private static long valueFromResultSet(ResultSet resultSet, String field, int scale ) throws SQLException {
        return resultSet.getBigDecimal( field ).setScale( scale, BigDecimal.ROUND_HALF_UP ).longValue();
    }

    private long createNetNewCashFlow(ResultSet resultSet) throws SQLException {
        BigDecimal sreg = resultSet.getBigDecimal( "sreg_total" );
        BigDecimal rreg = resultSet.getBigDecimal("rreg_total");
        BigDecimal sex = resultSet.getBigDecimal("sex_total");
        BigDecimal rex = resultSet.getBigDecimal("rex_total");
        BigDecimal nncf = sreg.subtract( rreg ).add( sex ).subtract( rex );
        return nncf.setScale( 0, BigDecimal.ROUND_HALF_UP ).longValue();
    }
}
