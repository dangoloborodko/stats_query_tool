package org.ici.stats.utils;

public class Constants {
    public static final String MACRO_CATEGORY = "macro";
    public static final String COMPOSITE_CATEGORY = "composite";
    public static final String OBJECTIVES_CATEGORY = "objectives";
    public static final String HIERARCHY_CATEGORY = "hierarchy";

    public static final String CATEGORY_LEVEL = "categoryLevel";
    public static final String CATEGORIES = "categories";
    public static final String SELECTED_CATEGORIES = "selectedCategories";

    public static final String SELECTED_MOS = "selectedMos";

    public static final String APP_TREND_CUR_DATE = "members.stats.trend_data.end_date";

    // Reporting type constants
    public static final int REPORT_MONTHLY = 1;
    public static final int REPORT_QUARTERLY = 2;
    public static final int REPORT_ANNUALLY = 3;
    public static final int REPORT_CURRENT_MONTH = 4;

    public static final String MONTHLY_TRENDS = "R1";
    public static final String QUARTERLY_TRENDS = "R2";
    public static final String RETIREMENT = "R3";
    public static final String ACTIVE_INDEX = "R4";
    public static final String TARGET_LIFECYCLE = "R5";
    public static final String FOF = "R6";
    public static final String INFLATION_PROTECTED = "R7";
    public static final String EMERGING_MARKETS = "R8";
    public static final String FLOATING_RATE = "R9";
    public static final String MARKET_CAPITALIZATION =  "R10";
    public static final String SECTOR = "R11" ;
    public static final String ALTERNATIVE_STRATEGIES = "R12";

    public final static String ASSETS_TEXT = "Assets, Flows, and Sales (Monthly)";

    public static final int COMBINED_EQUITY_TOTAL = 1;
    public static final int COMBINED_EQUITY_DOMESTIC = 2;
    public static final int COMBINED_EQUITY_WORLD = 3;
    public static final int COMBINED_HYBRID = 4;
    public static final int COMBINED_BOND_TOTAL = 5;
    public static final int COMBINED_BOND_TAXABLE = 6;
    public static final int COMBINED_BOND_MUNICIPAL = 7;
    public static final int COMBINED_COMMODITIES = 8;
}
