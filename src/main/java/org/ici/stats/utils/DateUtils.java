package org.ici.stats.utils;

import com.google.common.collect.Lists;
import org.ici.stats.domain.Period;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by jalexander on 10/20/2014.
 */
public class DateUtils {
    public static final DateTimeFormatter mmYYYY = DateTimeFormat.forPattern("MM/YYYY");
    public static final DateTimeFormatter mmm_yyyy = DateTimeFormat.forPattern("MMM. YYYY");
    public static final DateTimeFormatter mm_dd_yyyy = DateTimeFormat.forPattern("MM/dd/YYYY");
    public static final DateTimeFormatter monthName = DateTimeFormat.forPattern("MMMM");

    public static List<String> months() {
        List<String> monthNames = Lists.newArrayList();
        for (int month = 1; month <= 12 ; month++) {
            DateTime date = endOfMonth( month, 2017 );
            monthNames.add( monthName.print( date ));
        }
        return monthNames;
    }

    public static Timestamp convertDateToTimestamp( Date date ) {
        return date == null ? null : new Timestamp( date.getTime() );
    }

    public static int getQuarterFromMonth( int month ) {
        if ( month <= 3 ) {
            return 1;
        } else if ( month <=6 ) {
            return 2;
        } else if ( month <= 9 ) {
            return  3;
        } else {
            return 4;
        }
    }

    /**
     * create a date on the last day of a given month
     * @param month
     * @param year
     * @return
     */
    public static DateTime endOfMonth( int month, int year ) {
        DateTime date = new DateTime( year, month, 1, 0, 0 );
        return date.dayOfMonth().withMaximumValue();
    }

    public static DateTime beginningOfMonth( DateTime date  ) {
        return date.dayOfMonth().withMinimumValue();
    }

    public static Date getMostRecentFullQuarter( Date date ) {
        DateTime dt = new DateTime( date );
        return getMostRecentFullQuarter( dt ).toDate();
    }

    public static int getQuarterForMonth (int month ) {
        if ( month <= 3 ) {
            return 1;
        } else if ( month <=6 ) {
            return 2;
        } else if ( month <= 9 ) {
            return 3;
        } else {
            return 4;
        }
    }

    public static DateTime getMostRecentFullQuarter( DateTime date ) {
        int month = date.getMonthOfYear();
        int quarterPart = month % 3;
        if ( quarterPart == 0 ) {
            // have a perfect quarter
            return date;
        } else {
            int quarter = month / 3;
            if ( quarter == 0 ) {
                // looking at January or Feb
                return date.minusYears( 1 ).withMonthOfYear( 12 );
            } else {
                return date.withMonthOfYear( quarter * 3 );
            }
        }
    }

    public static DateTime endOfMonth(DateTime end) {
        return end.dayOfMonth().withMaximumValue();
    }
}
