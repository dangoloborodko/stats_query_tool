package org.ici.stats.utils;

import com.google.common.collect.*;
import org.ici.stats.domain.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class ReportRowUtils {

    public static ReportRow sum( List<ReportRow> rows, String category, int month, int year ) {
        if ( rows == null || rows.isEmpty() ) {
            return null;
        }

        // make sure all the rows are the same category, date period
        int fundCount = 0;
        Map<DataPoint,Long> sumMap = Maps.newHashMap();
        for (ReportRow row : rows) {
            for (Map.Entry<DataPoint, Long> entry : row.getValueMap().entrySet()) {
                DataPoint point = entry.getKey();
                Long value = entry.getValue();
                if ( !sumMap.containsKey( point )) {
                    sumMap.put( point, value );
                } else {
                    Long oldValue = sumMap.get( point );
                    sumMap.put( point, oldValue + value );
                }
            }
            fundCount += row.getFundCount();
        }

        return new ReportRow( category, fundCount, month, year, sumMap );
    }

    public static void sortByCatName( List<ReportRow> sortMe, final List<String> categories ) {
        Comparator<ReportRow> comparator = new Comparator<ReportRow>() {
            @Override
            public int compare(ReportRow o1, ReportRow o2) {
                int pos1 = categories.indexOf( o1.getCategory() );
                int pos2 = categories.indexOf( o2.getCategory() );
                return ComparisonChain.start().compare( pos1, pos2 ).compare( o1.getYear(), o2.getYear() ).compare( o1.getDatePeriod(), o2.getDatePeriod() ).result();
            }
        };
        Collections.sort( sortMe, comparator);
    }

    public static void sortByCategory( List<ReportRow> sortMe, Report report ) {
        List<Category> cats = report.getCategories();
        List<Category> flatCats = CategoryUtils.getFlatList( cats );
        List<String> catNames = CategoryUtils.getNames( flatCats );
        sortByCatName( sortMe, catNames );
    }

    private static class CategoryDate {
        private final String category;
        private final int datePart;
        private final int year;

        private CategoryDate(String category, int datePart, int year) {
            this.category = category;
            this.datePart = datePart;
            this.year = year;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CategoryDate that = (CategoryDate) o;

            return datePart == that.datePart && year == that.year && category.equals(that.category);
        }

        @Override
        public int hashCode() {
            int result = category.hashCode();
            result = 31 * result + datePart;
            result = 31 * result + year;
            return result;
        }
    }

    public static List<ReportRow> combineByCategoryAndDate( List<ReportRow> rows, Period period ) {
        ArrayListMultimap<CategoryDate,ReportRow> catMap = ArrayListMultimap.create();
        for (ReportRow row : rows) {
            int month = 0;
            if ( Period.Quarterly.equals( period )) {
                month = DateUtils.getQuarterFromMonth( row.getDatePeriod() );
            } else if ( Period.Monthly.equals( period ) || Period.CurrentMonth.equals( period )) {
                month = row.getDatePeriod();
            }
            CategoryDate catDate = new CategoryDate( row.getCategory(), month, row.getYear() );
            catMap.put( catDate, row );
        }

        List<ReportRow> results = Lists.newArrayList();
        for (CategoryDate categoryDate : catMap.keySet()) {
            List<ReportRow> sumMe = catMap.get( categoryDate );
            results.add( sum( sumMe, categoryDate.category, categoryDate.datePart, categoryDate.year ));
        }
        return results;
    }
}
