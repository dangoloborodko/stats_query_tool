package org.ici.stats.utils;

import com.google.common.collect.Lists;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.Level;
import org.joda.time.DateTime;

import java.util.List;

public class CategoryUtils {

    public static List<String> getNames( List<Category> cats ) {
        List<String> names  = Lists.newArrayList();
        if ( cats != null ) {
            for (Category cat : cats) {
                names.add( cat.getName() );
            }
        }
        return names;
    }

    public static List<Category> getChildlessChildren( Category root ) {
        List<Category> flatList = getFlatList( root );
        List<Category> singles = Lists.newArrayList();
        for ( Category cat : flatList ) {
            if ( !cat.hasChildren() ) {
                singles.add( cat );
            }
        }
        return singles;
    }

    public static List<Category> getFlatList( List<Category> categories ) {
        List<Category> flat = Lists.newArrayList();
        for ( Category cat : categories ) {
            flat.addAll( getFlatList( cat ));
        }
        return flat;
    }

    public static DateTime getEarliestAvailableDate( List<Category> categories ) {
        if ( categories == null || categories.isEmpty() ) {
            return null;
        }
        DateTime earliest = null;
        for (Category category : categories ) {
            if ( category.getEarliestDate() != null ) {
                if ( earliest == null ) {
                    earliest = category.getEarliestDate();
                } else {
                    if ( earliest.isBefore( category.getEarliestDate() )) {
                        earliest = category.getEarliestDate();
                    }
                }
            }
        }
        return earliest;
    }


    public static List<Category> getFlatList( Category root ) {
        List<Category> cats = Lists.newArrayList();
        cats.add( root );
        if ( root.hasChildren() ) {
            for ( Category child : root.getChildren() ) {
                cats.addAll( getFlatList( child ));
            }
        }
        return cats;
    }

    public static List<Category> categoriesAtLevel(List<Category> categories, Level level ) {
        List<Category> cats = Lists.newArrayList();
        for (Category category : categories) {
            List<Category> flat = getFlatList( category );
            for (Category child  : flat) {
                if ( child.getLevels().contains( level )) {
                    cats.add( child );
                }
            }
        }
        return cats;
    }

    public static List<Category> categoriesWithoutLevel(List<Category> categories) {
        List<Category> cats = Lists.newArrayList();
        for (Category category : categories) {
            List<Category> flat = getFlatList( category );
            for (Category child  : flat) {
                if ( child.getLevels().isEmpty()) {
                    cats.add( child );
                }
            }
        }
        return cats;
    }

    public static List<Integer> getIobsForCategory( Category category ) {
        List<Integer> results = Lists.newArrayList();
        if ( category.getId() > 0 && !category.hasChildren() ) {
            results.add( category.getId() );
        }
        if ( category.hasChildren() ) {
            for (Category childCat : category.getChildren()) {
                results.addAll( getIobsForCategory( childCat ));
            }
        }
        return results;
    }
}
