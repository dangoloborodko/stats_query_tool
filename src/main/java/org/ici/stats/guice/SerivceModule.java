package org.ici.stats.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import org.ici.config.Configuration;
import org.ici.stats.dao.*;
import org.ici.stats.dao.impl.ConfigDAOImpl;
import org.ici.stats.dao.impl.EtfReportDAOImpl;
import org.ici.stats.dao.impl.SpecialReportDAOImpl;
import org.ici.stats.dao.impl.TrendsQueryDAOImpl;
import org.ici.stats.guice.db.DataSourceProvider;
import org.ici.stats.services.*;
import org.ici.stats.services.impl.ExcelExportServiceImpl;
import org.ici.stats.services.impl.MFSpecialReportsImpl;
import org.ici.stats.services.impl.ReportDefinitionServiceImpl;
import org.ici.stats.services.impl.ReportServiceImpl;

import javax.sql.DataSource;

/**
 * module that maps our services together
 */
public class SerivceModule extends AbstractModule {
    @Override
    protected void configure() {
        Names.bindProperties(this.binder(), Configuration.load( "stats" ));
        bind(ConfigDAO.class).to(ConfigDAOImpl.class);
        bind(TrendsQueryDAO.class).to(TrendsQueryDAOImpl.class);
        bind(EtfReportDAO.class).to(EtfReportDAOImpl.class);
        bind(ReportDefinitionService.class).to(ReportDefinitionServiceImpl.class);
        bind(SpecialReportDAO.class).to(SpecialReportDAOImpl.class);
        bind(DataSource.class).toProvider(DataSourceProvider.class);
        bind(ReportService.class).to(ReportServiceImpl.class);
        bind(MFSpecialReports.class).to(MFSpecialReportsImpl.class);
        bind(ExcelExportService.class).to(ExcelExportServiceImpl.class);
    }
}
