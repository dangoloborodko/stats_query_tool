package org.ici.stats.guice;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.ici.stats.servlets.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * servlet module that acts as the nerve center of our web application
 */
public class ServletModule extends com.google.inject.servlet.ServletModule {

    private static final Logger logger = LoggerFactory.getLogger(ServletModule.class);

    @Override
    protected void configureServlets() {
        serve( "/" ).with( EtfMutualChoiceServlet.class );
        serve( "/etf" ).with( EtfChoiceServlet.class );
        serve( "/mutual" ).with( MutualChoiceServlet.class );
        serve( "/datapoints" ).with( DataPointServlet.class );
        serve( "/mf/objectives" ).with( MutualFundObjectivesServlet.class );
        serve( "/specialty/objectives" ).with( SpecialtyObjectivesServlet.class );
        serve( "/etf/objectives" ).with( EtfObjectivesServlet.class );
        serve( "/dates" ).with( DatesServlet.class );
        serve( "/report" ).with( ReportServlet.class );
        serve( "/report/export" ).with( ReportExportServlet.class );
        ResourceConfig rc = new PackagesResourceConfig( "org.ici.stats.rest");
        for ( Class<?> resource : rc.getClasses() ) {
            logger.info( "adding resource class: {}", resource );
            bind( resource ).in( Singleton.class );
        }

        // Route all requests through GuiceContainer
        serve( "/rest/*" ).with( GuiceContainer.class );
    }

    @Provides
    @Singleton
    public JacksonJsonProvider jacksonProvider() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure( SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure( JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true );
        mapper.configure( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false );
        mapper.registerModule( new JodaModule() );
        return new JacksonJsonProvider(mapper);
    }
}

