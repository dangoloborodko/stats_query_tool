package org.ici.stats.guice;

import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.ici.stats.dao.QuickQueryDao;
import org.ici.stats.dao.impl.QuickQueryDaoImpl;
import org.ici.stats.db.mappers.Groupiob12Mapper;
import org.ici.stats.db.mappers.Groupiob12Microiob12Mapper;
import org.ici.stats.db.mappers.Quickquery12SpecialMapper;
import org.ici.stats.guice.db.JodaDateTimeTypeHandler;
import org.joda.time.DateTime;
import org.mybatis.guice.MyBatisModule;

public class DbModule extends MyBatisModule {
    @Override
    protected void initialize() {
        environmentId( "dev" );
        addMapperClass(Groupiob12Mapper.class );
        addMapperClass(Quickquery12SpecialMapper.class );
        addMapperClass(Groupiob12Microiob12Mapper.class );
        bind(QuickQueryDao.class).to(QuickQueryDaoImpl.class);
        bindTransactionFactoryType(JdbcTransactionFactory.class);
        handleType(DateTime.class).with( JodaDateTimeTypeHandler.class);
    }
}
