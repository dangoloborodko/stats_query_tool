package org.ici.stats.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import org.ici.config.Configuration;

/**
 * default listener
 */
public class Listener extends GuiceServletContextListener {
    protected Injector getInjector() {
        Configuration.initializeLog4jIfExists();
        return Guice.createInjector( new SerivceModule(), new DbModule(), new ServletModule() );
    }
}
