package org.ici.stats.guice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * utilities to help load the correct properties file
 */
class ModuleUtils {
    private static final Logger logger = LoggerFactory.getLogger(ModuleUtils.class.getName());
    private static final String APP_PROPERTIES_FILE = "/auth.server.%s.properties";

    private static String getEnvironmentString() {
        return System.getProperty( "ici.env", "default" );
    }
    private static String getPropertiesName() {
        String name = getEnvironmentString();
        return String.format( APP_PROPERTIES_FILE, name );
    }

    static Properties loadProps() {
        return loadProps( getPropertiesName() );
    }

    static Properties loadProps(String filename) {
        logger.info("loading properties from " + filename);
        InputStream in = ModuleUtils.class.getResourceAsStream(filename);
        if ( in == null ) {
            String msg = String.format( "unable to find properties file: %s", filename );
            logger.error(msg);
            throw new RuntimeException( "unable to find file: " + filename + " on the classpath" );
        } else {
            Properties props = new Properties();
            try {
                props.load( in );
            } catch ( IOException ioe ) {
                logger.error( "error loading properties for file: {}", filename, ioe);
                RuntimeException re = new RuntimeException();
                re.initCause( ioe );
                throw re;
            }
            return props;
        }
    }
}
