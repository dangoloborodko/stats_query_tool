package org.ici.stats.guice.db;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

/**
 * Provide a pooled sybase driver as a fallback
 */
@Singleton
public class SybaseDatasourceProvider implements Provider<DataSource> {

    private static final Logger logger = LoggerFactory.getLogger(SybaseDatasourceProvider.class);

    private final HikariDataSource ds;

    @Inject
    public SybaseDatasourceProvider(
            @Named("db.url") String url,
            @Named("db.driver") String driverClass,
            @Named("db.user") String user,
            @Named("db.password") String password ) {
        boolean success = loadDriver( SybaseDatasourceProvider.class.getClassLoader(), driverClass );
        if ( !success ) {
            throw new RuntimeException( "unable to find driver: " + driverClass );
        }
        this.ds = new HikariDataSource();
        ds.setJdbcUrl( url );
        ds.setUsername( user );
        ds.setPassword( password );
        ds.setConnectionTestQuery( "select 1");
    }

    @Override
    public DataSource get() {
        return ds;
    }

    private static boolean loadDriver(ClassLoader classLoader, String driverClassName) {
        try {
            classLoader.loadClass(driverClassName).newInstance();
            return true;
        } catch (IllegalAccessException var3) {
            return true;
        } catch (Exception var4) {
            return false;
        }
    }

}
