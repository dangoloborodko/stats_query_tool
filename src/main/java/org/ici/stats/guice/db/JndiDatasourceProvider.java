package org.ici.stats.guice.db;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * looks up a data source using jndi
 */
@Singleton
public class JndiDatasourceProvider implements Provider<Optional<DataSource>> {

    private static final Logger logger = LoggerFactory.getLogger(JndiDatasourceProvider.class);

    private final Optional<DataSource> ds;

    @Inject
    public JndiDatasourceProvider(@Named("db.jndi.name") String jndiName) {
        Optional<DataSource> _ds;
        try {
            Context ctx = new InitialContext();
            try {
                _ds = Optional.of((DataSource) ctx.lookup( jndiName ));
            } catch ( NamingException e ) {
                logger.error( "unable to find jndi: {}", jndiName );
                _ds = Optional.absent();
            }
        } catch ( NamingException e ) {
            logger.error( "error getting context, possibly not running in container" );
            _ds = Optional.absent();
        }
        this.ds = _ds;
    }

    @Override
    public Optional<DataSource> get() {
        return ds;
    }
}
