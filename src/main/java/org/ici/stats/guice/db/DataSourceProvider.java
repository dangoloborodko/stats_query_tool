package org.ici.stats.guice.db;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

/**
 * provider the will use jndi if available or sybase as a default
 */

@Singleton
public class DataSourceProvider implements Provider<DataSource> {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceProvider.class);

    private final DataSource ds;

    @Inject
    public DataSourceProvider(SybaseDatasourceProvider sybaseDataSourceProvider, JndiDatasourceProvider jndiDataSourceProvider) {
        Optional<DataSource> jndiDs = jndiDataSourceProvider.get();
        if ( jndiDs.isPresent() ) {
            ds = jndiDs.get();
        } else {
            ds = sybaseDataSourceProvider.get();
        }
    }

    @Override
    public DataSource get() {
        return ds;
    }
}
