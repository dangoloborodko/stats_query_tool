package org.ici.stats.guice.db;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

@MappedJdbcTypes( JdbcType.TIMESTAMP )
@MappedTypes( DateTime.class )
public class JodaDateTimeTypeHandler extends BaseTypeHandler<DateTime> {

    private static final Logger logger = LoggerFactory.getLogger(JodaDateTimeTypeHandler.class);

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, DateTime dateTime, JdbcType jdbcType) throws SQLException {
        logger.debug( "setting non-null in pos: {} to value: {}", i, dateTime );
        ps.setTimestamp(i, new Timestamp( dateTime.toDate().getTime() ));
    }

    @Override
    public DateTime getNullableResult(ResultSet resultSet, String s) throws SQLException {
        logger.debug( "getting nullable result for {}" ,s );
        Timestamp t = resultSet.getTimestamp( s );
        if ( t == null ) {
            return null;
        } else {
            DateTime result = new DateTime( t.getTime() );
            logger.debug( "returning date time: {}", result );
            return result;
        }
    }

    @Override
    public DateTime getNullableResult(ResultSet resultSet, int i) throws SQLException {
        logger.debug( "getting nullable result for {}", i );
        Timestamp t = resultSet.getTimestamp( i );
        if ( t == null ) {
            return null;
        } else {
            DateTime result = new DateTime( t.getTime() );
            logger.debug( "returning date time: {}", result );
            return result;
        }
    }

    @Override
    public DateTime getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        logger.debug( "getting nullable result for {}", i );
        Timestamp t = callableStatement.getTimestamp( i );
        if ( t == null ) {
            return null;
        } else {
            DateTime result = new DateTime( t.getTime() );
            logger.debug( "returning date time: {}", result );
            return result;
        }
    }
}
