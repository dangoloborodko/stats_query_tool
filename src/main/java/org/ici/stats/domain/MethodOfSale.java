package org.ici.stats.domain;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

public class MethodOfSale {
    private final String name;
    private final int id;
    private final List<MethodOfSale> children;
    private final AccessLevel accessLevel;

    public enum AccessLevel { Public, AdminOnly }

    public MethodOfSale(String name, int id, MethodOfSale... children) {
        this.name = name;
        this.id = id;
        this.children = children != null && children.length != 0 ? ImmutableList.copyOf(children) : Collections.EMPTY_LIST;
        accessLevel = AccessLevel.Public;
    }

    public MethodOfSale(String name, int id) {
        this( name, id, AccessLevel.Public );
    }

    public MethodOfSale(String name, int id, AccessLevel level ) {
        this.name = name;
        this.id = id;
        this.children = Lists.newArrayList();
        this.accessLevel = level;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public List<MethodOfSale> getChildren() {
        return children;
    }

    public boolean hasChildren() {  return children != null && !children.isEmpty(); }

    public boolean isAdminOnly() {
        return AccessLevel.AdminOnly.equals( accessLevel );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MethodOfSale that = (MethodOfSale) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (children != null ? !children.equals(that.children) : that.children != null) return false;
        return accessLevel == that.accessLevel;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + (children != null ? children.hashCode() : 0);
        result = 31 * result + (accessLevel != null ? accessLevel.hashCode() : 0);
        return result;
    }
}
