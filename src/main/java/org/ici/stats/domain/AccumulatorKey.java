package org.ici.stats.domain;

import org.joda.time.DateTime;

public class AccumulatorKey  {
    private final int iob12;
    private final String iob12Name;
    private final DateTime date;

    public AccumulatorKey(int iob12, String iob12Name, DateTime date) {
        this.iob12 = iob12;
        this.iob12Name = iob12Name;
        this.date = date;
    }

    public int getIob12() {
        return iob12;
    }

    public String getIob12Name() {
        return iob12Name;
    }

    public DateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccumulatorKey that = (AccumulatorKey) o;
        return iob12 == that.iob12 && date.equals(that.date);
    }

    @Override
    public int hashCode() {
        int result = iob12;
        result = 31 * result + date.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AccumulatorKey{" +
                "iob12=" + iob12 +
                ", iob12Name='" + iob12Name + '\'' +
                ", date=" + date +
                '}';
    }
}
