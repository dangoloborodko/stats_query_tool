package org.ici.stats.domain;

import com.google.common.collect.Lists;

import java.util.List;

public class MosUtils {
    public static List<MethodOfSale> getFlatList(List<MethodOfSale> methodsOfSale) {
        List<MethodOfSale> mos = Lists.newArrayList();
        for (MethodOfSale methodOfSale : methodsOfSale) {
            mos.add( methodOfSale );
            if ( methodOfSale.hasChildren() ) {
                mos.addAll( getFlatList( methodOfSale.getChildren() ));
            }
        }
        return mos;
    }

    public static List<Integer> getMosIds( List<MethodOfSale> methodsOfSale ) {
        if ( methodsOfSale.isEmpty() ) {
            return Lists.newArrayList();
        }

        List<Integer> results = Lists.newArrayList();
        for (MethodOfSale methodOfSale : methodsOfSale) {
            if ( methodOfSale.getId() > 0 ) {
                results.add( methodOfSale.getId() );
            }
            if ( methodOfSale.hasChildren() ) {
                results.addAll( getMosIds( methodOfSale.getChildren() ));
            }
        }
        return results;
    }
}
