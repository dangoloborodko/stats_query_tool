package org.ici.stats.domain;


import org.ici.stats.utils.Constants;

/**
* Created by jalexander on 10/17/2014.
*/
public enum Period {
    CurrentMonth( "Current Month", Constants.REPORT_CURRENT_MONTH ),
    Monthly( "Monthly", Constants.REPORT_MONTHLY ),
    Quarterly( "Quarterly", Constants.REPORT_QUARTERLY ),
    Annual( "Annual", Constants.REPORT_ANNUALLY );

    private final String fullName;
    private final int constant;

    Period(String fullName, int constant) {
        this.fullName = fullName;
        this.constant = constant;
    }

    public int getConstant() {
        return constant;
    }

    public String getFullName() {
        return fullName;
    }

    public static Period fromConstant( int constant ) {
        switch ( constant ) {
            case Constants.REPORT_MONTHLY:
                return Monthly;
            case Constants.REPORT_ANNUALLY:
                return Annual;
            case Constants.REPORT_QUARTERLY:
                return Quarterly;
            case Constants.REPORT_CURRENT_MONTH:
                return CurrentMonth;
            default:
                throw new IllegalArgumentException( "unable to convert: " + constant + " to Period");
        }
    }
}
