package org.ici.stats.domain;

public enum Level { All, Super, Mega, High, Mid }
