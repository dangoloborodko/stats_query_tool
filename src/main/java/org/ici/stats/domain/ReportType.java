package org.ici.stats.domain;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Maps;
import org.ici.stats.utils.Constants;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

public enum ReportType {
    MutualFunds( "Monthly Mutual Fund Data", -1, Constants.MONTHLY_TRENDS ),
    ActiveIndex("Active vs Index", 0, Constants.ACTIVE_INDEX),
    Target("Target Date/Lifestyle", 1, Constants.TARGET_LIFECYCLE),
    Retirement("Retirement", 5, Constants.RETIREMENT),
    FOF("Fund of Funds", 6, Constants.FOF),
    EmergingMarkets("Emerging Markets Debt", 4, Constants.EMERGING_MARKETS ),
    FloatingRate("Floating Rate", 2, Constants.FLOATING_RATE),
    MarketCapitalization("Market Capitalization", 7, Constants.MARKET_CAPITALIZATION),
    TIPs("Inflation Protected/TIPS", 3, Constants.INFLATION_PROTECTED),
    Sector( "Sector", 8, Constants.SECTOR ),
    AlternativeStrategies( "Alternative Strategies", 9, Constants.ALTERNATIVE_STRATEGIES ),
    ETF( "Exchange Traded Funds", -1, "ETF" ),
    ETFActiveIndex( "Active vs Index", 0, "ETF_AI" ),
    ETF_FOF( "FOFs", -1, "ETF_FOF" ),
    ETF_1940( "1940 Act vs Non-1940 Act", -1, "ETF_1940" ),
    MF_ETF( "MF/ETF", -1, "MF_ETF" );

    // report id used to pass to stored procedure
    private final int dbId;
    // report id when looking up values in the report-param-config.xml
    private final String xmlId;
    // pretty report name
    private final String name;

    private static final Map<String,ReportType> reportIdMap = Maps.newHashMap();

    // removed retirement
    public static Set<ReportType> SPECIAL_REPORTS = ImmutableSet.of( ActiveIndex, Target, FOF, EmergingMarkets, FloatingRate, TIPs, Sector, AlternativeStrategies, Retirement, MarketCapitalization );

    public static Set<ReportType> ETF_SPECIAL_REPORTS = ImmutableSet.of( ETFActiveIndex, MF_ETF );

    public static Set<ReportType> ALL_ETF_REPORTS = ImmutableSet.<ReportType>builder().addAll( ETF_SPECIAL_REPORTS ).add( ETF ).build();

    public static Set<ReportType> AVAILABLE_REPORTS = ImmutableSet.<ReportType>builder().addAll( SPECIAL_REPORTS ).add( MutualFunds ).build();

    public static SortedSet<ReportType> SORTED_SPECIAL_REPORTS = ImmutableSortedSet.copyOf(new Comparator<ReportType>() {
        @Override
        public int compare(ReportType o1, ReportType o2) {
            return o1.getName().compareToIgnoreCase( o2.getName() );
        }
    }, SPECIAL_REPORTS );

    /**
     * report type
     * @param name
     * @param dbId number used by stored procedure to describe the report
     * @param xmlId report id from xml that is used to describe the report
     */
    ReportType(String name, int dbId, String xmlId ) {
        this.dbId = dbId;
        this.xmlId = xmlId;
        this.name = name;
    }

    static {
        for ( ReportType report : ReportType.values() ) {
            reportIdMap.put( report.getXmlId(), report );
        }
    }

    public int getDbId() {
        return dbId;
    }

    public String getXmlId() {
        return xmlId;
    }

    public String getName() {
        return name;
    }
    public final static ReportType getReportByXmlId( String xml ) {
        return reportIdMap.get( xml );
    }
}
