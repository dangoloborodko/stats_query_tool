package org.ici.stats.domain;

public enum EtfType {
    Total, Active, Index
}
