package org.ici.stats.domain;

import org.ici.stats.utils.DateUtils;
import org.joda.time.DateTime;

/**
 * holder for date ranges
 */
public class DateRange {
    private final DateTime start;
    private final DateTime end;

    public DateRange(DateTime start, DateTime end) {
        this.start = start;
        this.end = end;
    }

    public DateRange(String start, String end) {
        this.start = DateUtils.mmYYYY.parseDateTime( start );
        this.end = DateUtils.mmYYYY.parseDateTime( end );
    }

    public DateTime getStart() {
        return start;
    }

    public DateTime getEnd() {
        return end;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DateRange{");
        sb.append("start=").append((start == null ) ? "null" : DateUtils.mmYYYY.print(start));
        sb.append(", end=").append(( end == null ) ? "null" : DateUtils.mmYYYY.print(end));
        sb.append('}');
        return sb.toString();
    }
}
