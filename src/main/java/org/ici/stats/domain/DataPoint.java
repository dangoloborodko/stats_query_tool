package org.ici.stats.domain;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * data points we can select
 */
public enum DataPoint {
    TotalNetAssets( "Total Net Assets", true, false),
    NumberOfFunds("Number of Funds/Classes", true, false),
    LiquidAssets("Liquid Assets", false, false),
    TotalSales("Total Sales", true, true ),
    NewSales("New Sales", true, true),
    ReinvestedDividends("Reinvested Dividends", true, true ),
    Redemptions("Redemptions", true, true ),
    NetSales("Net Sales", true, true ),
    NetNewSales("Net New Sales", true, true ),
    NetNewCashFlow("Net New Cash Flow", true, true ),
    SalesExchanges("Sales Exchanges", true, true ),
    RedemptionExchanges("Redemption Exchanges", true, true ),
    NetExchanges("Net Exchanges", true, true ),
    CommonStockPurchases("Common Stock Purchases", false, true),
    CommonStockSales("Common Stock Sales", false, true ),
    NetCommonStockPurchases("Net Common Stock Purchases", false, true ),
    OtherPortfolioPurchases( "Other Portfolio Purchases", false, true ),
    OtherPortfolioSales( "Other Portfolio Sales", false, true ),
    NetOtherPortfolioPurchases( "Net Other Portfolio Purchases", false, true ),
    AllPortfolioPurchases( "All Portfolio Purchases", false, true ),
    AllPortfolioSales( "All Portfolio Sales", false, true ),
    NetPortfolioPurchases( "Net Portfolio Purchases", false, true ),
    NetIssuance( "Net Issuance", false, false ),
    GrossIssuance( "Gross Issuance", false, false ),
    GrossRedemptions( "Gross Redemptions", false, false ),
    Flows( "Flows", false, false );

    private final String fullName;
    private final boolean mosCompatible;
    private final boolean transactionAggregate;

    private static final Map<String,DataPoint> fullNameToDataPointMap = Maps.newHashMap();

    DataPoint(String fullName, boolean mosCompatible, boolean transactionAggregate) {
        this.fullName = fullName;
        this.mosCompatible = mosCompatible;
        this.transactionAggregate = transactionAggregate;
    }

    static {
        for ( DataPoint dataPoint : DataPoint.values() ) {
            fullNameToDataPointMap.put( dataPoint.fullName, dataPoint);
        }
    }

    public static List<DataPoint> getDataPoints( String[] values ) {
        List<DataPoint> points = Lists.newArrayList();
        for ( String value : values ) {
            DataPoint point = fullNameToDataPointMap.get(value);
            Preconditions.checkNotNull(point, "unable to find data point with name '%s'", value);
            points.add( point );
        }
        return points;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean isMosCompatible() {
        return mosCompatible;
    }

    public boolean isTransactionAggregate() {
        return transactionAggregate;
    }
}
