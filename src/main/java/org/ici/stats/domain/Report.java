package org.ici.stats.domain;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.ici.stats.utils.CategoryUtils;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Report implements ReportDefinition, Serializable {
    private final ReportType type;
    private final List<Category> categories;
    private final List<Category> allCategories;
    private final List<DataPointGroup> dataPoints;
    private final List<DataPoint> allDataPoints;
    private final List<MethodOfSale> methodsOfSale;
    private final List<MethodOfSale> allMethodsOfSale;
    private final Map<String,Category> categoryMap;
    private final Map<String,MethodOfSale> methodOfSaleMap;
    private final DateTime startDate;
    private final DateTime mosStartDate;
    private final String footNote;
    private final boolean allowPartialQuarters;

    private final Map<DataPoint, String> fieldNameMap;
    private final Map<DataPoint, String> fieldEquationMap;

    public Report(ReportType type, List<Category> categories, List<DataPointGroup> dataPoints, List<MethodOfSale> methodsOfSale, DateTime startDate, DateTime mosStartDate, String footNote, boolean allowPartialQuarters, Map<DataPoint, String> fieldNameMap, Map<DataPoint, String> fieldEquationMap) {
        this.type = type;
        this.categories = categories;
        this.dataPoints = dataPoints;
        this.methodsOfSale = methodsOfSale;
        this.startDate = startDate;
        this.mosStartDate = mosStartDate;
        this.footNote = footNote;
        this.allowPartialQuarters = allowPartialQuarters;
        this.allCategories = ImmutableList.copyOf(CategoryUtils.getFlatList( categories ));
        this.fieldNameMap = fieldNameMap;
        this.fieldEquationMap = fieldEquationMap;

        Map<String,Category> catMap = Maps.newHashMap();
        for (Category cat : allCategories) {
            catMap.put( cat.getName(), cat );
        }
        categoryMap = ImmutableMap.copyOf( catMap );
        allMethodsOfSale = ImmutableList.copyOf( MosUtils.getFlatList( methodsOfSale ));

        Map<String,MethodOfSale> mosMap = Maps.newHashMap();
        for (MethodOfSale mos : allMethodsOfSale) {
            mosMap.put( mos.getName(), mos );
        }
        methodOfSaleMap = ImmutableMap.copyOf( mosMap );

        // create a "flattened" list of data points
        FluentIterable<DataPoint> pointsIter = FluentIterable.from( dataPoints ).transformAndConcat(new Function<DataPointGroup, Iterable<DataPoint>>() {
            @Override
            public Iterable<DataPoint> apply(DataPointGroup dataPointGroup) {
                return dataPointGroup.getPoints();
            }
        });
        allDataPoints = ImmutableList.copyOf( pointsIter );
    }



    public ReportType getReportType() {
        return type;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Category getCategoryByName( String name ) {
        return categoryMap.get( name );
    }

    public List<Category> getAllCategories() { return allCategories; }

    public List<DataPoint> getAllDataPoints() {
        return allDataPoints;
    }

    public List<MethodOfSale> getAllMethodsOfSale() {
        return allMethodsOfSale;
    }

    public MethodOfSale getMethodOfSaleByName( String name ) {
        return methodOfSaleMap.get( name );
    }

    public List<DataPointGroup> getDataPointGroups() {
        return dataPoints;
    }

    @Override
    public List<MethodOfSale> getMethodsOfSale() {
        return methodsOfSale;
    }

    @Override
    public String getFieldName(DataPoint point) {
        return fieldNameMap.get( point );
    }

    @Override
    public String getFieldEquation(DataPoint point) {
        return fieldEquationMap.get( point );
    }

    public String getFootNote() {
        return footNote;
    }

    public String getName() {
        return type.getName();
    }

    @Override
    public DateTime getStartDate() {
        return startDate;
    }

    @Override
    public DateTime getMosStartDate() {
        return mosStartDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Report report = (Report) o;

        return type == report.type;
    }

    @Override
    public int hashCode() {
        return type != null ? type.hashCode() : 0;
    }
}
