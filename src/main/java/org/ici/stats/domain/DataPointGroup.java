package org.ici.stats.domain;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Created by jalexander on 10/28/2014.
 */
public class DataPointGroup {
    private final String name;
    private final List<DataPoint> points;

    public DataPointGroup( String name, List<DataPoint> points) {
        this.name = name;
        this.points = ImmutableList.copyOf( points );
    }

    public DataPointGroup( String name, DataPoint ... points ) {
        this.name = name;
        this.points = ImmutableList.copyOf( points );
    }

    public String getName() {
        return name;
    }

    public List<DataPoint> getPoints() {
        return points;
    }
}
