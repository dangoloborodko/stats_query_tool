package org.ici.stats.domain.builders;

import com.google.common.collect.Lists;
import org.ici.stats.domain.Category;
import org.ici.stats.domain.Level;

import java.util.Collections;
import java.util.List;

public class CategoryBuilder {
    private String name;
    private int id;
    private List<Level> levels = Lists.newArrayList();
    private List<Category> children = Lists.newArrayList();

    public CategoryBuilder(String name) {
        this.name = name;
    }

    public CategoryBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public CategoryBuilder addLevels(Level level) {
        this.levels.add( level );
        return this;
    }

    public CategoryBuilder addChild(Category child ) {
        this.children.add( child );
        return this;
    }

    public CategoryBuilder addChildren(Category ... newChildren ) {
        Collections.addAll( this.children, newChildren );
        return this;
    }


    public Category createCategory() {
        return new Category(name, id, levels, children );
    }
}