package org.ici.stats.domain.builders;

import com.google.common.collect.Lists;
import org.ici.stats.domain.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

public class ReportRequestBuilder {

    private static final DateTimeFormatter formatter = DateTimeFormat.forPattern( "MM/dd/yyyy");

    private ReportType report;
    private Period period;
    private DateTime start;
    private DateTime end;
    private List<DataPoint> dataPoints = Lists.newArrayList();
    private List<Category> categories = Lists.newArrayList();
    private List<MethodOfSale> methodsOfSale = Lists.newArrayList();
    private EtfType etfType = EtfType.Total;
    private boolean includeEtfFundOfFunds;

    public ReportRequestBuilder(ReportType report) {
        this.report = report;
    }

    public ReportRequestBuilder setPeriod(Period period) {
        this.period = period;
        return this;
    }

    public ReportRequestBuilder setStart(DateTime start) {
        this.start = start;
        return this;
    }

    public ReportRequestBuilder setStart(String start) {
        this.start = formatter.parseDateTime( start );
        return this;
    }

    public ReportRequestBuilder setEnd(DateTime end) {
        this.end = end;
        return this;
    }

    public ReportRequestBuilder setEnd(String end) {
        this.end = formatter.parseDateTime( end );
        return this;
    }

    public ReportRequestBuilder setDataPoints(List<DataPoint> dataPoints) {
        this.dataPoints = dataPoints;
        return this;
    }

    public ReportRequestBuilder setCategories(List<Category> categories) {
        this.categories.clear();
        this.categories.addAll( categories );
        return this;
    }

    public ReportRequestBuilder addCategory( Category cat ) {
        this.categories.add( cat );
        return this;
    }

    public ReportRequestBuilder setMethodsOfSale(List<MethodOfSale> methodsOfSale) {
        this.methodsOfSale.clear();
        this.methodsOfSale.addAll( methodsOfSale );
        return this;
    }

    public ReportRequestBuilder addMethodsOfSale(MethodOfSale methodOfSale) {
        this.methodsOfSale.add( methodOfSale );
        return this;
    }

    public ReportRequestBuilder setIncludeEtfFundOfFunds(boolean includeEtfFundOfFunds) {
        this.includeEtfFundOfFunds = includeEtfFundOfFunds;
        return this;
    }

    public ReportRequestBuilder setEtfType(EtfType etfType) {
        this.etfType = etfType;
        return this;
    }

    public ReportRequest createReportRequest() {
        return new ReportRequest(report, period, start, end, dataPoints, categories, methodsOfSale, etfType, includeEtfFundOfFunds);
    }

    public ReportRequestBuilder addDataPoint(DataPoint point) {
        this.dataPoints.add( point );
        return this;
    }
}