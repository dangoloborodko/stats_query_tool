package org.ici.stats.domain.builders;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.ici.stats.utils.DateUtils;
import org.ici.stats.domain.*;
import org.joda.time.DateTime;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ReportBuilder {
    private ReportType type;
    private List<Category> categories = Lists.newArrayList();
    private List<MethodOfSale> methodsOfSale = Lists.newArrayList();
    private List<DataPointGroup> dataPoints = Lists.newArrayList();
    private DateTime startDate;
    private DateTime mosStartDate;
    private String footNote;
    private boolean allowPartialQuarters;
    private Map<DataPoint, String> fieldNameMap = Maps.newHashMap();
    private Map<DataPoint, String> fieldEquationMap = Maps.newHashMap();

    public ReportBuilder setType(ReportType type) {
        this.type = type;
        return this;
    }

    public ReportBuilder setStartDate(String start ) {
        this.startDate = DateUtils.mmYYYY.parseDateTime( start );
        this.mosStartDate = this.startDate;
        return this;
    }

    public ReportBuilder setMosStartDate(String mosStart) {
        this.mosStartDate = DateUtils.mmYYYY.parseDateTime( mosStart );
        return this;
    }

    public ReportBuilder setCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    public ReportBuilder addCategories( Category ... categories ) {
        Collections.addAll(this.categories, categories);
        return this;
    }

    public ReportBuilder addMethodsOfSale( MethodOfSale ... methodsOfSale ) {
        Collections.addAll(this.methodsOfSale, methodsOfSale);
        return this;
    }

    public ReportBuilder addDataPoints( DataPointGroup ... dataPoints ) {
        Collections.addAll( this.dataPoints, dataPoints );
        return this;
    }

    public ReportBuilder setDataPointFieldMap(Map<DataPoint, String> fieldMap) {
        fieldNameMap.putAll( fieldMap );
        return this;
    }

    public ReportBuilder setDataPointEquation( Map<DataPoint, String> fieldEquationMap ) {
        this.fieldEquationMap.putAll( fieldEquationMap );
        return this;
    }

    public ReportBuilder setDataPoints(List<DataPointGroup> dataPoints) {
        this.dataPoints = dataPoints;
        return this;
    }

    public ReportBuilder setDataPoints(DataPointGroup group) {
        this.dataPoints = Lists.newArrayList( group );
        return this;
    }


    public ReportBuilder setFootNote(String footNote) {
        this.footNote = footNote;
        return this;
    }

    public ReportBuilder setAllowPartialQuarters( boolean allow ) {
        this.allowPartialQuarters = allow;
        return this;
    }

    public Report createReport() {
        return new Report(type, categories, dataPoints, methodsOfSale, startDate, mosStartDate, footNote, allowPartialQuarters, fieldNameMap, fieldEquationMap);
    }
}