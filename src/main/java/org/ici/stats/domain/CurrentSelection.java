package org.ici.stats.domain;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.List;

/**
 * Current selection of report options
 */
public class CurrentSelection {
    // default to current month
    private Period period = Period.CurrentMonth;
    private List<DataPoint> dataPoints = Lists.newArrayList();
    private List<Category> categories = Lists.newArrayList();
    private List<MethodOfSale> methodOfSales = Lists.newArrayList();
    private DateTime startDate;
    private DateTime endDate;
    private Report report;

    public Period getPeriod() {
        return period;
    }

    public boolean isCurrentMonth() {
        return Period.CurrentMonth.equals( period );
    }

    public boolean isMonthly() {
        return Period.Monthly.equals( period );
    }

    public boolean isQuarterly() {
        return Period.Quarterly.equals( period );
    }

    public boolean isAnnual() {
        return Period.Annual.equals( period );
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public List<DataPoint> getDataPoints() {
        return dataPoints;
    }

    public void setDataPoints(List<DataPoint> dataPoints) {
        this.dataPoints = dataPoints;
    }

    @SuppressWarnings("unused")
    public String getDataPointsStr() {
        if ( getDataPoints() == null ) {
            return "";
        }
        List<String> names = Lists.newArrayList();
        for (DataPoint dataPoint : getDataPoints()) {
            names.add( dataPoint.getFullName() );
        }
        return Joiner.on( "<br/> ").join( names );
    }

    @SuppressWarnings("unused")
    public String getFundType() {
        if (  isETF() ) {
            return "ETF";
        } else {
            return "Mutual Fund";
        }
    }

    public boolean isETF() {
        return ( ReportType.ALL_ETF_REPORTS.contains( report.getReportType() ));
    }

    @SuppressWarnings("unused")
    public boolean isReadyToRun() {
        return ( !getDataPoints().isEmpty() && !getCategories().isEmpty() && !Strings.isNullOrEmpty(getDateRange() ));
    }

    /**
     * used by the sidebar jsp
     * @return flag to denote whether we can allow mos selection
     */
    @SuppressWarnings("unused")
    public boolean isMosAvailable() {
        boolean available = true;
        List<DataPoint> points = getDataPoints();
        if (points != null ) {
            for (DataPoint point : points) {
                if ( !point.isMosCompatible() ) {
                    available = false;
                    break;
                }
            }
        }
        return available;
    }


    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     * used by the sidebar jsp
     * @return comma separated list of methods of categories
     */
    @SuppressWarnings("unused")
    public String getCategoriesStr() {
        if ( getCategories() == null ) {
            return "";
        }
        List<String> names = Lists.newArrayList();
        for (Category cat : getCategories()) {
            names.add( cat.getName() );
        }
        return Joiner.on( "<br/> ").join( names );
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public List<MethodOfSale> getMethodsOfSale() {
        return methodOfSales;
    }

    public void setMethodsOfSale(List<MethodOfSale> methodOfSales) {
        this.methodOfSales = methodOfSales;
    }

    /**
     * used by the sidebar jsp
     * @return comma separated list of methods of sale
     */
    @SuppressWarnings("unused")
    public String getMethodsOfSaleStr() {
        if ( getMethodsOfSale() == null ) {
            return "";
        }
        List<String> names = Lists.newArrayList();
        for (MethodOfSale mos : getMethodsOfSale()) {
            names.add( mos.getName() );
        }
        return Joiner.on( "<br/> ").join( names );
    }

    private final static org.joda.time.format.DateTimeFormatter monthlyFormat = DateTimeFormat.forPattern("MMMM yyyy" );

    /**
     * used by the sidebar jsp
     * @return nice view of the selected dates
     */
    @SuppressWarnings("unused")
    public String getDateRange() {
        if ( getPeriod() == null ) {
            return null;
        }
        switch ( getPeriod() ) {
            case Annual:
                return Integer.toString(getStartDate().year().get()) + " - " + Integer.toString(getEndDate().year().get());
            case Quarterly:
                return "Quarter";
            case CurrentMonth:
                return "Current Month Totals (" + monthlyFormat.print( getEndDate() ) + ")";
            case Monthly:
            default:
                return monthlyFormat.print( getStartDate() ) + " - " + monthlyFormat.print( getEndDate() );
        }
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report _report) {
        this.report = _report;
    }
}
