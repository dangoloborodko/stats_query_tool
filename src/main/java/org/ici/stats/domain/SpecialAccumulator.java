package org.ici.stats.domain;

import org.ici.stats.db.domain.Quickquery12Special;

public class SpecialAccumulator {

    private long sreg_total;
    private long srein_total;
    private long rreg_total;
    private long sex_total;
    private long rex_total;
    private long tna_total;
    private long ltot_total;
    private long fundcount_total;
    private long classcount_total;
    private long mctotp_total;
    private long mctots_total;
    private long mctot_net_total;
    private long csppurchases_total;
    private long cspsales_total;
    private long csp_net_total;
    private long othppurchases_total;
    private long othpsales_total;
    private long othp_net_total;

    public long getSreg_total() {
        return sreg_total/1000;
    }

    public long getSrein_total() {
        return srein_total/1000;
    }

    public long getRreg_total() {
        return rreg_total/1000;
    }

    public long getSex_total() {
        return sex_total/1000;
    }

    public long getRex_total() {
        return rex_total/1000;
    }

    public long getTna_total() {
        return tna_total/1000;
    }

    public long getLtot_total() {
        return ltot_total/1000;
    }

    public long getFundcount_total() {
        return fundcount_total/1000;
    }

    public long getClasscount_total() {
        return classcount_total/1000;
    }

    public long getMctotp_total() {
        return mctotp_total/1000;
    }

    public long getMctots_total() {
        return mctots_total/1000;
    }

    public long getMctot_net_total() {
        return mctot_net_total/1000;
    }

    public long getCsppurchases_total() {
        return csppurchases_total/1000;
    }

    public long getCspsales_total() {
        return cspsales_total/1000;
    }

    public long getCsp_net_total() {
        return csp_net_total/1000;
    }

    public long getOthppurchases_total() {
        return othppurchases_total/1000;
    }

    public long getOthpsales_total() {
        return othpsales_total/1000;
    }

    public long getOthp_net_total() {
        return othp_net_total/1000;
    }

    public void add(Quickquery12Special special ) {
        sreg_total += nullSafe( special.getSreg());
        srein_total += nullSafe( special.getSrein());
        rreg_total += nullSafe( special.getRreg());
        sex_total += nullSafe( special.getSex());
        rex_total += nullSafe( special.getRex());
        tna_total += nullSafe( special.getTna());
        ltot_total += nullSafe( special.getLtot());
        fundcount_total += nullSafe( special.getFundcount());
        classcount_total += nullSafe( special.getClasscount());
        mctotp_total += nullSafe( special.getMctotp());
        mctots_total += nullSafe( special.getMctots());
        mctot_net_total += nullSafe( special.getMctotNet());
        csppurchases_total += nullSafe( special.getCsppurchases());
        cspsales_total += nullSafe( special.getCspsales());
        csp_net_total += nullSafe( special.getCspNet());
        othppurchases_total += nullSafe( special.getOthppurchases());
        othpsales_total += nullSafe( special.getOthpsales());
        othp_net_total += nullSafe( special.getOthpNet());
    }

    private static int nullSafe( Integer value ) {
        return value == null ? 0 : value;
    }
}
