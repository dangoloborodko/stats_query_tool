package org.ici.stats.domain;

import org.ici.stats.services.ReportUtils;

import java.util.List;

public class ReportResult {
    private final ReportRequest request;
    private final String fundCountLabel;
    private final List<ReportRow> data;

    public ReportResult(ReportRequest request, List<ReportRow> rows, String fundCountLabel) {
        this.request = request;
        this.data = rows;
        this.fundCountLabel = fundCountLabel;
    }

    public ReportRequest getRequest() {
        return request;
    }

    public List<ReportRow> getData() {
        return data;
    }

    public String getDisplayDate( ReportRow row ) {
        return ReportUtils.getDisplayDate( request.getPeriod(), row.getDatePeriod(), row.getYear() );
    }

    public String getFundCountLabel() {
        return fundCountLabel;
    }

    public boolean isShowFundCounts() {
        return ReportType.MutualFunds.equals( request.getReportType() );
    }

}
