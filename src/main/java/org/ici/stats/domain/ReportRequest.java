package org.ici.stats.domain;

import org.joda.time.DateTime;

import java.util.List;

public class ReportRequest {
    private final ReportType reportType;
    private final Period period;
    private final DateTime start;
    private final DateTime end;

    private final List<DataPoint> dataPoints;
    private final List<Category> categories;
    private final List<MethodOfSale> methodsOfSale;

    private final EtfType etfType;

    private final boolean includeEtfFundOfFunds;

    public ReportRequest(ReportType reportType, Period period, DateTime start, DateTime end, List<DataPoint> dataPoints, List<Category> categories, List<MethodOfSale> methodsOfSale, EtfType etfType, boolean includeEtfFundOfFunds) {
        this.reportType = reportType;
        this.period = period;
        this.start = start;
        this.end = end;
        this.dataPoints = dataPoints;
        this.categories = categories;
        this.methodsOfSale = methodsOfSale;
        this.etfType = etfType;
        this.includeEtfFundOfFunds = includeEtfFundOfFunds;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public Period getPeriod() {
        return period;
    }

    public DateTime getStart() {
        return start;
    }

    public DateTime getEnd() {
        return end;
    }

    public List<DataPoint> getDataPoints() {
        return dataPoints;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public List<MethodOfSale> getMethodsOfSale() {
        return methodsOfSale;
    }

    public EtfType getEtfType() {
        return etfType;
    }

    @Override
    public String toString() {
        return "ReportRequest{" +
                "reportType=" + reportType +
                ", period=" + period +
                ", start=" + start +
                ", end=" + end +
                ", dataPoints=" + dataPoints +
                ", categories=" + categories +
                ", methodsOfSale=" + methodsOfSale +
                ", etfType=" + etfType +
                ", includeEtfFundOfFunds=" + includeEtfFundOfFunds +
                '}';
    }

    public boolean isIncludeEtfFundOfFunds() {
        return includeEtfFundOfFunds;
    }

}
