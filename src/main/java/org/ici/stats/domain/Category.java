package org.ici.stats.domain;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.ici.stats.utils.DateUtils;
import org.joda.time.DateTime;

import java.util.List;

public class Category {
    private final String name;
    private final int id;
    private final IobLevel iobLevel;
    private final List<Level> levels;
    private final List<Category> children;
    private final DateTime earliestDate;

    public Category(String name, int id, List<Level> levels, List<Category> children) {
        this.name = name;
        this.id = id;
        this.levels = levels;
        this.children = ImmutableList.copyOf(children);
        this.iobLevel = null;
        this.earliestDate = null;
    }

    public Category(String name, int id, Category ... children) {
        this.name = name;
        this.id = id;
        this.levels = Lists.newArrayList();
        this.children = ImmutableList.copyOf(children);
        this.iobLevel = null;
        this.earliestDate = null;
    }

    public Category(String name, int id, Level level, Category ... children ) {
        this.name = name;
        this.id = id;
        this.levels = Lists.newArrayList( level );
        this.children = ImmutableList.copyOf(children);
        this.iobLevel = null;
        this.earliestDate = null;
    }

    public Category(String name, int id, IobLevel iobLevel, Level level, Category ... children ) {
        this.name = name;
        this.id = id;
        this.levels = Lists.newArrayList( level );
        this.children = ImmutableList.copyOf(children);
        this.iobLevel = iobLevel;
        this.earliestDate = null;
    }

    public Category(String name, int id, Level level1, Level level2, Category ... children ) {
        this.name = name;
        this.id = id;
        this.levels = Lists.newArrayList( level1, level2 );
        this.children = ImmutableList.copyOf(children);
        this.iobLevel = null;
        this.earliestDate = null;
    }

    public Category(String name, int id, Level level1, Level level2, Level level3, Category ... children ) {
        this.name = name;
        this.id = id;
        this.levels = Lists.newArrayList( level1, level2, level3 );
        this.children = ImmutableList.copyOf(children);
        this.iobLevel = null;
        this.earliestDate = null;
    }

    public Category(String name, int id, String earliest ) {
        this.name = name;
        this.id = id;
        this.levels = Lists.newArrayList();
        this.children = Lists.newArrayList();
        this.iobLevel = null;
        this.earliestDate = DateUtils.mmYYYY.parseDateTime( earliest );
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public List<Level> getLevels() {
        return levels;
    }

    public List<Category> getChildren() {
        return children;
    }

    public boolean hasChildren() {  return children != null && !children.isEmpty(); }

    public IobLevel getIobLevel() {
        return iobLevel;
    }

    public DateTime getEarliestDate() {
        return earliestDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (id != category.id) return false;
        if (!name.equals(category.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + id;
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", iobLevel=" + iobLevel +
                ", levels=" + levels +
                ", children=" + children +
                '}';
    }
}
