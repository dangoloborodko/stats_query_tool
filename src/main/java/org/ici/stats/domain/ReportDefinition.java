package org.ici.stats.domain;

import org.joda.time.DateTime;

import java.util.List;

public interface ReportDefinition {
    String getName();
    String getFootNote();
    List<Category> getCategories();
    List<DataPointGroup> getDataPointGroups();
    List<MethodOfSale> getMethodsOfSale();
    DateTime getStartDate();
    DateTime getMosStartDate();
    String getFieldName( DataPoint point );
    String getFieldEquation( DataPoint point );

}
