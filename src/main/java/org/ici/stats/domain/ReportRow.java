package org.ici.stats.domain;

import org.ici.stats.services.ReportUtils;

import java.util.Map;

public class ReportRow {
    private final String category;
    private final int fundCount;
    private final int datePeriod;
    private final int year;
    private final Map<DataPoint,Long> valueMap;

    public ReportRow(String category, int fundCount, int datePeriod, int year, Map<DataPoint, Long> valueMap) {
        this.category = category;
        this.fundCount = fundCount;
        this.datePeriod = datePeriod;
        this.year = year;
        this.valueMap = valueMap;
    }

    public int getDatePeriod() {
        return datePeriod;
    }

    public int getYear() {
        return year;
    }

    public int getFundCount() {
        return fundCount;
    }

    public String getCategory() {
        return category;
    }

    public Map<DataPoint, Long> getValueMap() {
        return valueMap;
    }

}
