$(document).ready( function() {
    $(".dropdown.icon").click( function() {
        handleClick( this );
    });

    $(".caret.right.icon").click( function() {
        handleClick( this );
    });

    function handleClick( node ) {
        if ( isOpen( node )) {
            toggleClosed( node );
        } else {
            toggleOpen( node );
        }
    }

    function isOpen( node ) {
        return $(node).hasClass( 'dropdown' );
    }

    function toggleOpen( node ) {
        $( node ).next().children( "div.list").first().slideDown();
        $( node ).attr('class', 'ui dropdown icon');
    }

    function toggleClosed( node ) {
        $( node ).next().children( "div.list").first().slideUp();
        $( node ).attr('class', 'ui caret right icon');
    }
});