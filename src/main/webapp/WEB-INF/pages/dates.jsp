    <%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <t:page>
    <jsp:attribute name="title">5.  Choose Date Parameters</jsp:attribute>
    <jsp:body>
        <form method="post" class="form ui">
                <div class="field">
                    <div class="ui radio checkbox">
                        <c:choose>
                            <c:when test="${sessionScope.currentSelection.currentMonth}">
                                <input type="radio" name="reportPeriod" value="current" checked="checked">
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="reportPeriod" value="current">
                            </c:otherwise>
                        </c:choose>
                        <label>Current Month Totals (${currentMonth})</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <c:choose>
                            <c:when test="${sessionScope.currentSelection.annual}">
                                <input type="radio" name="reportPeriod" value="annual" checked="checked">
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="reportPeriod" value="annual">
                            </c:otherwise>
                        </c:choose>
                        <label>Annual Totals - ${firstAnnual} to Present (${currentAnnual})</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <c:choose>
                            <c:when test="${sessionScope.currentSelection.quarterly}">
                                <input type="radio" name="reportPeriod" value="quarterly" checked="checked">
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="reportPeriod" value="quarterly">
                            </c:otherwise>
                        </c:choose>
                        <label>Quarterly Totals - ${firstQuarter} to Present (${currentQuarter})</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <c:choose>
                            <c:when test="${sessionScope.currentSelection.monthly}">
                                <input type="radio" name="reportPeriod" value="range" checked="checked">
                            </c:when>
                            <c:otherwise>
                                <input type="radio" name="reportPeriod" value="range">
                            </c:otherwise>
                        </c:choose>
                        <label>Custom Date Range</label>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label>From month</label>
                        <select name="fromMonth" class="ui dropdown">
                            <c:forEach var="month" items="${months}" varStatus="status">
                                <c:set var="selected"></c:set>
                                <c:if test="${sessionScope.currentSelection.startDate != null && sessionScope.currentSelection.monthly && sessionScope.currentSelection.startDate.monthOfYear == status.count}">
                                    <c:set var="selected">selected</c:set>
                                </c:if>
                                <option ${selected} value="${status.count}">${month}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="field">
                        <label>Year</label>
                        <select name="fromYear" class="ui dropdown">
                            <c:forEach var="year" items="${years}">
                                <c:set var="selected"></c:set>
                                <c:if test="${sessionScope.currentSelection.startDate != null && sessionScope.currentSelection.monthly && sessionScope.currentSelection.startDate.year == year}">
                                    <c:set var="selected">selected</c:set>
                                </c:if>
                                <option ${selected} value="${year}">${year}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="field">
                        <label>To month</label>
                        <select name="toMonth" class="ui dropdown">
                            <c:forEach var="month" items="${months}" varStatus="status">
                                <c:set var="selected"></c:set>
                                <c:if test="${sessionScope.currentSelection.endDate != null && sessionScope.currentSelection.monthly && sessionScope.currentSelection.endDate.monthOfYear == status.count}">
                                    <c:set var="selected">selected</c:set>
                                </c:if>
                                <option ${selected} value="${status.count}">${month}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="field">
                        <label>Year</label>
                        <select name="toYear">
                            <c:forEach var="year" items="${years}">
                                <c:set var="selected"></c:set>
                                <c:if test="${sessionScope.currentSelection.endDate != null && sessionScope.currentSelection.monthly && sessionScope.currentSelection.endDate.year == year}">
                                    <c:set var="selected">selected</c:set>
                                </c:if>
                                <option ${selected} value="${year}">${year}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            <div><input type="submit" name="action" value="Run" class="ui button"></div>
        </form>
    </jsp:body>
</t:page>