<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:page>
    <jsp:attribute name="title">
        2.  Choose a report
    </jsp:attribute>
    <jsp:body>
        <form method="post" id="stats_form" class="ui form">
            <div class="ui segment basic compact">
                <div class="ui list">
                    <c:if test="${selectedReport.xmlId == 'R1'}">
                        <c:set var="extraClass" value="active"/>
                    </c:if>
                    <div class="item">
                        <a href="${pageContext.request.contextPath}/mutual?reportType=R1" class="ui fluid ${extraClass} button">${monthly.name}</a>
                    </div>
                    <div class="item">
                        <div class="header">Specialty Reports</div>
                    </div>

                    <c:forEach var="specialty" items="${monthlySpecialty}">
                        <c:set var="extraClass" value=""/>
                        <c:if test="${selectedReport.xmlId == specialty.xmlId}">
                            <c:set var="extraClass" value="active"/>
                        </c:if>
                        <div class="item">
                            <a href="${pageContext.request.contextPath}/mutual?reportType=${specialty.xmlId}" class="ui fluid ${extraClass} button">${specialty.name}</a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </form>
    </jsp:body>
</t:page>
