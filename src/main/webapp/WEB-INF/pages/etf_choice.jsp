<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:page>
    <jsp:attribute name="title">
        2.  Choose a report
    </jsp:attribute>
    <jsp:body>
        <div class="ui segment basic compact">
            <input type="hidden" name="reportType" id="selectedReport" value="${selectedReport.xmlId}"/>
            <div class="ui list">
                <c:if test="${selectedReport.xmlId == 'ETF'}">
                    <c:set var="extraClass" value="active"/>
                </c:if>
                <div class="item">
                    <a href="${pageContext.request.contextPath}/mutual?reportType=ETF" class="ui fluid ${extraClass} button">${etf.name}</a>
                </div>
                <div class="item">
                    Specialty Reports
                </div>

                <c:forEach var="specialty" items="${etfSpecialty}">
                    <c:set var="extraClass" value=""/>
                    <c:if test="${selectedReport.xmlId == specialty.xmlId}">
                        <c:set var="extraClass" value="active"/>
                    </c:if>
                    <div class="item">
                        <a href="${pageContext.request.contextPath}/etf?reportType=${specialty.xmlId}" class="ui fluid ${extraClass} button">${specialty.name}</a>
                    </div>
                </c:forEach>
            </div>
        </div>
    </jsp:body>
</t:page>
