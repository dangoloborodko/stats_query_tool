<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:page>
    <jsp:attribute name="header">
    <script type="text/javascript">
        $(document).ready(function () {
            $("select[name='categoryLevel']").change(function () {
                $("#stats_form").submit();
            });
        })
    </script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/expand_collapse.js"></script>
    </jsp:attribute>
    <jsp:attribute name="title">
        4.  Choose Investment Objective
    </jsp:attribute>
    <jsp:body>
            <div class="ui segment basic"><a href="http://www.ici.org/research/stats/iob_update/iob_definitions">See Objective Category Definitions</a></div>
        <form method="post" id="stats_form" class="ui form">
            <c:choose>
                <c:when test="${sessionScope.currentSelection.mosAvailable}">
                    <c:set var="columns">three</c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="columns">two</c:set>
                </c:otherwise>
            </c:choose>

            <c:if test="${!empty(errorMessage)}">
                <div class="ui inverted red segment">
                    <p>${errorMessage}</p>
                </div>
            </c:if>
            <div class="ui ${columns} column grid">
                <div class="column">
                    <h3 class="ui header">Choose a category view</h3>
                    <select name="categoryLevel" class="ui dropdown">
                        <option value="macro" <c:if test="${categoryLevel == 'macro'}"> selected="selected" </c:if>>
                            Level 3 - Macro
                        </option>
                        <option value="composite"
                                <c:if test="${categoryLevel == 'composite'}"> selected="selected" </c:if>>Level 4 -
                            Composite
                        </option>
                        <option value="objectives"
                                <c:if test="${categoryLevel == 'objectives'}"> selected="selected"</c:if>>Level 5 -
                            Objectives
                        </option>
                        <option value="hierarchy"
                                <c:if test="${categoryLevel == 'hierarchy'}"> selected="selected" </c:if>>Display
                            Complete Hierarchy
                        </option>
                    </select>
                    <h5 class="ui header">Check the investment objectives you wish to see.</h5>
                    <c:choose>
                        <c:when test="${categoryLevel == 'hierarchy'}">
                            <div class="ui list">
                                <c:forEach var="category" items="${categories}">
                                    <t:tree category="${category}"></t:tree>
                                </c:forEach>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="category" items="${categories}">
                                <div class="field">
                                    <div class="ui checkbox">
                                        <input type="checkbox" name="selectedCategories" value="${category.name}"
                                        <c:if test="${ici:contains(sessionScope.currentSelection.categories, category )}"> checked="checked"</c:if>>
                                        <label>${category.name}</label>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </div>
                <c:if test="${sessionScope.currentSelection.mosAvailable}">
                    <div class="column">
                        <h3>Method Of Sales (Optional)</h3>
                        <p>Method of sales breakouts are only available for high-level categories.</p>
                        <c:forEach var="mos" items="${sessionScope.currentSelection.report.methodsOfSale}">
                            <div class="field">
                                <div class="ui checkbox"><input type="checkbox" name="selectedMos" value="${mos.name}"
                                <c:if test="${ici:contains(sessionScope.currentSelection.methodsOfSale, mos)}"> checked="checked" </c:if>><label>${mos.name}</label>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </c:if>
                <div class="column">
                    <input type="submit" name="action" value="Next" class="ui button">
                </div>
            </div>
        </form>
    </jsp:body>
</t:page>