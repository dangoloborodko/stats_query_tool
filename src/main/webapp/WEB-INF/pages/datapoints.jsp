<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:page>
    <jsp:attribute name="header">
        <script type="text/javascript">
            function setSelectedReport(value) {
                document.getElementById("selectedReport").value = value;
                document.getElementById("stats_form").submit();
                console.log("submitted");
            }

            function selectAll() {
                $('input:checkbox').prop('checked', true);
            }

            function unselectAll() {
                $('input:checkbox').prop('checked', false);
            }
        </script>
    </jsp:attribute>
    <jsp:attribute name="title">
        3.  Choose Data Points
    </jsp:attribute>
    <jsp:body>
        <form method="post" id="stats_form" class="ui form">
            <div class="ui middle aligned two column centered grid">
                <div class="row">
                    <div class="column">
                        <c:forEach var="dataPointGroup" items="${sessionScope.currentSelection.report.dataPointGroups}">
                            <h5 class="ui header">${dataPointGroup.name}</h5>
                            <c:forEach var="dataPoint" items="${dataPointGroup.points}">
                                <div class="field">
                                    <div class="ui checkbox">
                                        <input type="checkbox" name="dataPoints" id="${ici:jsSafe( dataPoint.fullName)}"
                                               value="${dataPoint.fullName}" <c:if
                                                test="${ici:contains( sessionScope.currentSelection.dataPoints, dataPoint)}"> checked="checked" </c:if> />
                                        <label for="${ici:jsSafe( dataPoint.fullName)}">${dataPoint.fullName} <c:if
                                                test="${!dataPoint.mosCompatible}"> * </c:if></label>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:forEach>
                        <a href="#" onclick="selectAll()" class="ui button">Select All</a>
                        <a href="#" onclick="unselectAll()" class="ui button">Unselect All</a>
                    </div>
                    <div class="column"><input class="ui button" type="submit" name="action" value="Next"/></div>
                </div>
            </div>
        </form>
    </jsp:body>
</t:page>
