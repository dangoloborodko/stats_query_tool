<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<t:page>
    <jsp:attribute name="title">1.  Choose a fund type</jsp:attribute>
    <jsp:body>
        <form class="ui form">
            <a href="${pageContext.request.contextPath}?choice=mutual" class="ui button massive">Mutual Funds</a>
            <a href="${pageContext.request.contextPath}?choice=etf" class="ui button massive">ETF</a>
        </form>
    </jsp:body>
</t:page>