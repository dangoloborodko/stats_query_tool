<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:page>
    <jsp:attribute name="header">
    <script type="text/javascript">
        $(document).ready(function () {
            $("select[name='categoryLevel']").change(function () {
                $("#stats_form").submit();
            });
        })
    </script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/expand_collapse.js"></script>
    </jsp:attribute>
    <jsp:attribute name="title">
        4.  Choose Investment Objective
    </jsp:attribute>
    <jsp:body>
        <div class="ui segment basic"><a href="http://www.ici.org/research/stats/iob_update/iob_definitions">See
            Objective Category Definitions</a></div>
        <form method="post" id="stats_form" class="ui form">
            <div class="ui grid two columns">
                <div class="column">
                    <c:if test="${!empty(errorMessage)}">
                        <div class="ui inverted red segment">
                            <p>${errorMessage}</p>
                        </div>
                    </c:if>
                    <h5 class="ui header">Check the investment objectives you wish to see.</h5>
                    <div class="ui list">
                        <c:forEach var="category" items="${categories}">
                            <t:tree category="${category}" levelsToExpand="2"></t:tree>
                        </c:forEach>
                    </div>
                </div>
                <div class="column">
                    <input type="submit" name="action" value="Next" class="ui button">
                </div>
            </div>
        </form>
    </jsp:body>
</t:page>