<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:page>
    <jsp:attribute name="title">
        Final Report
    </jsp:attribute>
    <jsp:body>
    <form class="ui form">
        <table id="stats_results" class="ui very compact table striped">
            <thead>
            <tr>
                <th>Date</th>
                <th>Category</th>
                <c:if test="${result.showFundCounts}">
                    <th>${result.fundCountLabel}</th>
                </c:if>
                <c:forEach var="dataPoint" items="${result.request.dataPoints}">
                    <th class="ui right aligned">${dataPoint.fullName}</th>
                </c:forEach>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="row" items="${result.data}">
                <tr>
                    <td>${result.getDisplayDate(row)}</td>
                    <td>${row.category}</td>
                    <c:if test="${result.showFundCounts}">
                        <td>${row.fundCount}</td>
                    </c:if>
                    <c:forEach var="dataPoint" items="${result.request.dataPoints}">
                        <td class="ui right aligned">
                            <fmt:formatNumber type="number" value="${row.valueMap[dataPoint]}"/>
                        </td>
                    </c:forEach>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </form>
    </jsp:body>
</t:page>