<%@tag description="Page template" pageEncoding="UTF-8" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="title" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css"
          type="text/css">
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.js"
            type="text/javascript"></script>
    <jsp:invoke fragment="header"/>
</head>
<body>
<div class="main ui container">
    <div class="ui divided relaxed grid">
        <div class="row">
            <div class="ui sixteen wide column">
                <img src="http://www.ici.org/deployedfiles/ICI/Site%20Properties/images/20/stats_builder_banner.jpg">
            </div>
        </div>
        <div class="row">
            <div class="ui four wide column">
            </div>
            <div class="ui twelve wide column">
                <h1 class="ui header">${title}</h1>
            </div>
        </div>
        <div class="row">
            <div class="ui four wide column">
                <h4 class="ui header">Your Report Options</h4>
                <div class="ui divided relaxed list">
                    <div class="item">
                        <div class="right floated content">
                            <a class="ui mini button" href="${pageContext.request.contextPath}">edit</a>
                        </div>
                        <div class="content">
                            <div class="header">Fund Type</div>
                            ${sessionScope.currentSelection.fundType}
                        </div>
                    </div>
                    <div class="item">
                        <div class="right floated content">
                            <c:choose>
                                <c:when test="${sessionScope.currentSelection.ETF}">
                                    <a class="ui mini button" href="${pageContext.request.contextPath}/">edit</a>
                                </c:when>
                                <c:otherwise>
                                    <a class="ui mini button" href="${pageContext.request.contextPath}/mutual">edit</a>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="content">
                            <div class="header">Report Type</div>
                            ${sessionScope.currentSelection.report.reportType.name}
                        </div>
                    </div>
                    <c:if test="${!empty(sessionScope.currentSelection.dataPointsStr)}">
                        <div class="item">
                            <div class="right floated content">
                                <a class="ui mini button" href="${pageContext.request.contextPath}/datapoints">edit</a>
                            </div>
                            <div class="content">
                                <div class="header">Data Points</div>
                                    ${sessionScope.currentSelection.dataPointsStr}
                            </div>

                        </div>
                    </c:if>
                    <c:if test="${!empty(sessionScope.currentSelection.categoriesStr)}">
                        <div class="item">
                            <div class="right floated content">
                                <a class="ui mini button" href="${pageContext.request.contextPath}/objectives">edit</a>
                            </div>
                            <div class="content">
                                <div class="header">Objectives</div>
                                    ${sessionScope.currentSelection.categoriesStr}
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${!empty(sessionScope.currentSelection.methodsOfSaleStr)}">
                        <div class="item">
                            <div class="right floated content">
                                <a class="ui mini button" href="${pageContext.request.contextPath}/objectives">edit</a>
                            </div>
                            <div class="content">
                                <div class="header">Method Of Sale</div>
                                    ${sessionScope.currentSelection.methodsOfSaleStr}
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${!empty(sessionScope.currentSelection.dateRange)}">
                        <div class="item">
                            <div class="right floated content">
                                <a class="ui mini button" href="${pageContext.request.contextPath}/dates">edit</a>
                            </div>
                            <div class="content">
                                <div class="header">Date Range</div>
                                    ${sessionScope.currentSelection.dateRange}
                            </div>
                        </div>
                    </c:if>
                </div>
                <c:if test="${sessionScope.currentSelection.readyToRun}">
                    <p><a href="${pageContext.request.contextPath}/report" class="ui labeled icon button">Run
                        Report<i class="chevron circle right icon"></i></a></p>
                    <p><a href="${pageContext.request.contextPath}/report/export" class="ui labeled icon button">Export
                        To
                        Excel<i class="file excel outline icon"></i></a>
                    </p>
                </c:if>
            </div>
            <div class="ui twelve wide column">
                <jsp:doBody/>
            </div>
        </div>
    </div>
</div>
</body>
</html>