<%@tag description="tree template" pageEncoding="UTF-8" %>
<%@attribute name="category" type="org.ici.stats.domain.Category" required="true" %>
<%@attribute name="levelsToExpand" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="ici" uri="/WEB-INF/tld/ici.tld" %>
<%@taglib prefix="templates" tagdir="/WEB-INF/tags" %>

<c:set var="levelsToExpand" value="${(empty levelsToExpand) ? 1000 : levelsToExpand}" />

<div class="item">
    <c:choose>
        <c:when test="${not empty category.children && levelsToExpand > 0}">
            <i class="dropdown icon"></i>
        </c:when>
        <c:when test="${not empty category.children && levelsToExpand <= 0}">
            <i class="caret right icon"></i>
            <c:set var="display" value="style=\"display:none\""/>
        </c:when>
        <c:otherwise>
            <%-- used as a spacer to make sure we indent--%>
            <i class="icon"></i>
        </c:otherwise>
    </c:choose>
    <div class="content">
        <div class="description">
        <div class="ui checkbox">
            <input type="checkbox" name="selectedCategories" value="${category.name}"
                    <c:if test="${ici:contains(sessionScope.currentSelection.categories, category )}"> checked="checked"</c:if> />
            <label>${category.name}</label>
        </div>
        </div>
        <c:if test="${not empty category.children}">
            <div class="list" ${display}>
                <c:forEach var="cat1" items="${category.children}">
                    <templates:tree category="${cat1}" levelsToExpand="${levelsToExpand-1}"></templates:tree>
                </c:forEach>
            </div>
        </c:if>
    </div>
</div>
