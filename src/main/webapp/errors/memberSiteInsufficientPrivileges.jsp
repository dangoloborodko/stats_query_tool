<p class="column_header">Document Access Failure</p>

<%--    <html:messages id="msg" message="false" property="org.apache.struts.action.GLOBAL_MESSAGE">
        <p class="global_error"><c:out value="${msg}"/></p>
    </html:messages> --%>
<html:errors property="org.ici.icinet.view.ERRORS" name="confidential_messages"/>

<c:if test="${empty confidential_messages || confidential_messages['empty']}">
<p class="left_margin_text">The access codes you are using do not have the privileges needed to access this document.</p>

<p class="left_margin_text">Access to some materials on this site is limited to members of particular ICI committees or mailing lists. You may have received this message because a colleague forwarded a hyperlink to you, and their privileges are different from yours. Please contact <a href="mailto:webmaster@ici.org">webmaster@ici.org</a> for help obtaining the materials you need.</p>
</c:if>